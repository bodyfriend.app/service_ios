//
//  SalarySearchView.swift
//  service
//
//  Created by insung on 2018. 9. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SalarySearchView: UIView, InputTextProtocol, UIPickerViewDelegate, UIPickerViewDataSource {

    var delegate: SearchProtocol?
    
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!

    var searchDate: Date?
    var dateTF: CTTextField?
    var yearDatas: [String]?
    var monthDatas: [String]?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> SalarySearchView {
        return UINib(nibName: "SalarySearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SalarySearchView
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initValue()
        initLayout()
    }
    
    func initValue() {
        let year = Int(CommonUtil.todayFromDate(format: "yyyy"))
        yearDatas = [String]()
        for i in year!-30...year! {
            yearDatas?.append(String(format: "%d년", i))
        }
        monthDatas = [String]()
        for i in 0..<12 {
            monthDatas?.append(String(format: "%d월", i+1))
        }
    }
    
    func initLayout() {
        let todayDate = Date()
        
        searchDate = todayDate
        dateBtn.setTitle(CommonUtil.todayFromDate(format: "yyyy년 M월"), for: .normal)

        dateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        searchBtn.applyRoundBorder()

        dateTF = CTTextField()
        addSubview(dateTF!)
        dateTF?.accessoryDelegate = self
        dateTF?.setPickerView(self)
    }
    
    /// 설정된 파라미터
    ///
    /// - Returns: 파라미터
    func sendParameters() -> Dictionary<String, Any> {
        var parameters = Dictionary<String, Any>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminNm)!, forKey: "engineerNm")
        let year = CommonUtil.changeDateFormat(preFormat: "yyyy년 M월", date: dateBtn.titleLabel?.text, changeFormat: "yyyy")
        let month = CommonUtil.changeDateFormat(preFormat: "yyyy년 M월", date: dateBtn.titleLabel?.text, changeFormat: "M")
        parameters.updateValue(year!, forKey: "year")
        parameters.updateValue(month!, forKey: "month")
        return parameters
    }
    
    // MARK: Action Event
    @IBAction func pressedDate(_ sender: UIButton) {
        let year = CommonUtil.stringFromDate(searchDate!, format: "yyyy년")
        let month = CommonUtil.stringFromDate(searchDate!, format: "M월")
        dateTF?.contentPV?.selectRow((yearDatas?.firstIndex(of: year))!, inComponent: 0, animated: true)
        dateTF?.contentPV?.selectRow(monthDatas?.firstIndex(of: month) ?? 0, inComponent: 1, animated: true)
        dateTF?.showPickerView(self)
    }
    
    @IBAction func pressedSalarySearch(_ sender: UIButton) {
        delegate?.requestSearch()
    }
    
    // MARK: UIPickerViewDatasource & UIPickerViewDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return yearDatas?.count ?? 0
        } else {
            return monthDatas?.count ?? 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return yearDatas?[row] ?? ""
        } else {
            return monthDatas?[row] ?? ""
        }
    }
    
    // MARK: CTTextFieldDelegate
    func pressedDone(_ sender: Any) {
        if sender is UIPickerView {
            guard let pv = sender as? UIPickerView else {
                return
            }
            let yearIndex = pv.selectedRow(inComponent: 0)
            let monthIndex = pv.selectedRow(inComponent: 1)
            
            let selectedDate = String(format: "%@ %@", yearDatas![yearIndex], monthDatas![monthIndex])
            searchDate = CommonUtil.dateFromString(selectedDate, format: "yyyy년 M월")
            dateBtn.setTitle(selectedDate, for: .normal)
        }
    }
}
