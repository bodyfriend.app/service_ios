//
//  BusinessLogSearchView.swift
//  service
//
//  Created by insung on 2018. 9. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class BusinessLogSearchView: UIView, InputTextProtocol {

    var delegate: SearchProtocol?
    
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var endBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!

    var startDate = Date()
    var endDate = Date()
    var inputTF = CTTextField()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> BusinessLogSearchView {
        return UINib(nibName: "BusinessLogSearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BusinessLogSearchView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        startBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        endBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        searchBtn.applyRoundBorder()
        
        let todayDate = CommonUtil.todayFromDate(format: "yyyy-MM-dd")
        startBtn.setTitle(todayDate, for: .normal)
        endBtn.setTitle(todayDate, for: .normal)
        
        inputTF.accessoryDelegate = self
        self.addSubview(inputTF)
        inputTF.setDatePickerView(.date)
    }

    /// 설정된 파라미터
    ///
    /// - Returns: 파라미터
    func sendParameters() -> Dictionary<String, Any> {
        var parameters = Dictionary<String, String>()
        parameters.updateValue("4", forKey: "progress_no")
        parameters.updateValue("0", forKey: "searchone")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminNm)!, forKey: "searchnm")
        parameters.updateValue((startBtn.titleLabel?.text)!, forKey: "dateStart")
        parameters.updateValue((endBtn.titleLabel?.text)!, forKey: "dateEnd")
        return parameters
    }
    
    // MARK: Action Event
    @IBAction func pressedStart(_ sender: Any) {
        inputTF.dateType = DateType.start.rawValue
        inputTF.contentDP?.date = startDate
        inputTF.becomeFirstResponder()
    }
    
    @IBAction func pressedEnd(_ sender: Any) {
        inputTF.dateType = DateType.end.rawValue
        inputTF.contentDP?.date = endDate
        inputTF.becomeFirstResponder()
    }
    
    @IBAction func pressedSearch(_ sender: UIButton) {
        delegate?.requestSearch()
    }
    
    @objc func pressedDone(_ sender: Any, type: NSInteger) {
        guard let inputDate = inputTF.contentDP?.date else {
            return
        }
        let title = CommonUtil.stringFromDate(inputDate, format: "yyyy-MM-dd")
        if type == DateType.start.rawValue {
            startDate = inputDate
            startBtn.setTitle(title, for: .normal)
        } else if type == DateType.end.rawValue {
            endDate = inputDate
            endBtn.setTitle(title, for: .normal)
        }
    }
}
