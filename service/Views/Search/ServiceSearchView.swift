//
//  ServiceSearchView.swift
//  service
//
//  Created by insung on 2018. 7. 23..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit


enum SearchSortType: Int {
    case receiveDate
    case visitDate
}

class ServiceSearchView: UIView, InputTextProtocol {

    var delegate: SearchProtocol?
    var target: UIViewController?
    
    @IBOutlet weak var nameTF: CTTextField!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var sortBtn: UIButton!
    @IBOutlet weak var telTF: CTTextField!
    @IBOutlet weak var addressTF: CTTextField!
    @IBOutlet weak var materialBtn: UIButton!
    @IBOutlet weak var seeBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!

    var sortArray: Array<String>?
    var locationData: Array<CodeListData>?
    var locationArray: Array<String>?
    var startDate: Date?
    var endDate: Date?
    var inputTF = CTTextField()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> ServiceSearchView {
        return UINib(nibName: "ServiceSearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ServiceSearchView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initValue()
        initLayout()
    }
    
    func initValue() {
        sortArray = Constants.Array.SEARCH_SORT_LIST
    }
    
    func initValue(_ datas: Array<CodeListData>, preMonth: Int) {
        locationData = datas;
        
        startDate = CommonUtil.nextMonthDate(Date(), nextMonth: -preMonth)
        endDate = Date()
        
        startDateBtn.setTitle(CommonUtil.stringFromDate(startDate!, format: "yyyy-MM-dd"), for: .normal)
        endDateBtn.setTitle(CommonUtil.stringFromDate(endDate!, format: "yyyy-MM-dd"), for: .normal)
        
        restoreLocationArray()
        changeLocationTitle(0)
        changeSortTitle(0)
        
        inputTF.accessoryDelegate = self
        self.addSubview(inputTF)
        inputTF.setDatePickerView(.date)
    }
    
    func initLayout() {
        locationBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        startDateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        endDateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        sortBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        searchBtn.applyRoundBorder()
    }
    
    /// 설정된 파라미터
    ///
    /// - Returns: 파라미터
    func sendParameters() -> Dictionary<String, Any> {
        var parameters = Dictionary<String, Any>()
        if let sDate = startDateBtn.currentTitle {
            parameters.updateValue(sDate, forKey: "m_sdate")
        }
        if let eDate = endDateBtn.currentTitle {
            parameters.updateValue(eDate, forKey: "m_edate")
        }
        
        let sortType = CommonObject.indexSearchSortType(sortBtn.titleLabel?.text)
        parameters.updateValue(String(sortType), forKey: "m_order_type")
        if let name = nameTF.text {
            parameters.updateValue(name, forKey: "m_custname")
        }
        
        parameters.updateValue(materialBtn.isSelected ? "true" : "false", forKey: "SECTOR_MAT")
        parameters.updateValue(seeBtn.isSelected ? "true" : "false", forKey: "AllSector")
        if let tel = telTF.text, tel.isExist == true {
            parameters.updateValue(tel, forKey: "HPHONE_NO")
        }
        if let address = addressTF.text {
            parameters.updateValue(address, forKey: "m_addr")
        }
        let areaCode = CommonObject.codeFromData(locationData!, title: (locationBtn.titleLabel?.text)!)
        if areaCode.isExist == true {
            parameters.updateValue(areaCode!, forKey: "m_area")
        }
        return parameters
    }
    
    /// 지역 데이터 인덱스 0에 "전체" 추가
    func restoreLocationArray() {
        locationArray = Array<String>()
        for data in locationData! {
            locationArray?.append(data.detCdNm!)
        }
        locationArray?.insert("전체", at: 0)
    }
    
    /// 지역 타이틀 변경 시
    ///
    /// - Parameter index: 인덱스
    func changeLocationTitle(_ index: Int) {
        locationBtn.setTitle(locationArray?[index], for: .normal)
    }
    
    /// 정렬 타이틀 변경시
    ///
    /// - Parameter index: 인덱스
    func changeSortTitle(_ index: Int) {
        sortBtn.setTitle(sortArray?[index], for: .normal)
    }
    
    // MARK: Action Event
    @IBAction func pressedStartDate(_ sender: Any) {
        inputTF.dateType = DateType.start.rawValue
        inputTF.contentDP?.date = startDate!
        inputTF.becomeFirstResponder()
    }
    
    @IBAction func pressedEndDate(_ sender: Any) {
        inputTF.dateType = DateType.end.rawValue
        inputTF.contentDP?.date = endDate!
        inputTF.becomeFirstResponder()
    }
    
    @IBAction func pressedLocation(_ sender: UIButton) {
        CommonUtil.showActionSheet(datas: locationArray!, target: target!) { (index) in
            self.changeLocationTitle(index)
        }
    }
    
    @IBAction func pressedSort(_ sender: UIButton) {
        CommonUtil.showActionSheet(datas: sortArray!, target: target!) { (index) in
            self.changeSortTitle(index)
        }
    }
    
    @IBAction func pressedSection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func pressedSee(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func pressedSearch(_ sender: UIButton) {
        delegate?.requestSearch()
    }
    
    @objc func pressedDone(_ sender: Any, type: NSInteger) {
        guard let inputDate = inputTF.contentDP?.date else {
            return
        }
        let title = CommonUtil.stringFromDate(inputDate, format: "yyyy-MM-dd")
        if type == DateType.start.rawValue {
            startDate = inputDate
            startDateBtn.setTitle(title, for: .normal)
        } else if type == DateType.end.rawValue {
            endDate = inputDate
            endDateBtn.setTitle(title, for: .normal)
        }
    }
}
