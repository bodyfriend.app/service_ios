//
//  PriceSearchView.swift
//  service
//
//  Created by insung on 2018. 8. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class PriceSearchView: UIView {
    var delegate: SearchProtocol?
    var target: UIViewController?
    var receiveData: PriceSearchKeyData?
    @IBOutlet weak var manufacturerBtn: UIButton!
    @IBOutlet weak var divideBtn: UIButton!
    @IBOutlet weak var itemTF: CTTextField!
    @IBOutlet weak var modelTF: CTTextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> PriceSearchView {
        return UINib(nibName: "PriceSearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PriceSearchView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        manufacturerBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        divideBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        searchBtn.applyRoundBorder()
    }
    
    /// 설정된 파라미터
    ///
    /// - Returns: 파라미터
    func sendParameters() -> Dictionary<String, Any> {
        var parameters = Dictionary<String, Any>()
        if let manufacturer = manufacturerBtn?.titleLabel?.text {
            if manufacturer != "공통" {
                parameters.updateValue(manufacturerBtn?.titleLabel?.text ?? "", forKey: "manufacturer")
            }
        }
        if let divide = divideBtn?.titleLabel?.text {
            if divide != "기타" {
                parameters.updateValue(divideBtn?.titleLabel?.text ?? "", forKey: "classification")
            }
        }
        parameters.updateValue(itemTF.text ?? "", forKey: "item")
        parameters.updateValue(modelTF.text ?? "", forKey: "model_names")
        return parameters
    }
    
    /// 선택된 키 초기화
    func initSelectedKeys() {
        let manufacturer = receiveData?.mMap
        manufacturerBtn.setTitle(manufacturer?.first, for: .normal)
        let divide = receiveData?.cMap
        divideBtn.setTitle(divide?.first, for: .normal)
    }
    
    // MARK: Action Event
    @IBAction func pressedManufacturer(_ sender: UIButton) {
        let manufacturer = receiveData?.mMap
        CommonUtil.showActionSheet(datas: manufacturer!, target: target!) { (index) in
            self.manufacturerBtn.setTitle(manufacturer?[index], for: .normal)
        }
    }
    
    @IBAction func pressedDivide(_ sender: UIButton) {
        let divide = receiveData?.cMap
        CommonUtil.showActionSheet(datas: divide!, target: target!) { (index) in
            self.divideBtn.setTitle(divide?[index], for: .normal)
        }
    }
    
    @IBAction func pressedPriceSearch(_ sender: UIButton) {
        delegate?.requestSearch()
    }
}
