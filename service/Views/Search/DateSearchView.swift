//
//  DateSearchView.swift
//  service
//
//  Created by insung on 2018. 9. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DateSearchView: UIView, InputTextProtocol {
    
    var delegate: SearchProtocol?
    
    @IBOutlet weak var searchDateBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var dayBtn: UIButton!
    @IBOutlet weak var weekBtn: UIButton!
    
    var inputTF = CTTextField()
    var dayBtnGroup: Array<UIButton>?
    var searchDate = Date()
    var isSelectedDay = true
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> DateSearchView {
        return UINib(nibName: "DateSearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DateSearchView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initValue()
        initLayout()
    }
    
    func initValue() {
        dayBtnGroup = [dayBtn, weekBtn]
    }
    
    func initLayout() {
        searchDateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        searchBtn.applyRoundBorder()
        
        searchDateBtn.setTitle(CommonUtil.stringFromDate(searchDate, format: "yyyy-MM-dd"), for: .normal)
        
        inputTF.accessoryDelegate = self
        self.addSubview(inputTF)
        inputTF.setDatePickerView(.date)
        
        pressedRadio(dayBtn)
    }
    
    /// 설정된 파라미터
    ///
    /// - Returns: 파라미터
    func sendParameters() -> Dictionary<String, Any> {
        var parameters = Dictionary<String, Any>()
        var nextDayDate: Date?
        if dayBtn.isSelected == true {
            nextDayDate = searchDate
        } else if weekBtn.isSelected == true {
            nextDayDate = CommonUtil.nextDayDate(searchDate, nextDay: 6)
        }
        parameters.updateValue(CommonUtil.stringFromDate(searchDate, format: "yyyy-MM-dd"), forKey: "m_sdate")
        parameters.updateValue(CommonUtil.stringFromDate(nextDayDate!, format: "yyyy-MM-dd"), forKey: "m_edate")
        parameters.updateValue("M", forKey: "m_receivetype")
        return parameters
    }
    
    // MARK: Action Event
    @IBAction func pressedSearchDate(_ sender: UIButton) {
        inputTF.contentDP?.date = searchDate
        inputTF.becomeFirstResponder()
    }
    
    @IBAction func pressedRadio(_ sender: UIButton) {
        if sender.isSelected == true {
            return
        }
        UIButton.selectToGroup(sender, group: dayBtnGroup!)
    }
    
    @IBAction func pressedSearch(_ sender: UIButton) {
        isSelectedDay = dayBtn.isSelected
        searchDate = CommonUtil.dateFromString(searchDateBtn.titleLabel?.text, format: "yyyy-MM-dd")!
        delegate?.requestSearch()
    }
    
    @objc func pressedDone(_ sender: Any) {
        guard let inputDate = inputTF.contentDP?.date else {
            return
        }
        let title = CommonUtil.stringFromDate(inputDate, format: "yyyy-MM-dd")
        searchDateBtn.setTitle(title, for: .normal)
    }
}
