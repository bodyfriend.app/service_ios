//
//  CircleView.swift
//  service
//
//  Created by insung on 2018. 8. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class CircleView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        applyRoundBorder(bounds.size.height/2)
    }
}
