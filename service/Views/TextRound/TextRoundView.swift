//
//  TextRoundView.swift
//  service
//
//  Created by insung on 2018. 8. 23..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class TextRoundView: UIView {
    @IBOutlet weak var titleLb: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> TextRoundView {
        return UINib(nibName: "TextRoundView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TextRoundView
    }
}
