//
//  MarkerView.swift
//  service
//
//  Created by insung on 2018. 8. 29..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

let TEXT_PADDING: CGFloat = 4.0
let TEXT_PADDING_BOTTOM: CGFloat = 10.0

class MarkerView: UIView {
    var markerIV: UIImageView?
    var contentLb: UILabel?
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func initLayout() {
        markerIV = UIImageView(image: UIImage(named: "ic_marker")?.withRenderingMode(.alwaysTemplate))
        addSubview(markerIV!)
        contentLb = UILabel()
        contentLb?.numberOfLines = 0
        contentLb?.font = UIFont.boldSystemFont(ofSize: 12.0)
        contentLb?.textAlignment = .center
        markerIV?.addSubview(contentLb!)
    }
    
    func iconFromData(_ data: ServiceListData) -> UIImage? {
        var content = ""
        if let visitDate = data.visitDate,
            let visitTime = data.visitTime,
            data.visitTime.isExist == true {
            content = CommonUtil.changeDateFormat(preFormat: "yyyy-MM-dd", date: visitDate, changeFormat: "MM.dd")!
            content.append("\n")
            content.append(CommonUtil.changeDateFormat(preFormat: "HHmm", date: visitTime, changeFormat: "HH:mm")!)
        } else {
            content = "미정"
        }
        
        if let productName = data.oldProductName {
            content.append("\n")
            content.append(productName)
        }
        if let name = data.userName {
            content.append("\n")
            content.append(name)
        }
        contentLb?.text = content
        changeColor(data)
        
        if let iv = markerIV {
            contentLb?.sizeToFit()
            contentLb?.frame = CGRect(x: TEXT_PADDING, y: TEXT_PADDING, width: (contentLb?.frame.width)!, height: (contentLb?.frame.height)!)
            iv.frame = CGRect(x: 0, y: 0, width: (contentLb?.frame.width)! + TEXT_PADDING*2, height: (contentLb?.frame.height)! + TEXT_PADDING + TEXT_PADDING_BOTTOM)
            let markerImage = iv.image
            iv.image = markerImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 4.0, left: 8.0, bottom: 8.0, right: 8.0), resizingMode: .stretch)
            self.frame = iv.bounds
            return iv.imageCaptureView()
        }
        
        return nil
    }
    
    /// 데이터에 맞게 색상 변경
    ///
    /// - Parameter data: 데이터
    func changeColor(_ data: ServiceListData) {
        contentLb?.textColor = COLOR_WHITE
        markerIV?.tintColor = COLOR_GREEN
        let today = CommonUtil.todayFromDate(format: "yyyy-MM-dd")
        if let visitDate = data.visitDate {
            if visitDate == today {
                contentLb?.textColor = COLOR_WHITE
                markerIV?.tintColor = COLOR_RED
            }
        } else if data.callTypeReqDt == today {
            contentLb?.textColor = COLOR_WHITE
            markerIV?.tintColor = COLOR_ORANGE
        } else if data.longTermReq.isExist == true {
            contentLb?.textColor = COLOR_WHITE
            markerIV?.tintColor = COLOR_GREEN
        }
    }
}
