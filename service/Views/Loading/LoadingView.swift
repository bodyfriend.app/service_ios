//
//  LoadingView.swift
//  service
//
//  Created by insung on 2018. 8. 21..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    @IBOutlet weak var splashIV: UIImageView!
    
    static let shared = LoadingView.sharedView()
    private static let kAnimationKey = "rotation"
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func sharedView() -> LoadingView {
        let view = LoadingView.instanceFromNib()
        return view
    }
    
    class func instanceFromNib() -> LoadingView {
        return UINib(nibName: "LoadingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoadingView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        LoadingView.shared.frame = (Constants.Define.APPDELEGATE.window?.bounds)!
    }
    
    class func showLoadingView() {
        let view = LoadingView.shared
        if view.splashIV.layer.animation(forKey: LoadingView.kAnimationKey) == nil {
            if let window = Constants.Define.APPDELEGATE.window {
                window.addSubview(view)
                DispatchQueue.main.async { // 이전 뷰컨트롤러에 보여지던 현상 수정
                    window.bringSubviewToFront(view)
                }
            }
            
            let animation = CABasicAnimation(keyPath: "transform.rotation")
            animation.fromValue = 0.0
            animation.toValue = Float.pi * 2.0
            animation.duration = 2
            animation.repeatCount = .infinity
            view.splashIV.layer.add(animation, forKey: LoadingView.kAnimationKey)
        }
    }
    
    class func hideLoadingView() {
        let view = LoadingView.shared
        if view.splashIV.layer.animation(forKey: LoadingView.kAnimationKey) != nil {
            view.splashIV.layer.removeAnimation(forKey: LoadingView.kAnimationKey)
            view.removeFromSuperview()
        }
    }
    
    class func hideLoadingViewToDelay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            LoadingView.hideLoadingView()
        }
    }
}
