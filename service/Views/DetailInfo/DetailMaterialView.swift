//
//  DetailMaterialView.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DetailMaterialView: UIView {
    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var askLb: UILabel!
    @IBOutlet weak var planLb: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> DetailMaterialView {
        return UINib(nibName: "DetailMaterialView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DetailMaterialView
    }

    func reloadData(_ data: MatListData) {
        statusLb.text = data.matProgress
        askLb.text = data.matRequest
        planLb.text = data.matServicePlan
    }
}
