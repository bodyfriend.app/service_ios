//
//  DetailInfoBaseTypeView.swift
//  service
//
//  Created by insung on 2018. 7. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DetailInfoBaseTypeView: UIView {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var valueLb: UILabel!
    @IBOutlet weak var valueTap: UITapGestureRecognizer!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> DetailInfoBaseTypeView {
        return UINib(nibName: "DetailInfoBaseTypeView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DetailInfoBaseTypeView
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
