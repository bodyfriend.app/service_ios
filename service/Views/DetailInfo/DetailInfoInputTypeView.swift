//
//  DetailInfoInputTypeView.swift
//  service
//
//  Created by insung on 2018. 7. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DetailInfoInputTypeView: UIView, UITextViewDelegate {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var valueV: UIView!
    @IBOutlet weak var valueTV: CTTextView!
    @IBOutlet weak var subValueLb: UILabel!
    @IBOutlet weak var inputBtn: UIButton!
    @IBOutlet var valueHeightLC: NSLayoutConstraint!
    @IBOutlet var subValueHeightLC: NSLayoutConstraint!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> DetailInfoInputTypeView {
        return UINib(nibName: "DetailInfoInputTypeView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DetailInfoInputTypeView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout(){
        valueTV.delegate = self
        valueV.applyRoundBorder(COLOR_BORDER_GRAY)
        inputBtn.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        // 텍스트 길이에 따라 늘림
        let frame = textView.frame
        textView.sizeToFit()
        valueHeightLC.constant = textView.bounds.height
        textView.frame = frame
    }
}
