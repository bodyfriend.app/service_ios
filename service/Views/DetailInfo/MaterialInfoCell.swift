//
//  MaterialInfoCell.swift
//  service
//
//  Created by insung on 16/07/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit

class MaterialInfoCell: UITableViewCell {

    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var askLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    @IBOutlet weak var typeLb: UILabel!
    @IBOutlet weak var orderStateLb: UILabel!
    @IBOutlet weak var recieveDateLb: UILabel!
    @IBOutlet weak var stateLb: UILabel!
    @IBOutlet weak var detailLb: UILabel!
    @IBOutlet weak var memoLb: UILabel!
    
    @IBOutlet weak var serviceProgressV: UIView!
    @IBOutlet weak var serviceProgress01IV: UIImageView!
    @IBOutlet weak var serviceProgress02IV: UIImageView!
    @IBOutlet weak var serviceProgress03IV: UIImageView!
    @IBOutlet weak var serviceProgress04IV: UIImageView!
    @IBOutlet weak var serviceBarV: UIView!
    @IBOutlet weak var serviceBarValueV: UIView!
    @IBOutlet weak var serviceDesc01IV: UIImageView!
    @IBOutlet weak var serviceDesc02IV: UIImageView!
    @IBOutlet weak var serviceDesc03IV: UIImageView!
    @IBOutlet weak var serviceDesc04IV: UIImageView!
    
    @IBOutlet weak var deliveryProgressV: UIView!
    @IBOutlet weak var deliveryProgress01IV: UIImageView!
    @IBOutlet weak var deliveryProgress02IV: UIImageView!
    @IBOutlet weak var deliveryProgress03IV: UIImageView!
    @IBOutlet weak var deliveryBarV: UIView!
    @IBOutlet weak var deliveryBarValueV: UIView!
    @IBOutlet weak var deliveryDesc01IV: UIImageView!
    @IBOutlet weak var deliveryDesc02IV: UIImageView!
    @IBOutlet weak var deliveryDesc03IV: UIImageView!
    
    @IBOutlet var orderStateHeightLC: NSLayoutConstraint!
    @IBOutlet var serviceBarLC: NSLayoutConstraint!
    @IBOutlet var deliveryBarLC: NSLayoutConstraint!
    @IBOutlet var lineHeightLC: NSLayoutConstraint!
    
    var serviceActiveIVGroup: Array<UIImageView>?
    var deliveryActiveIVGroup: Array<UIImageView>?
    var serviceShowIVGroup: Array<UIImageView>?
    var deliveryShowIVGroup: Array<UIImageView>?
    
    var confirmBtnHandler: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initValue()
        initLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initValue() {
        serviceActiveIVGroup = [serviceProgress01IV, serviceProgress02IV, serviceProgress03IV, serviceProgress04IV]
        serviceShowIVGroup = [serviceDesc01IV, serviceDesc02IV, serviceDesc03IV, serviceDesc04IV]
        deliveryActiveIVGroup = [deliveryProgress01IV, deliveryProgress02IV, deliveryProgress03IV]
        deliveryShowIVGroup = [deliveryDesc01IV, deliveryDesc02IV, deliveryDesc03IV]
    }
    
    func initLayout() {
        confirmBtn.applyRoundBorder()
        serviceBarV.applyRoundBorder(serviceBarV.frame.height/2)
        serviceBarValueV.applyRoundBorder(serviceBarValueV.frame.height/2)
        deliveryBarV.applyRoundBorder(deliveryBarV.frame.height/2)
        deliveryBarValueV.applyRoundBorder(deliveryBarValueV.frame.height/2)
    }
    
    func setCellFromData(_ data: MaterialDetailData) {
        
        askLb.text = data.materialName
        countLb.text = String(data.count)
        detailLb.text = data.stateDetail
        memoLb.text = data.memo
        
        orderStateLb.text = nil
        recieveDateLb.text = nil
        orderStateHeightLC.isActive = true
        if data.requestType == "C" ||
            data.requestType == "V" {
            typeLb.text = "자재요청"
            serviceProgressV.isHidden = false
            deliveryProgressV.isHidden = true

            if data.stateCode == "AB" ||
                data.stateCode == "BA" {
                showProgressState(serviceProgress03IV, serviceDesc03IV, true)
            } else if data.stateCode == "CA" {
                showProgressState(serviceProgress04IV, serviceDesc04IV, true)
            } else if data.stockCount == 0 {
                let date = data.receiveSchduleDate
                if let state = data.orderStateName, state.isEmpty == false {
                    orderStateLb.text = state
                    recieveDateLb.text = String(format: " (입고예정일 : %@)", date ?? "")
                } else {
                    recieveDateLb.text = date
                }
                orderStateHeightLC.isActive = false
                showProgressState(serviceProgress01IV, serviceDesc01IV, true)
            } else if data.stateCode == "AA" {
                showProgressState(serviceProgress02IV, serviceDesc02IV, true)
            }
        } else if data.requestType == "A" ||
            data.requestType == "D" {
            typeLb.text = "택배요청"
            deliveryProgressV.isHidden = false
            serviceProgressV.isHidden = true
            
            if data.stateCode == "BA" {
                showProgressState(deliveryProgress03IV, deliveryDesc03IV, false)
            } else if data.stockCount == 0 {
                let date = data.receiveSchduleDate
                if let state = data.orderStateName, state.isEmpty == false {
                    orderStateLb.text = state
                    recieveDateLb.text = String(format: " (입고예정일 : %@)", date ?? "")
                } else {
                    recieveDateLb.text = date
                }
                orderStateHeightLC.isActive = false
                showProgressState(deliveryProgress01IV, deliveryDesc01IV, false)
            } else if data.stateCode == "AA" {
                showProgressState(deliveryProgress02IV, deliveryDesc02IV, false)
            }
        }
        
        let state = data.state
        stateLb.text = state
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        let isMe = data.receiveName == userData?.adminId
        if state == "요청" ||
            ((state == "준비완료" || (state == "수령확인" && isMe)) && (data.requestType == "C" || data.requestType == "V")) {
            confirmBtn.isHidden = false
            if state == "요청" {
                confirmBtn.setTitle("요청취소", for: .normal)
            } else if state == "준비완료"{
                confirmBtn.setTitle("수령확인", for: .normal)
            } else if state == "수령확인" {
                confirmBtn.setTitle("수령확인 취소", for: .normal)
            }
        } else {
            confirmBtn.isHidden = true
        }
    }
    
    func showProgressState(_ pIV: UIImageView, _ dIV: UIImageView, _ isService: Bool) {
        DispatchQueue.main.async {
            if isService == true {
                self.serviceBarLC.constant = pIV.center.x
                UIImageView.activeToGroup(pIV, group: self.serviceActiveIVGroup!)
                UIImageView.showToGroup(dIV, group: self.serviceShowIVGroup!)
            } else {
                self.deliveryBarLC.constant = pIV.center.x
                UIImageView.activeToGroup(pIV, group: self.deliveryActiveIVGroup!)
                UIImageView.showToGroup(dIV, group: self.deliveryShowIVGroup!)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        askLb.text = nil
        countLb.text = nil
        typeLb.text = nil
        orderStateLb.text = nil
        recieveDateLb.text = nil
        stateLb.text = nil
        detailLb.text = nil
        memoLb.text = nil
        serviceBarLC.constant = 0
        deliveryBarLC.constant = 0
    }
    
    @IBAction func pressedConfirm(_ sender: UIButton) {
        confirmBtnHandler?()
    }
}
