//
//  TableItemView.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class TableItemView: UIView {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var valueLb: UILabel!
    @IBOutlet weak var lineV: UIView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> TableItemView {
        return UINib(nibName: "TableItemView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TableItemView
    }
}
