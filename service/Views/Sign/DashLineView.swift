//
//  DashLineView.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DashLineView: UIView {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let linePath = UIBezierPath()
        if self.bounds.height < self.bounds.width {
            linePath.move(to: CGPoint(x: 0, y: self.bounds.height/2))
            linePath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height/2))
        } else if self.bounds.height > self.bounds.width {
            linePath.move(to: CGPoint(x: self.bounds.width/2, y: 0))
            linePath.addLine(to: CGPoint(x: self.bounds.width/2, y: self.bounds.height))
        }
        
        let dashPattern: [CGFloat] = [5.0, 2.0]
        linePath.setLineDash(dashPattern, count: 2, phase: 0)
        linePath.lineWidth = 0.4
        COLOR_BORDER_GRAY.setStroke()
        linePath.stroke()
    }
}
