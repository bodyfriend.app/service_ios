//
//  DrawView.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class DrawView: UIView {
    var drawPath: UIBezierPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        initValue()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        COLOR_BLACK.setStroke()
        drawPath?.stroke()
    }
    
    func initValue() {
        drawPath = UIBezierPath()
        drawPath?.lineWidth = 3.0
        drawPath?.lineCapStyle = .round
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self)
        drawPath?.move(to: point!)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self)
        drawPath?.addLine(to: point!)
        self.setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesMoved(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesEnded(touches, with: event)
    }
}
