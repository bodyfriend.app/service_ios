//
//  TopBarView.swift
//  service
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class TopbarView: UIView {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var salaryLb: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var promotionBtn: UIButton!
    @IBOutlet weak var noticeBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var optionV: UIView!
    @IBOutlet var optionHeightLC: NSLayoutConstraint!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func instanceFromNib() -> TopbarView {
        return UINib(nibName: "TopbarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TopbarView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // 하단 그림자 생성
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowOpacity = 0.5
    }
}
