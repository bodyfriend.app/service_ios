//
//  ServiceDetailResponse.swift
//  service
//
//  Created by insung on 2018. 8. 1..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct ServiceDetailResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: DetailData?
    let matList: [MatListData]?
}

struct DetailData: Codable {
    let oldProductName: String?
    let receiveSeq: String?
    let picturePathThree: String?
    let inProductDate: String?
    let purchasingOffice: String?
    let visitDate: String?
    let productSn: String?
    let modConsultant: String?
    let oldManufacturer: String?
    let picturePathTwo: String?
    let callType: String?
    let longTermReq: String?
    let addr: String?
    let accessIp: String?
    let symptomGroupSix: String?
    let cashReceiptCardNum: String?
    let depositCheck: String?
    let isComplain: String?
    let visitTime: String?
    let accessId: String?
    let serviceMonth: String?
    var exchange: String?
    let symptomGroupFive: String?
    let jibun: String?
    let depositConfirm: String?
    let serviceMatPlan: String?
    let lastConsultant: String?
    let component: String?
    let depositDetailHistory: String?
    let isVip: String?
    let matRequest: String?
    let upgrade: String?
    let deliveryProduct: String?
    let symptomGroupFour: String?
    let optionData: String?
    let productName: String?
    let costAmount: String?
    let matDeliveryNum: String?
    let picturePathOne: String?
    let matRequestConfirm: String?
    let costNCost: String?
    let divisionEngineer: String?
    let paymentType: String?
    let firmware: String?
    let repairCompleteDate: String?
    let manufacturer: String?
    let hPhoneNo: String?
    let symptom: String?
    let repairHistory: String?
    let area: String?
    let progressNo: String?
    let symptomGroupTwo: String?
    let telNo: String?
    let picturePathFour: String?
    let startTime: String?
    let symptomGroupThree: String?
    let etcClausal: String?
    let makeProductDate: String?
    let userName: String?
    let inDate: String?
    let symptomGroupOne: String?
    let endTime: String?
    let delYn: String?
    let engineerCharge: String?
    let filePath: String?
    let receiveDate: String?
    let matDeliveryType: String?
    let cashReceiptType: String?
    let salesTeamConfirm: String?
    let initSymptom: String?
    let treatHistory: String?
    let receptionType: String?
    let useProductType: String?
    let salesCheck: String?
    let orderNo: String?
    let custNo: String?

    enum CodingKeys: String, CodingKey {
        case oldProductName = "OLD_PRODUCT_NAME"
        case receiveSeq = "RECEIVE_SEQ"
        case picturePathThree = "PICTURE_PATH_THREE"
        case inProductDate = "IN_PRODUCT_DATE"
        case purchasingOffice = "PURCHASING_OFFICE"
        case visitDate = "VISIT_DATE"
        case productSn = "PRODUCT_SN"
        case modConsultant = "MOD_CONSULTANT"
        case oldManufacturer = "OLD_MANUFACTURER"
        case picturePathTwo = "PICTURE_PATH_TWO"
        case callType = "CALL_TYPE"
        case longTermReq = "LONG_TERM_REQ"
        case addr = "ADDR"
        case accessIp = "ACCESS_IP"
        case symptomGroupSix = "SYMPTOM_GROUP_SIX"
        case cashReceiptCardNum = "CASH_RECEIPT_CARD_NUM"
        case depositCheck = "DEPOSIT_CHECK"
        case isComplain = "IS_COMPLAIN"
        case visitTime = "VISIT_TIME"
        case accessId = "ACCESS_ID"
        case serviceMonth = "SERVICE_MONTH"
        case exchange = "EXCHANGE"
        case symptomGroupFive = "SYMPTOM_GROUP_FIVE"
        case jibun = "JIBUN"
        case depositConfirm = "DEPOSIT_CONFIRM"
        case serviceMatPlan = "SERVICE_MAT_PLAN"
        case lastConsultant = "LAST_CONSULTANT"
        case component = "COMPONENT"
        case depositDetailHistory = "DEPOSIT_DETAIL_HISTORY"
        case isVip = "IS_VIP"
        case matRequest = "MAT_REQUEST"
        case upgrade = "UPGRADE"
        case deliveryProduct = "DELIVERY_PRODUCT"
        case symptomGroupFour = "SYMPTOM_GROUP_FOUR"
        case optionData = "OPTIONDATA"
        case productName = "PRODUCT_NAME"
        case costAmount = "COST_AMOUNT"
        case matDeliveryNum = "MAT_DELIVERY_NUM"
        case picturePathOne = "PICTURE_PATH_ONE"
        case matRequestConfirm = "MAT_REQUEST_CONFIRM"
        case costNCost = "COSTNCOST"
        case divisionEngineer = "DIVISION_ENGINEER"
        case paymentType = "PAYMENT_TYPE"
        case firmware = "FIRMWARE"
        case repairCompleteDate = "REPAIR_COMPLETE_DATE"
        case manufacturer = "MANUFACTURER"
        case hPhoneNo = "HPHONE_NO"
        case symptom = "SYMPTOM"
        case repairHistory = "REPAIR_HISTORY"
        case area = "AREA"
        case progressNo = "PROGRESS_NO"
        case symptomGroupTwo = "SYMPTOM_GROUP_TWO"
        case telNo = "TEL_NO"
        case picturePathFour = "PICTURE_PATH_FOUR"
        case startTime = "STARTTIME"
        case symptomGroupThree = "SYMPTOM_GROUP_THREE"
        case etcClausal = "ETCCLAUSAL"
        case makeProductDate = "MAKE_PRODUCT_DATE"
        case userName = "USER_NAME"
        case inDate = "IN_DATE"
        case symptomGroupOne = "SYMPTOM_GROUP_ONE"
        case endTime = "ENDTIME"
        case delYn = "DELYN"
        case engineerCharge = "ENGINEER_CHARGE"
        case filePath = "FILE_PATH"
        case receiveDate = "RECEIVE_DATE"
        case matDeliveryType = "MAT_DELIVERY_TYPE"
        case cashReceiptType = "CASH_RECEIPT_TYPE"
        case salesTeamConfirm = "SALESTEAM_CONFIRM"
        case initSymptom = "INIT_SYMPTOM"
        case treatHistory = "TREAT_HISTORY"
        case receptionType = "RECEPTION_TYPE"
        case useProductType = "USEPRODUCT_TYPE"
        case salesCheck = "SALES_CHECK"
        case orderNo = "ORDER_NO"
        case custNo = "CUST_NO"
    }
}

struct MatListData: Codable {
    let progressNo: String?
    let matNum: Int?
    let matProgress: String?
    let matServicePlan: String?
    let matRequest: String?
    
    enum CodingKeys: String, CodingKey {
        case progressNo = "PROGRESS_NO"
        case matNum = "MAT_NUM"
        case matProgress = "MAT_PROGRESS"
        case matServicePlan = "MAT_SERVICE_PLAN"
        case matRequest = "MAT_REQUEST"
    }
}
