//
//  ManufacturerResponse.swift
//  service
//
//  Created by insung on 26/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct ManufacturerResponse: Codable {
    let result: [ManufacturerData]?
    let error: Bool
    let message: String?
}

struct ManufacturerData: Codable {
    let manufacturerName: String
    let manufacturerCode: String
    
    enum CodingKeys: String, CodingKey {
        case manufacturerName = "manufacturerName"
        case manufacturerCode = "manufacturerCode"
    }
}
