//
//  VipListResponse.swift
//  service
//
//  Created by insung on 2020/07/29.
//  Copyright © 2020 bodyfriend. All rights reserved.
//

import Foundation

struct VipListResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: VipListDatas?
}

struct VipListDatas: Codable {
    let listCount: Int
    let list: [VipListData]?
}

struct VipListData: Codable {
    let isComplain: String
    let area: String
    let costNCost: String
    let userName: String
    let receiveSeq: String
    let addr: String
    let requestDriver: String
    let isVip: String
    let timeSelect: String
    let agreeYn: String
    let oldProductName: String
    let hPhoneNo: String?
    let requestMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case isComplain = "IS_COMPLAIN"
        case area = "AREA"
        case costNCost = "COSTNCOST"
        case userName = "USER_NAME"
        case receiveSeq = "RECEIVE_SEQ"
        case addr = "ADDR"
        case requestDriver = "REQUEST_DRIVER"
        case isVip = "IS_VIP"
        case timeSelect = "TIME_SELECT"
        case agreeYn = "AGREE_YN"
        case oldProductName = "OLD_PRODUCT_NAME"
        case hPhoneNo = "HPHONE_NO"
        case requestMessage = "REQUEST_MESSAGE"
    }
}
