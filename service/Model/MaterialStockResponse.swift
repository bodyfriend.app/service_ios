//
//  MaterialStockResponse.swift
//  service
//
//  Created by insung on 01/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialStockResponse: Codable {
    let data: [MaterialStockData]?
    let error: Bool
    let message: String?
}

struct MaterialStockData: Codable {
    let userYn: String?
    let materialNum: String
    let count: String?
    let id: String?
    let materialName: String
    let type: String?
    let update: String?
    let name: String?
    let materialManufacturer: String?
    let productName: String?
    let returnCount: String?
    
    enum CodingKeys: String, CodingKey {
        case userYn = "USE_YN"
        case materialNum = "MATERIAL_NUM"
        case count = "COUNT"
        case id = "ID"
        case materialName = "MATERIAL_NAME"
        case type = "TYPE"
        case update = "UP_DATE"
        case name = "NAME"
        case materialManufacturer = "MATERIAL_MANUFACTURER"
        case productName = "PRODUCT_NAME"
        case returnCount = "RETURN_COUNT"
    }
}



