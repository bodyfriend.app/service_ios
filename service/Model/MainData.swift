//
//  MainData.swift
//  service
//
//  Created by insung on 2018. 7. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct MainData: Codable {
    var cntL: String?
    var cntM: String?
    var cntH: String?
    var cntU: String?
    var cntA: String?
    var cntUH: String?
    
    enum CodingKeys: String, CodingKey {
        case cntL = "CNT_L"
        case cntM = "CNT_M"
        case cntH = "CNT_H"
        case cntU = "CNT_U"
        case cntA = "CNT_A"
        case cntUH = "CNT_UH"
    }
}
