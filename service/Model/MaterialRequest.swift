//
//  MaterialRequest.swift
//  service
//
//  Created by insung on 02/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialRequest: Codable {
    var materialName: String?
    var materialNum: String?
    var count: Int?
    var requestType: String?
    var requestName: String?
    var serviceSeq: String?
    var manufacturerName: String?
    var productName: String?
}

