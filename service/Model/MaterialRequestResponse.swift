//
//  MaterialRequestResponse.swift
//  service
//
//  Created by insung on 30/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialRequestResponse: Codable {
    let data: [MaterialRequestData]?
    let error: Bool
    let message: String?
}

struct MaterialRequestData: Codable {
    let requestNum: String
    let requestDate: String
    let state: String
    let count: Int
    let userName: String
    let requestDivCd: String
    let requestType: String?
    let materialName: String
    let receiveDate: String?
    
    enum CodingKeys: String, CodingKey {
        case requestNum = "REQUEST_NUM"
        case requestDate = "REQUEST_DATE"
        case state = "STATE"
        case count = "COUNT"
        case userName = "USER_NAME"
        case requestDivCd = "REQUEST_DIV_CD"
        case requestType = "REQUEST_TYPE"
        case materialName = "MATERIAL_NAME"
        case receiveDate = "RECEIVE_DATE"
    }
}
