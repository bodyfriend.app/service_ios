//
//  DetailHistoryResponse.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct DetailHistoryResponse: Codable {
    let resultCode: String?
    let resultMsg: String?
    var resultData: HistoryListDatas?
}

struct HistoryListDatas: Codable {
    let listCount: Int
    let list: [HistoryListData]?
}

struct HistoryListData: Codable {
    let phoneNo: String?
    let progressNo: String?
    let userName: String?
    let receiveSeq: String?
    var oldProductName: String?
    let repairCompleteDate: String?
    let inProductDate: String?
    let receiveDate: String?
    let serviceMonth: String?
    
    enum CodingKeys: String, CodingKey {
        case phoneNo = "PHONE_NO"
        case progressNo = "PROGRESS_NO"
        case userName = "USER_NAME"
        case receiveSeq = "RECEIVE_SEQ"
        case oldProductName = "OLD_PRODUCT_NAME"
        case repairCompleteDate = "REPAIR_COMPLETE_DATE"
        case inProductDate = "IN_PRODUCT_DATE"
        case receiveDate = "RECEIVE_DATE"
        case serviceMonth = "SERVICE_MONTH"
    }
}
