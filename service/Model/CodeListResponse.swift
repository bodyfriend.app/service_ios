//
//  CodeListResponse.swift
//  service
//
//  Created by insung on 2018. 7. 25..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct CodeListResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: [CodeListData]?
}

struct CodeListData: Codable {
    let insDt: Int?
    let detCd: String?
    let description: String?
    let detCdNm: String?
    var commCdNm: String?
    let updId: String?
    let updDt: Int?
    let groupCd: String?
    let insId: String?
    let sortSeq: String?
    let ref2: String?
    let ref3: String?
    let ref1: String?
    var commCd: String?
    
    enum CodingKeys: String, CodingKey {
        case insDt = "INS_DT"
        case detCd = "DET_CD"
        case description = "DESCRIPTION"
        case detCdNm = "DET_CD_NM"
        case commCdNm = "COMM_CD_NM"
        case updId = "UPD_ID"
        case updDt = "UPD_DT"
        case groupCd = "GROUP_CD"
        case insId = "INS_ID"
        case sortSeq = "SORT_SEQ"
        case ref2 = "REF_2"
        case ref3 = "REF_3"
        case ref1 = "REF_1"
        case commCd = "COMM_CD"
    }
}
