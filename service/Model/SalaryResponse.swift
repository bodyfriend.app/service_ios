//
//  SalaryResponse.swift
//  service
//
//  Created by insung on 2018. 8. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct SalaryResponse: Codable {
    let resultCode: String?
    let resultMsg: String?
    var resultData: [SalaryListData]?
}

struct SalaryListData: Codable {
    let distTot: Int?
    let engineerNm: String?
    let daysInWeek: Int?
    let workingDays: Int?
    let year: Int?
    let distScore: Float?
    let weekOfMonth: Int?
    let weekCnt: Int?
    let weekendCnt: Int?
    let month: Int?
    let weekDaysTot: Int?
    let weekendTot: Int?
    
    enum CodingKeys: String, CodingKey {
        case distTot = "DIST_TOT"
        case engineerNm = "ENGINEER_NM"
        case daysInWeek = "DAYS_IN_WEEK"
        case workingDays = "WORKING_DAYS"
        case year = "YEAR"
        case distScore = "DIST_SCORE"
        case weekOfMonth = "WEEK_OF_MONTH"
        case weekCnt = "WEEK_CNT"
        case weekendCnt = "WEEKEND_CNT"
        case month = "MONTH"
        case weekDaysTot = "WEEKDAYS_TOT"
        case weekendTot = "WEEKEND_TOT"
    }
}
