//
//  MaterialDetailResponse.swift
//  service
//
//  Created by insung on 31/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialDetailResponse: Codable {
    let data: [MaterialDetailData]?
    let error: Bool
    let message: String?
}

struct MaterialDetailData: Codable {
    let requestNum: String
    let wareHouse: String
    let regDate: String
    let state: String
    let manufacturerName: String
    let productName: String
    let outputCount: Int
    let materialNum: String
    let requestName: String
    let requestDate: String
    let receiveDate: String?
    let count: Int
    let requestType: String
    let materialName: String
    let memo: String?
    let stockCount: Int?
    let orderState: String?
    let orderStateName: String?
    let receiveSchduleDate: String?
    let stateCode: String?
    let stateDetail: String?
    let receiveName: String?
    let deliveryState: String?
    
    enum CodingKeys: String, CodingKey {
        case requestNum = "REQUEST_NUM"
        case wareHouse = "WAREHOUSE"
        case regDate = "REG_DATE"
        case state = "STATE"
        case manufacturerName = "MANUFACTURER_NAME"
        case productName = "PRODUCT_NAME"
        case outputCount = "OUTPUT_COUNT"
        case materialNum = "MATERIAL_NUM"
        case requestName = "REQUEST_NAME"
        case requestDate = "REQUEST_DATE"
        case receiveDate = "RECEIVE_DATE"
        case count = "COUNT"
        case requestType = "REQUEST_TYPE"
        case materialName = "MATERIAL_NAME"
        case memo = "MEMO"
        case stockCount = "STOCK_COUNT"
        case orderState = "ORDER_STATE"
        case orderStateName = "ORDER_STATE_NAME"
        case receiveSchduleDate = "RECEIVE_SCHEDULE_DATE"
        case stateCode = "STATE_CODE"
        case stateDetail = "STATE_DETAIL"
        case receiveName = "RECEIVE_NAME"
        case deliveryState = "DELIVERY_STATE"
    }
}
