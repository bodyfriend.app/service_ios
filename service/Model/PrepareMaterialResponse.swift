//
//  PrepareMaterialResponse.swift
//  service
//
//  Created by insung on 05/07/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit

struct PrepareMaterialResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: PrepareMaterialData?
}

struct PrepareMaterialData: Codable {
    let cntArea: String?
    let cntEngineer: String?
    
    enum CodingKeys: String, CodingKey {
        case cntArea = "CNT_AREA"
        case cntEngineer = "CNT_ENGINEER"
    }
}
