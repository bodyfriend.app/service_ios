//
//  PriceSearchKeyResponse.swift
//  service
//
//  Created by insung on 2018. 8. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct PriceSearchKeyResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: PriceSearchKeyData?
}

struct PriceSearchKeyData: Codable {
    let cMap: [String]?
    let mMap: [String]?
}
