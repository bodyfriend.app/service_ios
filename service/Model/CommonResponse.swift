//
//  CommonResponse.swift
//  service
//
//  Created by insung on 2018. 8. 21..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct CommonResponse: Codable {
    let resultCode: String?
    let resultMsg: String?
}

