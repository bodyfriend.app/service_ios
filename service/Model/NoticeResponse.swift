//
//  NoticeResponse.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct NoticeResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: NotiseListDatas?
}

struct NotiseListDatas: Codable {
    let listCount: Int
    let list: [NoticeListData]?
}

struct NoticeListData: Codable {
    let noticeTitle: String?
    let pictureOne: String?
    let rgId: String?
    let noticeContents: String?
    var noticeSeq: Int?
    let noticeNew: String?
    let rgNm: String?
    let rgDt: String?
    let noticeScoreTarget: String?
    
    enum CodingKeys: String, CodingKey {
        case noticeTitle = "NOTICE_TITLE"
        case pictureOne = "PICTURE_ONE"
        case rgId = "RG_ID"
        case noticeContents = "NOTICE_CONTENTS"
        case noticeSeq = "NOTICE_SEQ"
        case noticeNew = "NOTICE_NEW"
        case rgNm = "RG_NM"
        case rgDt = "RG_DT"
        case noticeScoreTarget = "NOTICE_SCOPE_TARGET"
    }
}
