//
//  WarehouseResponse.swift
//  service
//
//  Created by insung on 01/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct WarehouseResponse: Codable {
    let result: [WarehouseData]?
    let error: Bool
    let message: String?
}

struct WarehouseData: Codable {
    let warehouseName: String
    let warehouseSeq: Int
    
    enum CodingKeys: String, CodingKey {
        case warehouseName = "warehouseName"
        case warehouseSeq = "warehouseSeq"
    }
}
