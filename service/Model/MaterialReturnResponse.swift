//
//  MaterialReturnResponse.swift
//  service
//
//  Created by insung on 01/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialReturnResponse: Codable {
    let data: MaterialStockDetailData?
    let error: Bool
    let message: String?
}

struct MaterialStockDetailData: Codable {
    let detail: MaterialStockData
    let returnList: [MaterialReturnData]
}

struct MaterialReturnData: Codable {
    let requestNum: String
    let warehouseSeq: String
    let count: Int
    let warehouseName: String
    
    enum CodingKeys: String, CodingKey {
        case requestNum = "REQUEST_NUM"
        case warehouseSeq = "WAREHOUSE_SEQ"
        case count = "COUNT"
        case warehouseName = "WAREHOUSE_NAME"
    }
}

extension MaterialReturnData: Equatable {
    static func == (lhs: MaterialReturnData, rhs: MaterialReturnData) -> Bool {
        return lhs.requestNum == rhs.requestNum
    }
}
