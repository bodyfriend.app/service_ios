//
//  ProductInfoDetailResponse.swift
//  service
//
//  Created by insung on 2018. 9. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct ProductInfoDetailResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: ProductInfoData?
}

