//
//  PayatData.swift
//  service
//
//  Created by insung on 19/08/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

class PayatData {
    static let sharedInstance = PayatData()
    
    var approval: Bool = false
    var approvalTime: String?
    var orderNo: String?
    var approvalNo: String?
    var cardInstallmentMonth: String?
    var cardName: String?
    var plainCardNo: String?
    var employeeName: String?
}
