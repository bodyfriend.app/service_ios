//
//  MaterialResponse.swift
//  service
//
//  Created by insung on 29/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialResponse: Codable {
    let result: [MaterialData]?
    let error: Bool
    let message: String?
}

struct MaterialData: Codable {
    let materialName: String
    let materialNum: String
    let materialPrice: String
    var count: Int?
    
    enum CodingKeys: String, CodingKey {
        case materialName = "materialName"
        case materialNum = "materialNum"
        case materialPrice = "materialPrice"
    }
}
