//
//  MaterialCommonResponse.swift
//  service
//
//  Created by insung on 01/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct MaterialCommonResponse: Codable {
    let error: Bool
    let message: String?
}
