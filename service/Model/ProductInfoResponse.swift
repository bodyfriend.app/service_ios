//
//  ProductInfoResponse.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct ProductInfoResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: ProductInfoDatas?
}

struct ProductInfoDatas: Codable {
    let listCount: Int
    let list: [ProductInfoData]?
}

struct ProductInfoData: Codable {
    let imagetwo: String?
    let imageone: String?
    let productIntro: String?
    let productImageS: String?
    let productImageL: String?
    let productType: String?
    let productName: String?
    let productSpec: String?
    let productCode: String?
    let productDetail: String?
    let productCaution1Mov: String?
    let productCaution2Mov: String?
    let productCaution3Mov: String?
    let productCaution4Mov: String?
    let productCaution5Mov: String?
    let productCaution1: String?
    let productCaution2: String?
    let productCaution3: String?
    let productCaution4: String?
    let productCaution5: String?
    
    enum CodingKeys: String, CodingKey {
        case imagetwo = "imagetwo"
        case imageone = "imageone"
        case productIntro = "PRODUCT_INTRO"
        case productImageS = "PRODUCT_IMAGE_S"
        case productImageL = "PRODUCT_IMAGE_L"
        case productType = "PRODUCT_TYPE"
        case productName = "PRODUCT_NAME"
        case productSpec = "PRODUCT_SPEC"
        case productCode = "PRODUCT_CODE"
        case productDetail = "PRODUCT_DETAIL"
        case productCaution1Mov = "PRODUCT_CAUTION5_MOV"
        case productCaution2Mov = "PRODUCT_CAUTION4_MOV"
        case productCaution3Mov = "PRODUCT_CAUTION3_MOV"
        case productCaution4Mov = "PRODUCT_CAUTION2_MOV"
        case productCaution5Mov = "PRODUCT_CAUTION1_MOV"
        case productCaution1 = "PRODUCT_CAUTION1"
        case productCaution2 = "PRODUCT_CAUTION2"
        case productCaution3 = "PRODUCT_CAUTION3"
        case productCaution4 = "PRODUCT_CAUTION4"
        case productCaution5 = "PRODUCT_CAUTION5"
    }
}
