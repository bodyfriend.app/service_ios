//
//  ProductResponse.swift
//  service
//
//  Created by insung on 29/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

struct ProductResponse: Codable {
    let result: [ProductData]?
    let error: Bool
    let message: String?
}

struct ProductData: Codable {
    let productName: String
    let productCode: String
    
    enum CodingKeys: String, CodingKey {
        case productName = "productName"
        case productCode = "productCode"
    }
}
