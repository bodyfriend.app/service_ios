//
//  CertResponse.swift
//  service
//
//  Created by insung on 2018. 9. 12..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct CertResponse: Codable {
    let data: CertCodeData?
    let status: CertStatusData?
}

struct CertCodeData: Codable {
    var authCode: String?
    var msgid: String?
}

struct CertStatusData: Codable {
    var code: String?
    var message: String?
}
