//
//  SaleHistoryResponse.swift
//  service
//
//  Created by insung on 25/02/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

struct SaleHistoryResponse: Codable {
    let resultCode: String?
    var resultData: SaleHistoryListData
}

struct SaleHistoryListData: Codable {
    let listCount: Int?
    let list: [SaleHistoryData]?
}

struct SaleHistoryData: Codable {
    let inUserName: String?
    let inTel: String?
    let salesType: String?
    let productName: String?
    let inHandphone: String?
    let managerName: String?
    let nType: String?
    let salesSeq: Int?
    let regDate: String?
    let etcClausal: String?
    let productType: String?
    let salesConfirm: String?
    let receivePs: String?
    let productTypeNm: String?
    let inUserNo: String?
    
    enum CodingKeys: String, CodingKey {
        case inUserName = "IN_USER_NAME"
        case inTel = "IN_TEL"
        case salesType = "SALES_TYPE"
        case productName = "PRODUCT_NAME"
        case inHandphone = "IN_HANDPHONE"
        case managerName = "MANAGER_NAME"
        case nType = "N_TYPE"
        case salesSeq = "SALES_SEQ"
        case regDate = "REG_DATE"
        case etcClausal = "ETCCLAUSAL"
        case productType = "PRODUCT_TYPE"
        case salesConfirm = "SALES_CONFIRM"
        case receivePs = "RECEIVE_PS"
        case productTypeNm = "PRODUCT_TYPE_NM"
        case inUserNo = "IN_USER_NO"
    }
}
