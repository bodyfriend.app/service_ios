//
//  ServiceListResponse.swift
//  service
//
//  Created by insung on 2018. 7. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct ServiceListResponse: Codable {
    let resultCode: String
    let resultMsg: String
    let resultData: ServiceListDatas?
}

struct ServiceListDatas: Codable {
    let listCount: Int
    let list: [ServiceListData]?
}

struct ServiceListData: Codable {
    let upgrade: String?
    let isComplain: String?
    let progressNo: String?
    let receiveSeq: String?
    let oldProductName: String?
    let etc: String?
    let visitTime: String?
    let newFlag: String?
    let visitDate: String?
    let expireFlag: String?
    let has2Msg: String?
    let receiveType: String?
    let mReceiveType: String?
    let matRequestConfirm: String?
    let phoneNo: String?
    let userName: String?
    let costNCost: String?
    let divisionEngineer: String?
    let firmware: String?
    let callType: String?
    let longTermReq: String?
    let addr: String?
    let receiveDate: String?
    let callTypeReqDt: String?
    let matRequestConfirm2: String?
    let overhaulType: String?
    let overhaulTypeSub: String?
    
    enum CodingKeys: String, CodingKey {
        case upgrade = "UPGRADE"
        case isComplain = "IS_COMPLAIN"
        case progressNo = "PROGRESS_NO"
        case receiveSeq = "RECEIVE_SEQ"
        case oldProductName = "OLD_PRODUCT_NAME"
        case etc = "ETC"
        case visitTime = "VISIT_TIME"
        case newFlag = "NEW_FLAG"
        case visitDate = "VISIT_DATE"
        case expireFlag = "EXPIRE_FLAG"
        case has2Msg = "HAS_2_MSG"
        case receiveType = "RECEIVE_TYPE"
        case mReceiveType = "m_receivetype"
        case matRequestConfirm = "MAT_REQUEST_CONFIRM"
        case phoneNo = "PHONE_NO"
        case userName = "USER_NAME"
        case costNCost = "COSTNCOST"
        case divisionEngineer = "DIVISION_ENGINEER"
        case firmware = "FIRMWARE"
        case callType = "CALL_TYPE"
        case longTermReq = "LONG_TERM_REQ"
        case addr = "ADDR"
        case receiveDate = "RECEIVE_DATE"
        case callTypeReqDt = "CALL_TYPE_REQDT"
        case matRequestConfirm2 = "MAT_REQUEST_CONFIRM_2"
        case overhaulType = "OVERHAUL_TYPE"
        case overhaulTypeSub = "OVERHAUL_TYPE_SUB"
    }
}

extension ServiceListData: Equatable {
    static func == (lhs: ServiceListData, rhs: ServiceListData) -> Bool {
        return lhs.receiveSeq == rhs.receiveSeq &&
        lhs.userName == rhs.userName
    }
}


