//
//  ServiceListData.swift
//  service
//
//  Created by insung on 2018. 7. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct ServiceListData: Codable {
    var upgrade: String?
    var isComplain: String?
    var progressNo: String?
    var receiveSeq: String?
    var oldProductName: String?
    var etc: String?
    var visitTime: String?
    var newFlag: String?
    var visitDate: String?
    var expireFlag: String?
    var has2Msg: String?
    var receiveType: String?
    var matRequestConfirm: String?
    var phoneNo: String?
    var userName: String?
    var costNCost: String?
    var divisionEngineer: String?
    var firmware: String?
    var callType: String?
    var longTermReq: String?
    var addr: String?
    var receiveDate: String?
    
    enum CodingKeys: String, CodingKey {
        case upgrade = "UPGRADE"
        case isComplain = "IS_COMPLAIN"
        case progressNo = "PROGRESS_NO"
        case receiveSeq = "RECEIVE_SEQ"
        case oldProductName = "OLD_PRODUCT_NAME"
        case etc = "ETC"
        case visitTime = "VISIT_TIME"
        case newFlag = "NEW_FLAG"
        case visitDate = "VISIT_DATE"
        case expireFlag = "EXPIRE_FLAG"
        case has2Msg = "HAS_2_MSG"
        case receiveType = "RECEIVE_TYPE"
        case matRequestConfirm = "MAT_REQUEST_CONFIRM"
        case phoneNo = "PHONE_NO"
        case userName = "USER_NAME"
        case costNCost = "COSTNCOST"
        case divisionEngineer = "DIVISION_ENGINEER"
        case firmware = "FIRMWARE"
        case callType = "CALL_TYPE"
        case longTermReq = "LONG_TERM_REQ"
        case addr = "ADDR"
        case receiveDate = "RECEIVE_DATE"
    }
}

