//
//  PriceTagResponse.swift
//  service
//
//  Created by insung on 2018. 8. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct PriceTagResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: [PriceTagListData]?
}

struct PriceTagListData: Codable {
    let chargeAmount: String?
    let modelNames: String?
    let upDate: String?
    let workplace: String?
    let regDate: String?
    let isUse: String?
    let num: Int?
    let upId: String?
    let manufacturer: String?
    let classification: String?
    let item: String?
    let rentalPeriod: String?
    let regId: String?
    let priceSeq: Int?

    enum CodingKeys: String, CodingKey {
        case chargeAmount = "charge_amount"
        case modelNames = "model_names"
        case upDate = "up_date"
        case workplace = "workplace"
        case regDate = "reg_date"
        case isUse = "is_use"
        case num = "num"
        case upId = "up_id"
        case manufacturer = "manufacturer"
        case classification = "classification"
        case item = "item"
        case rentalPeriod = "rental_period"
        case regId = "reg_id"
        case priceSeq = "price_seq"
    }
}
