//
//  BusinessLogResponse.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct BusinessLogResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: BusinessLogDatas?
}

struct BusinessLogDatas: Codable {
    let listCount: Int?
    var list: [DetailData]?
}
