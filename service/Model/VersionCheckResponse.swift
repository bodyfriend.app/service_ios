//
//  VersionCheckResponse.swift
//  service
//
//  Created by insung on 2018. 9. 11..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct VersionCheckResponse: Codable {
    let resultCode: String
    let resultMsg: String
    var resultData: VersionData?
}

struct VersionData: Codable {
    var msg: String?
    var version: String?
    var updateType: String?
}
