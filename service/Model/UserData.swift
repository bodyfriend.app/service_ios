//
//  UserData.swift
//  service
//
//  Created by insung on 2018. 7. 18..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

struct UserData: Codable {
    var carNumber: String?
    var telNo: String
    var resultMsg: String
    var sId: String
    var perCd: String
    var groupCd: String
    var adminNm: String
    var companyNm: String?
    var iphoneUuid: String?
    var resultCode: String
    var loginDate: String
    var pushRegId: String?
    var adminId: String
    
    enum CodingKeys: String, CodingKey {
        case carNumber = "CAR_NUMBER"
        case telNo = "TEL_NO"
        case resultMsg = "resultMsg"
        case sId = "SID"
        case perCd = "PER_CD"
        case groupCd = "GROUP_CD"
        case adminNm = "ADMIN_NM"
        case companyNm = "COMPANY_NM"
        case iphoneUuid = "IPHONE_UUID"
        case resultCode = "resultCode"
        case loginDate = "loginDate"
        case pushRegId = "PUSH_REGID"
        case adminId = "ADMIN_ID"
    }
}
