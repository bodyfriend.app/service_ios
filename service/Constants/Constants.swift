//
//  Constants.swift
//  service
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import SnapKit

struct Constants {
    struct URL {
        #if DEBUG
//        static let BASE = "http://121.138.34.30:8080/"
        static let BASE = "http://172.30.40.29:8280/"             // 유경
//        static let BASE = "http://172.30.40.37:8081/"             // 재영
//        static let BASE = "http://192.168.1.88:8084/"               // 동근
        static let BASE_MATERIAL = "http://192.168.1.88:8088/"

        #else
        static let BASE = "https://svc.bfservice.co.kr/"
        static let BASE_MATERIAL = "http://121.138.34.175:8080/"
        #endif
        
        static let BASE_CERTIFICATION = "http://121.138.34.14/"
        static let LOGIN = "login/appLogin.json"
        static let CERTIFICATION_SEND = "auth/sendMessage"
        static let CERTIFICATION_CONFIRM = "auth/checkAuthCode"
        static let VALIDATION = "auth/checkMsgid"
        static let LOGOUT = "login/appLogout.json"
        static let APP_VERSION = "mobile/api/versionCheck.json"
        static let MAIN_INFO = "mobile/api/appServiceMainInfo.json"
        static let MAIN_UPGRADE = "mobile/api/appServiceUpdateHeatMainInfo.json"
        static let PREPARE_MATERIAL = "mobile/api/selectEngineerAreaMtrl.json"
        static let SERVICE_LIST = "mobile/api/appReceiveList.json"
        static let UPGRADE_LIST = "mobile/api/appUpdateHeatReceiveList.json"
        static let OVERHEAT_LIST = "mobile/api/appLegHeatReceiveList.json"
        static let VIP_LIST = "mobile/api/selectVIPRList.json"
        static let VIP_AGREE = "mobile/api/agreeVIPrequest.json"
        static let CODE_LIST = "mobile/api/appSearchCodeList.json"
        static let PROMISE_DATE_MODIFY = "mobile/api/modifyVisitDate.json"
        static let PROMISE_TIME_MODIFY = "mobile/api/modifyVisitTime.json"
        static let PROMISE_CANCEL = "mobile/api/cancelVisit.json"
        static let DETAIL = "mobile/api/appReceiveDetail.json"
        static let DETAIL_UPDATE_ADDRESS = "mobile/api/updateAddr.json"
        static let DETAIL_SYMPTOM = "common/searchMCodeList.json"
        static let DETAIL_SYMPTOM_SEARCH = "mobile/api/appSearchMCodeView.json"
        static let DETAIL_RECEIVE_TYPE = "mobile/api/selectRecepitonCodeViewList.json"
        static let DETAIL_MATERIAL = "mobile/api/insertMatMgmt.json"
        static let DETAIL_SERVICE_HISTORY = "mobile/api/selectUserHistory.json"
        static let DETAIL_MODIFY_REPAIR = "mobile/api/modifyRepairhistory.json"
        static let DETAIL_MODIFY_TREAT = "mobile/api/modifyTreathistory.json"
        static let DETAIL_REGISTER = "mobile/api/appModifyReceiveDetail.json"
        static let DETAIL_UPDATE_PICTURE = "mobile/api/updatePicture.json"
        static let DETAIL_MODIFY_PAYAT = "mobile/api/appModifyPayatt.json"
        static let NOTICE_LIST = "mobile/api/appNoticeListNew.json"
        static let PRICE_TAG_LIST = "mobile/api/appServicePriceTagList.json"
        static let PRICE_SEARCH_KEY = "mobile/api/appServicePriceTagSearchKey.json"
        static let CLEAN_CAR = "stats/admin/insertCarStatus.json"
        static let SALARY_LIST = "ServiceMgt/ServiceMonthlyList/selectMonthlyReportList.json"
        static let BUSINESS_LOG_LIST = "mobile/api/BusinessLog.json"
        static let MANUAL_DOWNLOAD_FORMAT = "images/pdf/%@.pdf"
        static let MATERIAL_REQUEST_LIST = "materials/api/requestListByDeliveryman"
        static let MATERIAL_WAREHOUSE_LIST = "materials/api/getWarehouseList"
        static let MATERIAL_MANUFACTURE_LIST = "materials/api/getManufacturerList"
        static let MATERIAL_PRODUCT_LIST = "materials/api/getProductList"
        static let MATERIAL_LIST = "materials/api/getMaterialList"
        static let MATERIAL_REQUEST = "materials/api/insertRequest"
        static let MATERIAL_DELIVERY_REQUEST = "materials/api/updateDeliveryInfo"
        static let MATERIAL_DETAIL_LIST = "materials/api/materialListByRequestno"
        static let MATERIAL_REQUEST_CANCEL = "materials/api/cancelRequest"
        static let MATERIAL_STOCK_LIST = "materials/api/stockList"
        static let MATERIAL_UPDATE_STATE = "materials/api/updateStateRequest"
        static let MATERIAL_USE = "materials/api/materialUseYN"
        static let MATERIAL_RETURN = "materials/api/returnRequest"
        static let MATERIAL_STOCK_DETAIL = "materials/api/materialDetailInfo2"
        static let MATERIAL_CANCEL = "materials/api/returnCancel"
        static let SALE_HISTORY = "mobile/api/selectBizSales.json"
        static let DETAIL_COUPON = "mobile/api/createDriverCupon.json"
        
        static let PRODUCT_INFO_LIST = "mobile/api/appProductList.json"
        static let PRODUCT_INFO_DETAIL = "mobile/api/appProductDetail.json"
        static let SALE_REGISTER = "mobile/api/createBizSale.json"
        static let APP_LOG = "mobile/api/insertWorkMsgLog.json"
        
        static let DELIVERY_KD_WEB = "http://www.kdexp.com/sub3_shipping.asp?stype=1&p_item=%@"
        static let DELIVERY_HJ_WEB = "http://www.hanjin.co.kr/Delivery_html/inquiry/result_waybill.jsp?wbl_num=%@"
        static let MAP_GEOCODING = "https://api2.sktelecom.com/tmap/geo/fullAddrGeo?version=1&format=json&callback=result"
        
        static let APP_DOWNLOAD = "https://svc.bfservice.co.kr/"
        static let PROMOTION = "https://www.bfservice.co.kr/ShippingMgt/ShippingTeamMgt/promotionNotice.view"
    }
    
    struct Define {
        static let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
        
        static let CODE_LOCATION = "400"
        
        static let GOOGLE_MAP_API_ID = "AIzaSyDTVgpdm1sPVicoz0mVaPzALcrAyBxFTZY"
        static let MAP_CLIENT_KEY = "084d65d2-5495-42f7-9ce8-07a72b526823"
        static let CERTIFICATION_AES_KEY = "bfservicekey!@12"
        static let CERTIFICATION_SERVICE_CODE = "SHIPPING"
        static let CERTIFICATION_SECRET_KEY = "27e924340df23acc1776b1d07f9cd304356a4814"
    }
    
    struct Title {
        static let SIDE_MENU_MANUAL = "서비스메뉴얼"
        static let SIDE_MENU_PRICETAG = "서비스 가격표"
        static let SIDE_MENU_SALARY = "실적조회"
        static let SIDE_MENU_CLEAN_CAR = "세차사진 등록"
        static let SIDE_MENU_JOURNAL = "업무일지"
        static let SIDE_MENU_RECIEVE = "유통/전시장 접수"
        static let SIDE_MENU_MATERIAL_HISTORY = "자재/택배 접수 내역"
        static let SIDE_MENU_MATERIAL_REMAIN = "여유 자재 신청"
        static let SIDE_MENU_MATERIAL_STOCK = "보유 자재"
        static let SIDE_MENU_SALE_HISTORY = "제품판매 조회"
        static let SIDE_MENU_LOGOUT = "로그아웃"
        
        static let TOPBAR_CHAIR_LIST = "안마의자"
        static let TOPBAR_PRODUCT_LIST = "생활가전"
        static let TOPBAR_YET_LIST = "미처리"
        static let TOPBAR_VIP_LIST = "VIP 접수함"
        static let TOPBAR_UPGRADE_LIST = "업그레이드"
        static let TOPBAR_OVERHEAT_LIST = "면상발열"
        static let TOPBAR_DETAIL = "BF서비스관리"
        static let TOPBAR_MATERIAL_REMAIN = "여유 자재 신청"
        static let TOPBAR_MATERIAL_RECEIVE = "자재 접수"
        static let TOPBAR_MATERIAL_DELIVERY = "택배 접수"
        
        static let DETAIL_NAME = "이름"
        static let DETAIL_OVERHEAT = "면상발열"
        static let DETAIL_PHONE = "연락 받을 번호"
        static let DETAIL_TEL = "인입번호"
        static let DETAIL_AREA = "지역"
        static let DETAIL_PURCHASE_OFFICE = "구입처"
        static let DETAIL_ADDRESS = "주소"
        static let DETAIL_CALL_TYPE = "요청타입"
        static let DETAIL_DELIVERY = "분배기사"
        static let DETAIL_CONTRACT = "계약자 동일여부"
        static let DETAIL_ETC = "기타요청사항"
        static let DETAIL_CONSULTANT = "접수상담원"
        static let DETAIL_RECEIVE_DATE = "접수일"
        static let DETAIL_VISIT_DATE = "방문예정일"
        static let DETAIL_VISIT_TIME = "방문예정시간"
        static let DETAIL_RECEIVE_DETAIL = "접수내역"
        static let DETAIL_CONTACT_HISTORY = "접촉이력"
        static let DETAIL_LONG_ASK = "장기 요청사항"
        static let DETAIL_MODEL_NAME = "모델명"
        static let DETAIL_SERIAL_NUM = "S/N"
        static let DETAIL_PURCHASE_DATE = "구입일"
        static let DETAIL_AS = "무상 서비스 기간"
        static let DETAIL_MANUFACTURE = "제조사"
        
        static let DETAIL_MATERIAL_STATUS = "진행상황"
        static let DETAIL_MATERIAL_ASK = "요청자재"
        static let DETAIL_MATERIAL_PLAN = "자재처리계획"
        
        static let POPUP_DETAIL_SYMPTOM = "증상 선택"
        static let POPUP_DETAIL_HISTORY = "서비스이력 리스트"
        
        static let MANUAL_PHARAOH = "파라오 / 팬텀"
        static let MANUAL_REGINA = "레지나"
        static let MANUAL_PRESIDENT = "프레지던트(플러스)"
        static let MANUAL_FIRSTLADY = "퍼스트레이디"
        static let MANUAL_IROBO = "아이로보(S)"
        static let MANUAL_ROSEMARY = "로즈마리"
        static let MANUAL_CRUZE = "뉴크루즈파워"
        
        static let BUSINESS_LOG_NUMBER = "번호"
        static let BUSINESS_LOG_COMPLETE_DATE = "수리 완료일"
        static let BUSINESS_LOG_NAME = "이름"
        static let BUSINESS_LOG_PURCHASE_DATE = "구입일"
        static let BUSINESS_LOG_AS = "무상기간"
        static let BUSINESS_LOG_MANUFACTURE = "제조일"
        static let BUSINESS_LOG_ADDRESS = "주소"
        static let BUSINESS_LOG_RECEIVE_DATE = "접수일"
        static let BUSINESS_LOG_VISIT_DATE = "방문예정일"
        static let BUSINESS_LOG_TEL1 = "연락처1"
        static let BUSINESS_LOG_TEL2 = "연락처2"
        static let BUSINESS_LOG_SN = "시리얼"
        static let BUSINESS_LOG_CONSULTANT = "접수자"
        static let BUSINESS_LOG_PRODUCT_NAME = "제품명"
        static let BUSINESS_LOG_COST = "유/무상"
        static let BUSINESS_LOG_DIVIDE = "구분"
        static let BUSINESS_LOG_MAT_REQUEST = "부품내역"
        static let BUSINESS_LOG_REPAIR_HISTORY = "처리내역"
        static let BUSINESS_LOG_END_TIME = "도착시간"
        static let BUSINESS_LOG_START_TIME = "출발시간"
        static let BUSINESS_LOG_ARRIVAL = "도착주행"
        static let BUSINESS_LOG_DISTANCE = "이동거리"
        static let BUSINESS_LOG_DEPOSIT = "입금"
        
        static let POPUP_WAREHOUSE = "창고 선택"
        static let POPUP_MANUFACTURER = "제조사 선택"
        static let POPUP_PRODUCT = "제품 선택"
        static let POPUP_MATERIAL = "자재 선택"
        static let POPUP_MATERIAL_CANCEL = "자재 요청 취소"
        static let POPUP_MATERIAL_MEMO = "요청 메모"
        static let POPUP_MATERIAL_USE = "사용 자재 선택"
        static let POPUP_MATERIAL_RETURN = "반납 자재 선택"
        
        static let PRODUCT_USEABLE = "제품사용가능/불가(선택)"
    }
    
    struct Desc {
        static let LOGOUT = "로그아웃 하시겠습니까?"
        
        static let SMS_VACANCY = "통화 연결이 되지 않아 문자 남깁니다.\n연락 부탁드립니다."
        static let SMS_DIRECT = "[바디프랜드]"
        static let SMS_MATERIAL = "현재 필요한자재 수급중에 있습니다. 수급되면 빠른 스케줄 잡아 방문하겠습니다."
        static let SMS_HAPPYCARE = """
                                    약속드린 해피케어 서비스 쿠폰 발송드립니다.

                                    쿠폰코드 : BFHAPPYCARE
                                    유효기간 : 발행일로부터 14일

                                    ※소셜커머스, 인터넷 쇼핑몰 구매시 사용 불가
                                    """
        
        static let SAFETY_CHECK_01 = "사용 중인 멀티탭에 먼지 유입 / 접지부 노후화 등 화재의 위험이 없는지 확인할 것"
        static let SAFETY_CHECK_02 = "파워코드 플러그 파손 및 전선에 손상, 훼손된 부위가 없는지 확인할 것"
        static let SAFETY_CHECK_03 = "퓨즈 결합 상태 및 전원부 스위치 접점 불량이 없는지 확인할 것"
        static let SAFETY_CHECK_04 = "기기 내부 PCB, 전선, 커넥터 등 화재 위험이 없이 결합 되어있는지 확인할 것"
        static let SAFETY_CHECK_05 = "온열 패드 접힘, 온열 전선, 커넥터 단선 없이 정상적으로 작동하는지 확인할 것"
    }
    
    struct Format {
        static let SMS_INFO = "[바디프랜드 서비스 안내]\n안녕하세요 %@고객님! 바디프랜드 서비스기사 %@입니다.\n"
        static let SMS_BOOK = "%@%@ %@에서 %@ 사이에 방문예정입니다.\n감사합니다."
    }
    
    struct Array {
        static let SIDE_MENU_TITLE_LIST = [Title.SIDE_MENU_MANUAL, Title.SIDE_MENU_PRICETAG, Title.SIDE_MENU_SALARY, Title.SIDE_MENU_CLEAN_CAR, Title.SIDE_MENU_JOURNAL, Title.SIDE_MENU_MATERIAL_HISTORY, Title.SIDE_MENU_MATERIAL_REMAIN, Title.SIDE_MENU_MATERIAL_STOCK, Title.SIDE_MENU_SALE_HISTORY, Title.SIDE_MENU_LOGOUT]
        static let SIDE_MENU_IMAGE_LIST = ["ic_manual", "ic_price", "ic_salary", "ic_wash", "ic_work", "ic_history", "ic_material", "ic_stock", "ic_sale", "ic_logout"]
        static let SEARCH_SORT_LIST = ["접수일순", "방문예정일순"]
        
        static let DETAIL_USER_INFO_TITLE_LIST = [Title.DETAIL_NAME, Title.DETAIL_OVERHEAT, Title.DETAIL_PHONE, Title.DETAIL_TEL, Title.DETAIL_AREA, Title.DETAIL_PURCHASE_OFFICE, Title.DETAIL_ADDRESS]
        static let DETAIL_ASK_INFO_TITLE_LIST = [Title.DETAIL_CALL_TYPE, Title.DETAIL_DELIVERY, Title.DETAIL_CONTRACT, Title.DETAIL_ETC, Title.DETAIL_CONSULTANT, Title.DETAIL_RECEIVE_DATE, Title.DETAIL_VISIT_DATE, Title.DETAIL_VISIT_TIME, Title.DETAIL_RECEIVE_DETAIL, Title.DETAIL_CONTACT_HISTORY, Title.DETAIL_LONG_ASK]
        static let DETAIL_PRODUCT_INFO_TITLE_LIST = [Title.DETAIL_MODEL_NAME, Title.DETAIL_SERIAL_NUM, Title.DETAIL_PURCHASE_DATE, Title.DETAIL_AS, Title.DETAIL_MANUFACTURE]
        
        static let DETAIL_MATERIAL_TITLE_LIST = [Title.DETAIL_MATERIAL_STATUS, Title.DETAIL_MATERIAL_ASK, Title.DETAIL_MATERIAL_PLAN]
        
        static let DETAIL_PHOTO_TITLE_LIST = ["시리얼번호", "전체사진", "첨부1", "첨부2"]
        static let CAMERA_OPTION_LIST = ["카메라", "앨범"]
        
        static let DETAIL_SYMPTOM_YET_LIST = ["맞교체", "회수수리", "수리거부", "반품요청", "공백"]
        static let DETAIL_COMPLAIN_STATE_LIST = ["취소접수", "접수", "방문약속", "미처리", "수리완료", "자재확인"]
        static let DETAIL_COMPLAIN_UNTREAT_STATE_LIST = ["취소접수", "방문약속", "수리완료", "미처리"]
        
        static let DETAIL_COST_TYPE_LIST = ["무상", "유상"]
        static let DETAIL_DEPOSIT_TYPE_LIST = ["카드", "계좌"]
        static let DETAIL_RECEIPT_TYPE_LIST = ["핸드폰", "카드"]
        
        static let MANUAL_LIST = [Title.MANUAL_PHARAOH, Title.MANUAL_REGINA, Title.MANUAL_PRESIDENT, Title.MANUAL_FIRSTLADY, Title.MANUAL_IROBO, Title.MANUAL_ROSEMARY, Title.MANUAL_CRUZE]
        
        static let CLEAN_CAR_PHOTO_TITLE_LIST = ["전면", "후면", "운전석", "좌측면", "우측면", "트렁크"]
        static let BUSINESS_LOG_TITLE_LIST = [Title.BUSINESS_LOG_NUMBER, Title.BUSINESS_LOG_COMPLETE_DATE, Title.BUSINESS_LOG_NAME, Title.BUSINESS_LOG_PURCHASE_DATE, Title.BUSINESS_LOG_AS, Title.BUSINESS_LOG_MANUFACTURE, Title.BUSINESS_LOG_ADDRESS, Title.BUSINESS_LOG_RECEIVE_DATE, Title.BUSINESS_LOG_VISIT_DATE, Title.BUSINESS_LOG_TEL1, Title.BUSINESS_LOG_TEL2, Title.BUSINESS_LOG_SN, Title.BUSINESS_LOG_CONSULTANT, Title.BUSINESS_LOG_PRODUCT_NAME, Title.BUSINESS_LOG_COST, Title.BUSINESS_LOG_DIVIDE, Title.BUSINESS_LOG_MAT_REQUEST, Title.BUSINESS_LOG_REPAIR_HISTORY, Title.BUSINESS_LOG_END_TIME, Title.BUSINESS_LOG_START_TIME, Title.BUSINESS_LOG_ARRIVAL, Title.BUSINESS_LOG_DISTANCE, Title.BUSINESS_LOG_DEPOSIT]
        static let PRODUCT_INFO_DETAIL_TITLE_LIST = ["제품명", "제품스펙", "제품설명", "주의사항"]
        static let SALE_TYPE_LIST = ["렌탈", "판매"]
        static let SALE_NEW_TYPE_LIST = ["기존", "신규"]
        
        static let SAFETY_CHECK_LIST = [Desc.SAFETY_CHECK_01, Desc.SAFETY_CHECK_02, Desc.SAFETY_CHECK_03, Desc.SAFETY_CHECK_04, Desc.SAFETY_CHECK_05]
        static let PRODUCT_USEABLE_LIST = [Title.PRODUCT_USEABLE, "가능", "불가"]
        static let GIFT_LIST = ["증정", "미증정"]
    }
    
    struct Key {
        static let KEY_USER_DATA = "userData"
        static let KEY_AUTO_LOGIN = "autoLogin"
        static let KEY_LOGIN_DATE = "loginDate"
    }
    
    struct Identifier {
        static let TEST_ID = "testtt"
        
        static let STORYBOARD_LOGIN_NAME = "Login"
        static let STORYBOARD_MAIN_NAME = "Main"
        static let STORYBOARD_SIDE_MENU_NAME = "SideMenu"
        static let STORYBOARD_LIST_NAME = "List"
        
        static let STORYBOARD_LOGIN = "LoginSID"
        static let STORYBOARD_SIDE_MENU = "SideMenuSID"
        static let STORYBOARD_MAIN = "MainSID"
    }
}

