//
//  AppDelegate.swift
//  service
//
//  Created by insung on 2018. 6. 15..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(Constants.Define.GOOGLE_MAP_API_ID)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        LoadingView.hideLoadingView()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let dic = PayatSDkManager.translationDictionary(url) {
            let payat = PayatData.sharedInstance
            var message: String?
            if Int(dic["code"] as! String) == 200 {
                message = "결제에 성공하였습니다"
                if let data = dic["data"] as? [AnyHashable : Any] {
                    payat.approval = true
                    payat.approvalTime = data["approval_time"] as? String
                    payat.orderNo = data["order_no"] as? String
                    payat.approvalNo = data["approval_no"] as? String
                    payat.cardInstallmentMonth = data["card_installment_month"] as? String
                    payat.cardName = data["card_name"] as? String
                    payat.plainCardNo = data["plain_card_no"] as? String
                    if let employee = data["employee"] as? [AnyHashable : Any] {
                        payat.employeeName = employee["name"] as? String
                    }
                    NotificationCenter.default.post(name: Notification.Name("ModifyPayat"), object: nil)
                }
            } else {
                payat.approval = false
                message = dic["message"] as? String
            }
            CommonUtil.showMessage(message!)
        }
        
        return true
    }
}
