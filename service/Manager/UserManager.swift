//
//  UserManager.swift
//  service
//
//  Created by insung on 2018. 7. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class UserManager: NSObject {

    class func saveData(data: Any, key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: key)
        userDefaults.synchronize()
    }
    
    class func removeDataForKey(key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
    }
    
    class func loadUserData(key: String) -> UserData? {
        let userDefaults = UserDefaults.standard
        guard let data = userDefaults.object(forKey: key) as? Data else {
            return nil
        }
        guard let json = try? JSONDecoder().decode(UserData.self, from: data) else {
            return nil
        }
        return json
    }
    
    class func removeUserData() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Constants.Key.KEY_USER_DATA)
        userDefaults.removeObject(forKey: Constants.Key.KEY_LOGIN_DATE)
        userDefaults.synchronize()
    }
}
