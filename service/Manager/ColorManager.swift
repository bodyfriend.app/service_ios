//
//  ColorManager.swift
//  service
//
//  Created by insung on 2018. 7. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

let COLOR_BACKGROUND_GRAY = UIColor(rgb: 0xDDDDDD)
let COLOR_DARKGRAY = UIColor(rgb: 0x434343)
let COLOR_BORDER_GRAY = UIColor(rgb: 0xB5B5B5)
let COLOR_RED = UIColor(rgb: 0xE12828)
let COLOR_ORANGE = UIColor(rgb: 0xEE9050)
let COLOR_BACKGROUND_ORANGE = UIColor(rgb: 0xFFC864, a:0.3)
let COLOR_GREEN = UIColor(rgb: 0xAAD2AA)
let COLOR_BACKGROUND_GREEN = UIColor(rgb: 0xB4F096, a:0.3)
let COLOR_BORDER_RED = UIColor(rgb: 0xFF7878)
let COLOR_BACKGROUND_RED = UIColor(rgb: 0xFFF2F2)
let COLOR_BLUE = UIColor(rgb: 0x2763FF)
let COLOR_BORDER_BLUE = UIColor(rgb: 0x6595D5)
let COLOR_BORDER_PURPLE = UIColor(rgb: 0xA286D6)
let COLOR_BLACK = UIColor(rgb: 0x000000)
let COLOR_WHITE = UIColor(rgb: 0xFFFFFF)

extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    convenience init(rgb: UInt, a: CGFloat) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(a)
        )
    }
}
