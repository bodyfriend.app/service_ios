//
//  ApiManager.swift
//  service
//
//  Created by insung on 2018. 6. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import Alamofire.Swift

class ApiManager {
    
    class func requestToPost(_ url: String, parameters: [String : Any]?, completion: @escaping (Data?) -> Void) {
        self.requestToPost(url, parameters: parameters, isLoading: true, completion: completion)
    }
    
    class func requestToPost(_ url: String, parameters: [String : Any]?, isLoading: Bool, completion: @escaping (Data?) -> Void) {
        DispatchQueue.main.async {
            KeyboardManager.hide()
            if isLoading == true {
                LoadingView.showLoadingView()
            }
        }
        let requestUrl = Constants.URL.BASE + url;
        print("requestUrl :", requestUrl)
        if let p = parameters {
            print("parameters :", p)
        }
        Alamofire.request(requestUrl, method: .post, parameters: parameters)
            .responseData { (response) in
                guard let value = response.result.value else {
                    self.failedRequest(isLoading)
                    return
                }
                let result = try? JSONDecoder().decode(CommonResponse.self, from: value)
                guard let code = result?.resultCode else {
                    completion(response.result.value!)
                    return
                }
                let codeValue = Int(code)
                switch codeValue {
                case 309:
                    if url.contains(Constants.URL.LOGIN) == false {
                        CommonUtil.moveLoginViewController()
                        LoadingView.hideLoadingView()
                    }
                    break
                case -1:
                    self.failedRequest(isLoading)
                    break
                default:
                    completion(response.result.value!)
                    break
                }
                return
        }
    }
    
    class func requestToMultipart(_ url: String, parameters: [String : Any]?, uploadImages: [String : UIImage], completion: @escaping (Data?) -> Void) {
        DispatchQueue.main.async {
            KeyboardManager.hide()
            LoadingView.showLoadingView()
        }
        let requestUrl = Constants.URL.BASE + url;
        print("requestUrl :", requestUrl)
        if let p = parameters {
            print("parameters :", p)
        }
        let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            self.addImageData(multipartFormData: multipartFormData, images: uploadImages)
        }, usingThreshold: UInt64.init(), to: requestUrl, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseData { (response) in
                    switch response.result {
                    case .success:
                        completion(response.data)
                    case .failure:
                        self.failedRequest(true)
                    }
                }
            case .failure(_):
                CommonUtil.showMessage("파일 업로드에 실패하였습니다")
                LoadingView.hideLoadingView()
            }
        }
    }
    
    class func requestToCertification(_  url: String, parameters: [String : Any]?, completion: @escaping (Data?) -> Void) {
        DispatchQueue.main.async {
            KeyboardManager.hide()
            LoadingView.showLoadingView()
        }
        
        let requestUrl = Constants.URL.BASE_CERTIFICATION + url;
        print("requestUrl :", requestUrl)
        if let p = parameters {
            print("parameters :", p)
        }
        let headers: HTTPHeaders = ["Content-type" : "application/json", "serviceCode" : Constants.Define.CERTIFICATION_SERVICE_CODE, "secretKey" : Constants.Define.CERTIFICATION_SECRET_KEY]
        Alamofire.request(requestUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseData { (response) in
                guard let result = try? JSONDecoder().decode(CertResponse.self, from: response.data!) else {
                    return
                }
                if result.status?.code == "200" {
                    completion(response.result.value)
                } else if result.status?.code == "ERR_MS_6001" {
                    CommonUtil.showMessage("인증번호가 일치하지 않습니다")
                    LoadingView.hideLoadingView()
                } else if result.status?.code == "ERR_MS_6003" {
                    if let msg = result.status?.message {
                        CommonUtil.showMessage(msg)
                    }
                    completion(nil)
                } else {
                    if let msg = result.status?.message {
                        CommonUtil.showMessage(msg)
                    }
                    LoadingView.hideLoadingView()
                }
        }
    }
    
    class func requestToMaterial(_ url: String, parameters: [String : Any]?, completion: @escaping (Data?) -> Void) {
        DispatchQueue.main.async {
            KeyboardManager.hide()
            LoadingView.showLoadingView()
        }
        
        let requestUrl = Constants.URL.BASE_MATERIAL + url;
        print("requestUrl :", requestUrl)
        if let p = parameters {
            print("parameters :", p)
        }
        
        Alamofire.request(requestUrl, method: .get, parameters: parameters)
            .responseData { (response) in
                guard let value = response.result.value else {
                    self.failedRequest(true)
                    return
                }
                
                let result = try? JSONDecoder().decode(CommonResponse.self, from: value)
                guard let code = result?.resultCode else {
                    completion(response.result.value!)
                    return
                }
                let codeValue = Int(code)
                switch codeValue {
                case 309:
                    if url.contains(Constants.URL.LOGIN) == false {
                        CommonUtil.moveLoginViewController()
                        LoadingView.hideLoadingView()
                    }
                    break
                case -1:
                    self.failedRequest(true)
                    break
                default:
                    completion(response.result.value!)
                    break
                }
                return
        }
    }
    
    class func downloadFile(_ url: String, completion: @escaping (URL?) -> Void) {
        DispatchQueue.main.async {
            KeyboardManager.hide()
            LoadingView.showLoadingView()
        }
        
        let requestUrl = Constants.URL.BASE + url;
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        Alamofire.download(requestUrl, to: destination).response(completionHandler: { (response) in
            completion(response.destinationURL)
            LoadingView.hideLoadingView()
        })
    }
    
    private class func addImageData(multipartFormData: MultipartFormData, images: [String : UIImage]!) {
        for (key, image) in images {
            let data = image.jpegData(compressionQuality: 0.5)
            let fileName = String(format: "%@.jpg", key)
            multipartFormData.append(data!, withName: key, fileName: fileName, mimeType: "image/jpeg")
        }
    }
    
    private class func failedRequest(_ isLoading: Bool) {
        CommonUtil.showMessage("통신이 원활하지 않습니다")
        if isLoading == true {
            LoadingView.hideLoadingView()
        }
    }
}
