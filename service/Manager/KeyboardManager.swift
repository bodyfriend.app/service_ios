//
//  KeyboardManager.swift
//  service
//
//  Created by insung on 2018. 7. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

enum ToolbarType: Int {
    case close
    case done
}

class KeyboardManager {
    class func hide() {
        Constants.Define.APPDELEGATE.window?.endEditing(true)
    }
    
    class func addObserver(_ targer: BaseViewController) {
        NotificationCenter.default.addObserver(targer, selector: #selector(targer.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(targer, selector: #selector(targer.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    class func removeObserver(_ target: Any) {
        NotificationCenter.default.removeObserver(target, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(target, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    class func setAccessoryView(view: Any?, target: Any?) {
        self.setAccessoryView(view: view, target: target, type: .close)
    }
    
    class func setAccessoryView(view: Any?, target: Any?, type: ToolbarType) {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (Constants.Define.APPDELEGATE.window?.bounds.size.width)!, height: 44))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        if type == .close {
            let closeBtn = UIBarButtonItem(title: "닫기", style: UIBarButtonItem.Style.plain, target: target, action: #selector(pressedClose(_:)))
            toolbar.items = [space, closeBtn]
        } else if type == .done {
            let cancelBtn = UIBarButtonItem(title: "취소", style: UIBarButtonItem.Style.plain, target: target, action: #selector(pressedCancel(_:)))
            let doneBtn = UIBarButtonItem(title: "완료", style: UIBarButtonItem.Style.plain, target: target, action: #selector(pressedDone(_:)))
            toolbar.items = [space, cancelBtn, doneBtn]
        }
        
        if let textField = view as? UITextField {
            textField.inputAccessoryView = toolbar
        } else if let textView = view as? UITextView {
            textView.inputAccessoryView = toolbar
        }
    }

    @objc func pressedClose(_ sender: Any) {}
    @objc func pressedDone(_ sender: Any) {}
    @objc func pressedCancel(_ sender: Any) {}

}

