//
//  PhotoCell.swift
//  service
//
//  Created by insung on 2018. 8. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var mustV: CircleView!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var titleLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }

    func initLayout() {
        photoBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        photoBtn.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    func setData(title: String, image: UIImage?) {
        titleLb.text = title
        if let img = image {
            photoBtn.setImage(img, for: .normal)
        } else {
            let defaultImage = UIImage(named: "ic_in_photo")
            photoBtn.setImage(defaultImage, for: .normal)
        }
        var isMust = false
        if (title == "시리얼번호" ||
            title == "전체사진") && image == nil { // 필수사진에 이미지가 없으면 표기
            isMust = true
        }
        mustV.isHidden = !isMust
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = nil
        photoBtn.setImage(nil, for: .normal)
    }
}
