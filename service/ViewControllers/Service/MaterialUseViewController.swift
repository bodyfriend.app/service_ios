//
//  MaterialUseViewController.swift
//  service
//
//  Created by insung on 06/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialUseViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, ListPopupDelegate {

    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var stockBtn: UIButton!
    @IBOutlet weak var countTF: CTTextField!
    @IBOutlet weak var addBtn: UIButton!
    
    var target: BaseViewController?
    var serviceSeq: String?
    
    var selectedMaterialData: MaterialStockData?
    var addedDatas: [Dictionary<String, Any>]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListPopupSegue" {
            let vc = segue.destination as! ListPopupViewController
            vc.receiveData = sender as? [String : Any]
            vc.delegate = self
        }
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("자재 사용")

        stockBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        addBtn.applyRoundBorder()
        
        let cellNib = UINib(nibName: "MaterialRequestCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "MaterialRequestCell")
        listTbV.tableFooterView = UIView()
    }
    
    func initInputView() {
        stockBtn.setTitle(nil, for: .normal)
        countTF.text = nil
        selectedMaterialData = nil
        KeyboardManager.hide()
    }
    
    func requestStockList() {
        var parameters = Dictionary<String, String>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "admin_id")
        
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_STOCK_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialStockResponse.self, from: response!) {
                guard let data = decode.data else {
                    CommonUtil.showMessage("여유 자재가 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                let sender = ["data" : data as Any, "title" : Constants.Title.POPUP_MATERIAL_USE] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func requestMaterialUse() {
        guard let datas = addedDatas else {
            return
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: datas, options: .prettyPrinted)
        let jsonString = String(data: jsonData, encoding: .utf8)
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(jsonString ?? "", forKey: "list")

        ApiManager.requestToMaterial(Constants.URL.MATERIAL_USE, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialCommonResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("사용 처리 되었습니다")
                }
            }
            LoadingView.hideLoadingView()
            self.closeViewController(self.target)
        })
    }
    
    func changeBtnTitle(_ data: Any) {
        if let materialData = data as? MaterialStockData {
            selectedMaterialData = materialData
            stockBtn?.setTitle(materialData.materialName, for: .normal)
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedStock(_ sender: Any) {
        requestStockList()
    }
    
    @IBAction func pressedAdd(_ sender: UIButton) {
        guard let materialData = selectedMaterialData else {
            CommonUtil.showMessage("자재를 선택해주세요")
            return
        }
        guard let count = Int(countTF.text ?? "0"), count != 0 else {
            CommonUtil.showMessage("수량을 입력해주세요")
            return
        }
        if let dataCount = Int(materialData.count ?? "0"), dataCount < count {
            CommonUtil.showMessage("보유 자재가 부족합니다")
            return
        }

        var parameters = Dictionary<String, Any>()
        if let count = countTF.text {
            parameters.updateValue(count, forKey: "spendCount")
        }
        if let seq = serviceSeq, seq.isExist {
            parameters.updateValue(seq, forKey: "serviceSeq")
        }
        parameters.updateValue(materialData.materialName, forKey: "materialName")
        parameters.updateValue(materialData.materialNum, forKey: "materialNum")
        parameters.updateValue(materialData.userYn ?? "N", forKey: "useYN")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "adminId")

        addedDatas?.append(parameters)
        listTbV.reloadData()
        initInputView()
    }
    
    @IBAction func pressedUse(_ sender: Any) {
        if let data = addedDatas, data.count > 0 {
            requestMaterialUse()
        } else {
            CommonUtil.showMessage("사용할 자재를 추가해주세요")
        }
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = addedDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialRequestCell", for: indexPath) as! MaterialRequestCell
        guard let data = addedDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
    
    // MARK: ListPopupDelegate
    func selectedData(_ data: Any) {
        changeBtnTitle(data)
    }
}
