//
//  VipListViewController.swift
//  service
//
//  Created by insung on 2020/07/22.
//  Copyright © 2020 bodyfriend. All rights reserved.
//

import UIKit

class VipListViewController: BaseViewController {

    @IBOutlet weak var listTbV: UITableView!
    
    var listDatas: [VipListData] = [VipListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestVipList()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        
        setTopbarForTitle(Constants.Title.TOPBAR_VIP_LIST)
        
        let headerNib = UINib(nibName: "ServiceListHeaderView", bundle: nil)
        listTbV.register(headerNib, forHeaderFooterViewReuseIdentifier: "ServiceListHeaderView")
        let cellNib = UINib(nibName: "VipListCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "VipListCell")
        listTbV.tableFooterView = UIView()
    }
    
    func requestVipList() {
        var parameters = Dictionary<String, Any>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "sess_id")
        ApiManager.requestToPost(Constants.URL.VIP_LIST, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(VipListResponse.self, from: response!).resultData?.list! else {
                LoadingView.hideLoadingView()
                return
            }
            self.listDatas = datas
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    func requestAgree(_ data: VipListData) {
        var parameters = Dictionary<String, Any>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "sess_id")
        parameters.updateValue(data.receiveSeq, forKey: "receive_seq")
        ApiManager.requestToPost(Constants.URL.VIP_AGREE, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            print("datas :", datas)
            self.requestVipList()
            LoadingView.hideLoadingView()
        })
    }
}

extension VipListViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDatas.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VipListCell") as! VipListCell
        let data = listDatas[indexPath.row]
        cell.setData(data)
        cell.confirmBtnHandler = {
            CommonUtil.showAlert(msg: "해당 요청건을 승인 하시겠습니까?", target: self, cancel: {}, confirm: {
                self.requestAgree(data)
            })
        }
        cell.phoneBtnHandler = {
            if let phoneNo = data.hPhoneNo {
                CommonUtil.openLink(.call, phoneNo)
            } else {
                CommonUtil.showMessage("연락처가 없습니다")
            }
        }
        return cell
    }
}
