//
//  VipListCell.swift
//  service
//
//  Created by insung on 2020/07/21.
//  Copyright © 2020 bodyfriend. All rights reserved.
//

import UIKit

class VipListCell: UITableViewCell {

    @IBOutlet weak var productLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var addressLb: UILabel!
    @IBOutlet weak var timeLb: UILabel!
    @IBOutlet weak var confirmLb: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var confirmBtnHandler: (() -> Void)?
    var phoneBtnHandler: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        productLb.text = nil
        nameLb.text = nil
        addressLb.text = nil
        timeLb.text = nil
    }
    
    func setData(_ data: VipListData) {
        productLb.text = data.oldProductName.removeWhitespace()
        nameLb.text = data.userName
        addressLb.text = data.addr
        
        var time = data.timeSelect
        if let msg = data.requestMessage {
            time.append(" / \(msg)")
        }
        timeLb.text = time
        confirmBtn.isHidden = data.agreeYn == "Y"
        confirmLb.isHidden = !confirmBtn.isHidden
    }
    
    // MARK: Action Event
    @IBAction func pressedConfirm(_ sender: Any) {
        confirmBtnHandler?()
    }
    
    @IBAction func pressedPhone(_ sender: Any) {
        phoneBtnHandler?()
    }
}
