//
//  HistoryDetailViewController.swift
//  service
//
//  Created by insung on 2018. 7. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class HistoryDetailViewController: BaseViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var receiveSeq: String?
    var target: BaseViewController?
    var productType: String?
    
    @IBOutlet weak var receiptBtn: UIButton!
    @IBOutlet weak var serviceBtn: UIButton!
    @IBOutlet weak var selectedLineV: UIView!
    @IBOutlet weak var detailSV: UIScrollView!
    @IBOutlet weak var receiptSV: UIScrollView!
    @IBOutlet weak var serviceSV: UIScrollView!
    @IBOutlet weak var receiveContentV01: UIView!
    @IBOutlet weak var receiveContentV02: UIView!
    @IBOutlet weak var receiveContentV03: UIView!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var endLb: UILabel!
    @IBOutlet weak var startLb: UILabel!
    @IBOutlet weak var symptomLb: UILabel!
    @IBOutlet weak var repairV: UIView!
    @IBOutlet weak var repairTV: CTTextView!
    @IBOutlet weak var draftV: UIView!
    @IBOutlet weak var draftTV: CTTextView!
    @IBOutlet weak var materialV: UIView!
    @IBOutlet weak var photoCV: UICollectionView!
    @IBOutlet weak var costLb: UILabel!
    @IBOutlet weak var asDateLb: UILabel!
    
    @IBOutlet weak var totalPriceLb: UILabel!
    @IBOutlet weak var depositLb: UILabel!
    @IBOutlet weak var cashReceiptLb: UILabel!
    @IBOutlet weak var receiptLb: UILabel!
    @IBOutlet weak var depositDetailLb: UILabel!
    @IBOutlet var draftVHeightLC: NSLayoutConstraint!
    @IBOutlet var draftHeightLC: NSLayoutConstraint!
    @IBOutlet var repairHeightLC: NSLayoutConstraint!

    var resultData: DetailData?
    var matDatas: [MatListData]?
    var selectedPhotoTitle: String?
    var selectedPhotos: [String : UIImage]? = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestDetail()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle(Constants.Title.TOPBAR_DETAIL)
        
        setReceiveContentView(receiveContentV01, list: Constants.Array.DETAIL_USER_INFO_TITLE_LIST)
        setReceiveContentView(receiveContentV02, list: Constants.Array.DETAIL_ASK_INFO_TITLE_LIST)
        setReceiveContentView(receiveContentV03, list: Constants.Array.DETAIL_PRODUCT_INFO_TITLE_LIST)

        repairV.applyRoundBorder(COLOR_BORDER_GRAY)
        draftV.applyRoundBorder(COLOR_BORDER_GRAY)
        
        let cellNib = UINib(nibName: "PhotoCell", bundle: nil)
        photoCV.register(cellNib, forCellWithReuseIdentifier: "PhotoCell")
        
        pressedDetail(receiptBtn)        
    }
    
    /// 상세 요청
    func requestDetail() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.DETAIL, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(ServiceDetailResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if self.resultData != nil {
                self.resultData = nil
            }
            if self.matDatas != nil {
                self.matDatas = nil
            }
            self.resultData = datas.resultData
            self.matDatas = datas.matList
            
            self.reloadReceiveView()
            self.reloadServiceView()
            
            guard let areaCode = datas.resultData!.area else {
                LoadingView.hideLoadingView()
                return
            }
            self.requestCodeArea(areaCode)
        })
    }
    
    /// 지역 코드 요청
    ///
    /// - Parameter area: 지역 코드
    func requestCodeArea(_ area: String) {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(Constants.Define.CODE_LOCATION, forKey: "code")
        ApiManager.requestToPost(Constants.URL.CODE_LIST, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }
            for data in datas {
                if data.detCd == area {
                    self.setViewArea(data.detCdNm!)
                    break
                }
            }
            LoadingView.hideLoadingViewToDelay()
        })
    }
    
    /// 지역 코드에 따른 지역 이름 설정
    ///
    /// - Parameter value: 지역 코드
    func setViewArea(_ value: String) {
        let view = receiveContentV01.viewWithStringTag(Constants.Title.DETAIL_AREA) as! DetailInfoBaseTypeView
        view.valueLb.text = value
    }
    
    /// 접수내역 리로드
    func reloadReceiveView() {
        reloadReceiveViewFromData(receiveContentV01)
        reloadReceiveViewFromData(receiveContentV02)
        reloadReceiveViewFromData(receiveContentV03)
    }
    
    /// 서비스내역 리로드
    func reloadServiceView() {
        dateLb.text = resultData?.repairCompleteDate
        endLb.text = resultData?.endTime
        startLb.text = resultData?.startTime
        
        repairTV.text = resultData?.repairHistory
        textViewDidChange(repairTV)
        draftTV.text = resultData?.salesTeamConfirm
        textViewDidChange(draftTV)
        
        costLb.text = resultData?.costNCost == "1" ? "유상" : "무상"
        let date = CommonUtil.dateFromString(resultData?.inProductDate, format: "yyyy-MM-dd")
        let nextMonth = resultData?.serviceMonth
        let asDate = CommonUtil.nextMonthDate(date!, nextMonth: Int(nextMonth!) ?? 0)
        asDateLb.text = CommonUtil.stringFromDate(asDate, format: "yyyy-MM-dd")

        totalPriceLb.text = resultData?.costAmount
        let paymentType = Int((resultData?.paymentType)!) ?? 0
        depositLb.text = CommonObject.paymentType(paymentType)
        let cashReceiptType = Int((resultData?.cashReceiptType)!) ?? 0
        if let receiptNum = resultData?.cashReceiptCardNum, resultData?.cashReceiptCardNum.isExist == true {
            cashReceiptLb.text = CommonObject.cashReceiptType(cashReceiptType)
            receiptLb.text = receiptNum
        }
        depositDetailLb.text = resultData?.depositDetailHistory
        
        if var exchange = resultData?.exchange {
            let titles = Constants.Array.DETAIL_SYMPTOM_YET_LIST
            if titles.contains(exchange) == false {
                exchange = "공백"
            }
            changeYetTitle(exchange)
        }
        resetMaterialView()
        setSelectedPhoto()
    }
    
    /// 선택된 사진 설정
    func setSelectedPhoto() {
        let titles = Constants.Array.DETAIL_PHOTO_TITLE_LIST
        for title in titles {
            var imageUrl = imageUrlFromTitle(title)
            if imageUrl.isExist == true {
                imageUrl = CommonUtil.imageUrl(imageUrl)!
                if let data = try? Data(contentsOf: URL(string: imageUrl!)!) {
                    if let image = UIImage(data: data) {
                        self.selectedPhotos?.updateValue(image, forKey: title)
                        self.photoCV.reloadData()
                    }
                }
            }
        }
    }
    
    /// 이름에 따른 이미지 링크
    ///
    /// - Parameter title: 이름
    /// - Returns: 이미지 링크
    func imageUrlFromTitle(_ title: String) -> String? {
        if title == "시리얼번호" {
            return resultData?.picturePathOne
        } else if title == "전체사진" {
            return resultData?.picturePathTwo
        } else if title == "첨부1" {
            return resultData?.picturePathThree
        } else if title == "첨부2" {
            return resultData?.picturePathFour
        }
        return ""
    }
    
    /// 자재 뷰 재설정
    func resetMaterialView() {
        UIView.removeSubviews(materialV)
        let count = matDatas?.count ?? 0
        var preV: UIView?
        for index in 0..<count {
            let data = matDatas![index]
            let view = DetailMaterialView.instanceFromNib()
            materialV.addSubview(view)
            view.reloadData(data)
            
            view.snp.makeConstraints { (make) in
                if preV == nil {
                    make.top.equalTo(materialV)
                } else {
                    make.top.equalTo(preV!.snp.bottom).offset(8)
                }
                make.leading.trailing.equalTo(materialV)
                
                if index == count-1 {
                    make.bottom.equalTo(materialV)
                }
            }
            preV = view
        }
    }

    /// 접수내역 뷰 생성 및 타이틀 설정
    ///
    /// - Parameters:
    ///   - superview: 타입별 뷰
    ///   - list: 타입별 타이틀 리스트
    func setReceiveContentView(_ superview: UIView, list: [String]) {
        var preV: UIView?
        var isNextLine = false
        let count = list.count
        for index in 0..<count {
            let title = list[index]
            var view: UIView?
            let baseV = DetailInfoBaseTypeView.instanceFromNib()
            baseV.titleLb.text = title
            view = baseV
            
            view!.stringTag = title
            superview.addSubview(view!)
            var isOneLine = false
            switch(title) { // 해당 타이틀은 한줄 전체 영역을 잡음
            case Constants.Title.DETAIL_ADDRESS,
                 Constants.Title.DETAIL_CALL_TYPE,
                 Constants.Title.DETAIL_DELIVERY,
                 Constants.Title.DETAIL_ETC,
                 Constants.Title.DETAIL_RECEIVE_DETAIL,
                 Constants.Title.DETAIL_CONTACT_HISTORY,
                 Constants.Title.DETAIL_LONG_ASK,
                 Constants.Title.DETAIL_MODEL_NAME:
                isOneLine = true
            default:
                break
            }
            
            // 뷰 오토레이아웃 설정
            view!.snp.makeConstraints { (make) in
                if preV == nil {
                    make.top.leading.equalTo(superview)
                    if isOneLine == true {
                        make.top.equalTo(superview)
                        isNextLine = true
                    }
                } else {
                    if isOneLine == true {
                        make.top.equalTo((preV?.snp.bottom)!).offset(4)
                        make.leading.equalTo(superview)
                        isNextLine = true
                    } else if isNextLine == true {
                        make.top.equalTo((preV?.snp.bottom)!).offset(4)
                        make.leading.equalTo(superview)
                        isNextLine = false
                    } else {
                        make.top.equalTo((preV?.snp.top)!)
                        make.leading.equalTo((preV?.snp.trailing)!).offset(10)
                        make.width.height.equalTo(preV!)
                        isNextLine = true
                    }
                }
                
                if isNextLine == true {
                    make.trailing.equalTo(superview).offset(-10)
                }
                
                if index == count-1 {
                    make.bottom.equalTo(superview)
                }
            }
            preV = view
        }
    }
    
    /// 생성 되어있는 뷰를 찾아 그 안의 값들을 갱신
    ///
    /// - Parameters:
    ///   - data: 데이터
    ///   - superview: 속해있는 뷰
    func reloadReceiveViewFromData(_ superview: UIView) {
        var list: [String] = []
        if superview.isEqual(receiveContentV01) {
            list = Constants.Array.DETAIL_USER_INFO_TITLE_LIST
        } else if superview.isEqual(receiveContentV02) {
            list = Constants.Array.DETAIL_ASK_INFO_TITLE_LIST
        } else if superview.isEqual(receiveContentV03) {
            list = Constants.Array.DETAIL_PRODUCT_INFO_TITLE_LIST
        }
        
        guard let data = resultData else {
            return
        }
        
        for title in list {
            let value = CommonObject.valueFromData(data, title: title)
            let view = superview.viewWithStringTag(title) 
            if view is DetailInfoBaseTypeView { // 기본 타입인 경우
                if let baseV = view as? DetailInfoBaseTypeView {
                    if title == Constants.Title.DETAIL_PHONE ||
                        title == Constants.Title.DETAIL_TEL { // 전화번호는 밑줄과 제스쳐 추가
                        baseV.valueLb.addUnderLine(string: value!)
                        baseV.valueTap.addTarget(self, action: #selector(gestureTapValue(_:)))
                    } else {
                        baseV.valueLb.text = value
                    }
                }
            } else if view is DetailInfoInputTypeView { // 뷰 타입이 입력인 경우 (주소)
                let textView = (view as! DetailInfoInputTypeView).valueTV!
                textView.text = value
                if let inputV = view as? DetailInfoInputTypeView {
                    inputV.textViewDidChange(textView)
                    if title == Constants.Title.DETAIL_ADDRESS {
                        if data.addr.isExist == true &&
                            data.jibun.isExist == true {
                            inputV.subValueLb.text = resultData?.jibun
                            inputV.subValueHeightLC.isActive = false
                        }
                    }
                }
            }
        }
    }
    
    /// 버튼 셀렉트 상태 변경
    ///
    /// - Parameter btn: 호출된 버튼
    func changedDetailButton(_ btn: UIButton) {
        KeyboardManager.hide()
        btn.isSelected = !btn.isSelected
        if btn.isEqual(receiptBtn) {
            serviceBtn.isSelected = false
            baseSV = receiptSV
        } else if btn.isEqual(serviceBtn) {
            receiptBtn.isSelected = false
            baseSV = serviceSV
        }
    }
    
    /// 뷰 스크롤 변경
    ///
    /// - Parameter btn: 상단 버튼
    func changedDetailScroll(_ btn: UIButton) {
        if btn.isEqual(receiptBtn) {
            detailSV.scrollRectToVisible(receiptSV.frame, animated: true)
        } else if btn.isEqual(serviceBtn) {
            detailSV.scrollRectToVisible(serviceSV.frame, animated: true)
        }
    }
    
    /// 미처리 특이사항 변경 시
    ///
    /// - Parameter title: 제목
    func changeYetTitle(_ title: String) {
        symptomLb.text = title
        draftVHeightLC.isActive = title == "공백"
    }
    
    // MARK: Action Event
    @IBAction func pressedDetail(_ sender: UIButton) {
        if sender.isSelected == true {
            return
        }
        changedDetailButton(sender)
        changedDetailScroll(sender)
    }
    
    /// 전화번호 탭 시
    ///
    /// - Parameter sender: 탭 제스쳐
    @objc func gestureTapValue(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if view is UILabel {
            if let label = view as? UILabel {
                CommonUtil.openLink(.call, label.text!)
            }
        }
    }

    // MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let count = scrollView.bounds.width / selectedLineV.bounds.width
        let index = round(scrollView.contentOffset.x / scrollView.bounds.width)
        if index == 0 && serviceBtn.isSelected {
            changedDetailButton(receiptBtn)
        } else if index == 1 && receiptBtn.isSelected {
            changedDetailButton(serviceBtn)
        }
        selectedLineV.transform = CGAffineTransform(translationX: scrollView.contentOffset.x/count, y: 0)
    }
    
    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if textView.isEqual(repairTV) {
            let frame = textView.frame
            textView.sizeToFit()
            repairHeightLC.constant = textView.contentSize.height
            textView.frame = frame
        } else if textView.isEqual(draftTV) {
            let frame = textView.frame
            textView.sizeToFit()
            draftHeightLC.constant = textView.contentSize.height
            textView.frame = frame
        }
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.Array.DETAIL_PHOTO_TITLE_LIST.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        let title = Constants.Array.DETAIL_PHOTO_TITLE_LIST[indexPath.row]
        cell.photoBtn.stringTag = title
        let image = selectedPhotos?[title]
        cell.setData(title: title, image: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        return CGSize(width: (screenWidth - screenWidth * 0.1) / 4 , height: 110)
    }
}
