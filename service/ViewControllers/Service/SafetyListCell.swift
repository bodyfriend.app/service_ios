//
//  SafetyListCell.swift
//  service
//
//  Created by insung on 25/03/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit

class SafetyListCell: UITableViewCell {

    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var titleLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
