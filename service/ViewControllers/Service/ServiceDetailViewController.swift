//
//  ServiceDetailViewController.swift
//  service
//
//  Created by insung on 2018. 7. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import Photos

class ServiceDetailViewController: BaseViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, ListPopupDelegate, InputPopupDelegate, SignPopupDelegate {

    @IBOutlet weak var receiptBtn: UIButton!
    @IBOutlet weak var serviceBtn: UIButton!
    @IBOutlet weak var selectedLineV: UIView!
    @IBOutlet weak var detailSV: UIScrollView!
    @IBOutlet weak var receiptSV: UIScrollView!
    @IBOutlet weak var serviceSV: UIScrollView!
    @IBOutlet weak var userInfoContentV: UIView!
    @IBOutlet weak var materialTbV: UITableView!
    @IBOutlet weak var askInfoContentV: UIView!
    @IBOutlet weak var productInfoContentV: UIView!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var endBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var receiveTypeCircleV: CircleView!
    @IBOutlet weak var receiveTypeBtn: UIButton!
    @IBOutlet weak var usableCircleV: CircleView!
    @IBOutlet weak var usableBtn: UIButton!
    @IBOutlet weak var giftBtn: UIButton!
    @IBOutlet weak var keywordTF: CTTextField!
    @IBOutlet weak var keywordBtn: UIButton!
    @IBOutlet weak var symptomV: UIView!
    @IBOutlet weak var symptomBtn: UIButton!
    @IBOutlet weak var repairBtn: UIButton!
    @IBOutlet weak var repairV: UIView!
    @IBOutlet weak var repairTV: CTTextView!
    @IBOutlet weak var treatBtn: UIButton!
    @IBOutlet weak var treatV: UIView!
    @IBOutlet weak var treatTV: CTTextView!
    @IBOutlet weak var draftV: UIView!
    @IBOutlet weak var draftTV: CTTextView!
    @IBOutlet weak var complainStateBtn: UIButton!
    @IBOutlet weak var complainBtn: UIButton!
    @IBOutlet weak var materialV: UIView!
    @IBOutlet weak var materialBtn: UIButton!
    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var materialUseBtn: UIButton!
    @IBOutlet weak var trackingBtn: UIButton!
    @IBOutlet weak var couponBtn: UIButton!
    @IBOutlet weak var photoSaveBtn: UIButton!
    @IBOutlet weak var photoCV: UICollectionView!
    @IBOutlet weak var safetyCircleV: CircleView!
    @IBOutlet weak var safetyTbV: UITableView!
    @IBOutlet weak var signCircleV: CircleView!
    @IBOutlet weak var signBtn: UIButton!
    @IBOutlet weak var costBtn: UIButton!
    @IBOutlet weak var costLb: UILabel!
    @IBOutlet weak var asDateLb: UILabel!
    @IBOutlet weak var totalPriceTF: CTTextField!
    @IBOutlet weak var receiptTF: CTTextField!
    @IBOutlet weak var depositTF: CTTextField!
    @IBOutlet weak var depositBtn: UIButton!
    @IBOutlet weak var cashReceiptBtn: UIButton!
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet var materialHeightLC: NSLayoutConstraint!
    @IBOutlet var materialInfoHeightLC: NSLayoutConstraint!
    @IBOutlet var receiveTypeLeftLC: NSLayoutConstraint!
    @IBOutlet var usableLeftLC: NSLayoutConstraint!
    @IBOutlet var draftVHeightLC: NSLayoutConstraint!
    @IBOutlet var draftHeightLC: NSLayoutConstraint!
    @IBOutlet var repairHeightLC: NSLayoutConstraint!
    @IBOutlet var safetyHeightLC: NSLayoutConstraint!
    @IBOutlet var paymentWidthLC: NSLayoutConstraint!
    
    
    var receiveSeq: String?
    var target: BaseViewController?
    var productType: String?
    
    var materialList: [MaterialDetailData]?
    var receiveTypeDatas: [CodeListData]?
    var symptomMainList: [CodeListData]?
    var selectedSymptomMainList: [CodeListData]?
    var selectedSymptomSubList: [CodeListData]?
    var resultData: DetailData?
    var matDatas: [MatListData]?
    var selectedPhotoTitle: String?
    var selectedPhotos: [String : UIImage]? = [:]
    var savedPhotoCount = 0
    var selectedSafety: [String] = ["N", "N", "N", "N", "N"]
    var savedSignImage: UIImage?
    var dateTF = CTTextField()
    var isSelectedReceptionType = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        safetyTbV.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        materialTbV.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(requestModifyPayat), name: NSNotification.Name(rawValue: "ModifyPayat"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        safetyTbV.removeObserver(self, forKeyPath: "contentSize")
        materialTbV.removeObserver(self, forKeyPath: "contentSize")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ModifyPayat"), object: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if keyPath == "contentSize" {
                if let newSize = change?[NSKeyValueChangeKey.newKey] as? CGSize {
                    if obj.isEqual(materialTbV) {
                        materialInfoHeightLC.constant = newSize.height
                    } else if obj.isEqual(safetyTbV) {
                        safetyHeightLC.constant = newSize.height
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListPopupSegue" {
            let vc = segue.destination as! ListPopupViewController
            vc.receiveData = sender as? [String : Any]
            vc.delegate = self
        } else if segue.identifier == "InputPopupSegue" {
            let vc = segue.destination as! InputPopupViewController
            vc.delegate = self
            vc.receiveData = sender as? [String : Any]
        } else if segue.identifier == "SignPopupSegue" {
            let vc = segue.destination as! SignPopupViewController
            vc.delegate = self
        } else if segue.identifier == "MaterialRequestSegue" {
            let vc = segue.destination as! MaterialRequestViewController
            vc.target = self
            vc.topbarTitle = sender as? String
            vc.serviceSeq = receiveSeq
            vc.areaCode = resultData?.area
        } else if segue.identifier == "MaterialUseSegue" {
            let vc = segue.destination as! MaterialUseViewController
            vc.target = self
            vc.serviceSeq = receiveSeq
        }
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle(Constants.Title.TOPBAR_DETAIL)
        
        setReceiveContentView(userInfoContentV, list: Constants.Array.DETAIL_USER_INFO_TITLE_LIST)
        setReceiveContentView(askInfoContentV, list: Constants.Array.DETAIL_ASK_INFO_TITLE_LIST)
        setReceiveContentView(productInfoContentV, list: Constants.Array.DETAIL_PRODUCT_INFO_TITLE_LIST)

        historyBtn.applyRoundBorder()
        keywordBtn.applyRoundBorder()

        dateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        endBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        startBtn.applyRoundBorder(COLOR_BORDER_GRAY)

        receiveTypeBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        usableBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        giftBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        symptomBtn.applyRoundBorder(COLOR_BORDER_GRAY)

        repairBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        repairV.applyRoundBorder(COLOR_BORDER_GRAY)
        treatBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        treatV.applyRoundBorder(COLOR_BORDER_GRAY)
        draftV.applyRoundBorder(COLOR_BORDER_GRAY)
        
        complainStateBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        complainBtn.applyRoundBorder(COLOR_BORDER_GRAY)

        materialBtn.applyRoundBorder()
        deliveryBtn.applyRoundBorder()
        materialUseBtn.applyRoundBorder()
        couponBtn.applyRoundBorder()
        paymentBtn.applyRoundBorder()

        photoSaveBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        signBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        depositBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        
        let photoCellNib = UINib(nibName: "PhotoCell", bundle: nil)
        photoCV.register(photoCellNib, forCellWithReuseIdentifier: "PhotoCell")

        let materialCellNib = UINib(nibName: "MaterialInfoCell", bundle: nil)
        materialTbV.register(materialCellNib, forCellReuseIdentifier: "MaterialInfoCell")
        
        let safetyCellNib = UINib(nibName: "SafetyListCell", bundle: nil)
        safetyTbV.register(safetyCellNib, forCellReuseIdentifier: "SafetyListCell")
        
        changedDeposit(0) // default 카드
        pressedDetail(receiptBtn)
        
        dateTF.accessoryDelegate = self
        view.addSubview(dateTF)
    }
    
    func changedDeposit(_ index: Int) {
        let depositTitles = Constants.Array.DETAIL_DEPOSIT_TYPE_LIST
        depositBtn.setTitle(depositTitles[index], for: .normal)
        self.paymentWidthLC.isActive = index != 0
    }
    
    override func reloadViewController() {
        requestMaterialList()
    }
    
    /// 상세데이터 요청
    func requestDetail() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.DETAIL, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(ServiceDetailResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if self.resultData != nil {
                self.resultData = nil
            }
            if self.matDatas != nil {
                self.matDatas = nil
            }
            self.resultData = datas.resultData
            self.matDatas = datas.matList
            
            self.reloadReceiveView()
            self.reloadServiceView()
            
            self.requestMaterialList()
            self.requestReceiveTypeList()
            
            guard let areaCode = datas.resultData!.area else {
                LoadingView.hideLoadingView()
                return
            }
            self.requestCodeArea(areaCode)
        })
    }
    
    /// 지역 코드 요청
    ///
    /// - Parameter area: 지역
    func requestCodeArea(_ area: String) {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(Constants.Define.CODE_LOCATION, forKey: "code")
        ApiManager.requestToPost(Constants.URL.CODE_LIST, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }
            for data in datas {
                if data.detCd == area {
                    self.setViewArea(data.detCdNm!)
                    break
                }
            }
            self.requestSymptom()
        })
    }
    
    /// 자재 리스트
    func requestMaterialList() {
        var parameters = Dictionary<String, String>()
        parameters.updateValue(receiveSeq!, forKey: "service_seq")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_DETAIL_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialDetailResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    if self.materialList != nil {
                        self.materialList = nil
                    }
                    self.materialList = decode.data
                    self.reloadMaterialView()
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 접수 유형 리스트
    func requestReceiveTypeList() {
        ApiManager.requestToPost(Constants.URL.DETAIL_RECEIVE_TYPE, parameters: nil, isLoading: false, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }
            self.receiveTypeDatas = datas
            
            if let receptionType = self.resultData?.receptionType, receptionType.isExist == true {
                for data in datas {
                    if data.detCd == receptionType {
                        self.receiveTypeBtn.setTitle(data.detCdNm, for: .normal)
                    }
                }
            } else {
                self.receiveTypeBtn.setTitle("접수 유형 선택", for: .normal)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 주소 업데이트 요청
    func requestUpdateAddress() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        let view = userInfoContentV.viewWithStringTag(Constants.Title.DETAIL_ADDRESS) as! DetailInfoInputTypeView
        let address = view.valueTV.text
        if address.isExist == false {
            return
        }
        parameters.updateValue(address!, forKey: "addr")
        ApiManager.requestToPost(Constants.URL.DETAIL_UPDATE_ADDRESS, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 증상 요청
    func requestSymptom() {
        ApiManager.requestToPost(Constants.URL.DETAIL_SYMPTOM, parameters: nil, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }

            if self.symptomMainList != nil {
                self.symptomMainList?.removeAll()
                self.symptomMainList = nil
            }
            self.symptomMainList = datas
            self.initSelectedSymptomData()
            self.resetSymptomView()
            LoadingView.hideLoadingViewToDelay()
        })
    }
    
    /// 코드 리스트 요청
    ///
    /// - Parameters:
    ///   - code: code
    ///   - dataIndex: 데이터 index
    func requestCodeList(_ code: String, dataIndex: Int) {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(code, forKey: "code")
        ApiManager.requestToPost(Constants.URL.CODE_LIST, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }
            
            let titles = CommonObject.titlesFromCodeData(datas, type: .sub)
            CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
                if CommonObject.countOfArray(self.selectedSymptomSubList) > dataIndex {
                    if let _ = self.selectedSymptomSubList?[dataIndex] {
                        self.selectedSymptomSubList?[dataIndex] = datas[index]
                    } else {
                        self.selectedSymptomSubList?.append(datas[index])
                    }
                } else {
                    self.selectedSymptomSubList?.append(datas[index])
                }
                self.resetSymptomView()
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 수리 내역 수정 요청
    ///
    /// - Parameter key: key
    func requestRepairModify() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        
        if repairTV.text.isExist == false {
            CommonUtil.showMessage("내용을 입력해주세요")
            return
        } else {
            parameters.updateValue(repairTV.text!, forKey: "repairhistory")
        }
        
        ApiManager.requestToPost(Constants.URL.DETAIL_MODIFY_REPAIR, parameters: parameters) { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        }
    }
    
    /// 처리 내역 수정 요청
    ///
    /// - Parameter key: key
    func requestTreatModify() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        
        if treatTV.text.isExist == false {
            CommonUtil.showMessage("내용을 입력해주세요")
            return
        } else {
            parameters.updateValue(treatTV.text!, forKey: "treathistory")
        }
        
        ApiManager.requestToPost(Constants.URL.DETAIL_MODIFY_TREAT, parameters: parameters) { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        }
    }

    func reqeustMaterialUpdateState(_ requestNo: String, _ state: String) {
        var parameters = Dictionary<String, String>()
        parameters.updateValue(requestNo, forKey: "requestNum")
        parameters.updateValue(state, forKey: "state")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_UPDATE_STATE, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialCommonResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    var msg: String?
                    if state == "CA" {
                        msg = "수령확인 되었습니다"
                    } else {
                        msg = "수령확인 취소 되었습니다"
                    }
                    CommonUtil.showMessage(msg!)
                    self.requestMaterialList()
                    return
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 접수 유형 리스트 보여짐
    func showReceiveTypeList() {
        if let datas = receiveTypeDatas {
            var titles: [String] = []
            for data in datas {
                titles.append(data.detCdNm!)
            }
            CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
                self.isSelectedReceptionType = true
                self.receiveTypeBtn.setTitle(titles[index], for: .normal)
            }
        }
    }
    
    func showUsableList() {
        let titles = Constants.Array.PRODUCT_USEABLE_LIST
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            self.usableBtn.setTitle(titles[index], for: .normal)
        }
    }
    
    func showGiftList() {
        let titles = Constants.Array.GIFT_LIST
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            self.giftBtn.setTitle(titles[index], for: .normal)
        }
    }
    
    /// 이력 요청
    func requestHistory() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.DETAIL_SERVICE_HISTORY, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(DetailHistoryResponse.self, from: response!).resultData!.list) as [HistoryListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            
            if datas?.count == 0 {
                CommonUtil.showMessage("서비스이력이 존재 하지 않습니다")
            } else {
                let sender = ["type" : self.productType!, "data" : datas as Any, "title" : Constants.Title.POPUP_DETAIL_HISTORY] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 키워드 검색 요청
    func requestKeyword() {
        guard let keyword = keywordTF.text, keywordTF.text.isExist == true else {
            CommonUtil.showMessage("입력된 키워드가 없습니다")
            return
        }
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(keyword, forKey: "det_cd_nm")
        ApiManager.requestToPost(Constants.URL.DETAIL_SYMPTOM_SEARCH, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData) as [CodeListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            if datas?.count == 0 {
                CommonUtil.showMessage("검색 결과가 존재 하지 않습니다")
            } else {
                let sender = ["data" : datas as Any, "title" : Constants.Title.POPUP_DETAIL_SYMPTOM] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func requestCoupon() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(resultData?.hPhoneNo ?? "", forKey: "handphone")
        parameters.updateValue(resultData?.receiveSeq ?? "", forKey: "receive_seq")
        parameters.updateValue(resultData?.orderNo ?? "", forKey: "order_no")
        parameters.updateValue(resultData?.custNo ?? "", forKey: "cust_no")
        parameters.updateValue(resultData?.userName ?? "", forKey: "user_nm")
        ApiManager.requestToPost(Constants.URL.DETAIL_COUPON, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let message = datas.resultMsg {
                CommonUtil.showAlert(msg: message, target: self, cancel: nil, confirm: nil)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 누락된 데이터 체크 후 메세지 유무
    ///
    /// - Returns: 메세지
    func messageCheckValid() -> String? {
        let date = CommonUtil.changeDateFormat(preFormat: "yyyy-MM-dd", date: dateBtn.titleLabel?.text, changeFormat: "yyyymmdd")
        if date == nil {
            return "날짜를 선택해주세요"
        }

        if endBtn.titleLabel?.text.isExist == false {
            return "도착시간을 선택해주세요"
        }

        if startBtn.titleLabel?.text.isExist == false {
            return "출발시간을 선택해주세요"
        }

        if let datas = receiveTypeDatas, let title = receiveTypeBtn.titleLabel?.text {
            if CommonObject.codeFromData(datas, title: title) == "501" ||
                isSelectedReceptionType == false { // 필수 최소 한번 입력 해야함
                return "접수 유형을 선택해주세요"
            }
        } else {
            return "접수 유형을 선택해주세요"
        }
        
        if let title = usableBtn.titleLabel?.text, title.contains("제품사용가능/불가")  {
            if let stateTitle = complainStateBtn.titleLabel?.text, isNeedUsableInputData(stateTitle) == true {
                return "제품사용가능여부를 선택해주세요"
            }
        }

        var isSelected = false
        for data in selectedSymptomMainList! {
            if data.commCd != nil {
                isSelected = true
                break
            }
        }
        if isSelected == false {
            return "증상을 선택해주세요"
        }

        if complainStateBtn.titleLabel?.text.isExist == false {
            return "진행 사항을 선택해주세요"
        }

        if let title = complainStateBtn.titleLabel?.text {
            if isNeedInputData(title) {
                if savedSignImage == nil {
                    return "고객 서명을 해주세요"
                }
                if selectedPhotos!["시리얼번호"] == nil {
                    return "시리얼번호 이미지를 첨부해주세요"
                }
                if selectedPhotos!["전체사진"] == nil {
                    return "전체사진 이미지를 첨부해주세요"
                }
                for index in 0..<selectedSafety.count {
                    if selectedSafety[index] == "N" {
                        return "안전 점검 체크리스트를 확인해주세요"
                    }
                }
            }
        }

        if resultData?.costNCost != nil &&
            resultData?.costNCost == "1" {
            if depositTF.text.isExist == false {
                return "입금 상세내역을 입력해주세요"
            }
            if totalPriceTF.text.isExist == false {
                return "유상건 합계 금액을 입력해주세요"
            }
            let paymentType = CommonObject.indexPaymentType(depositBtn.titleLabel?.text!)
            if paymentType == 0 {
                return "입금 유형을 선택해주세요"
            }
        }

        if symptomBtn.titleLabel?.text != "공백" {
            if draftTV.text.isExist == false {
                return "기안 내역을 입력해주세요"
            }
        }
        
        return nil
    }

    /// 이미지 업로드 파라미터
    ///
    /// - Returns: 파라미터
    func parameterUploadImages() -> Dictionary<String, UIImage> {
        var uploadImages = Dictionary<String, UIImage>()
        for (key, value) in selectedPhotos! {
            var uploadKey: String?
            if key == "시리얼번호" {
                uploadKey = "screen1"
            } else if key == "전체사진" {
                uploadKey = "screen2"
            } else if key == "첨부1" {
                uploadKey = "screen3"
            } else if key == "첨부2" {
                uploadKey = "screen4"
            }
            if uploadKey.isExist == true {
                uploadImages.updateValue(value, forKey: uploadKey!)
            }
        }
        return uploadImages
    }
    
    /// 이미지 업로드 요청
    func requestImageUpload() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        
        let uploadImages = parameterUploadImages()
        ApiManager.requestToMultipart(Constants.URL.DETAIL_UPDATE_PICTURE, parameters: parameters, uploadImages: uploadImages) { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        }
    }
    
    @objc func requestModifyPayat() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        let payat = PayatData.sharedInstance
        var confirm: String?
        if payat.approval {
            confirm = "\(payat.approvalTime!), \(payat.orderNo!), \(payat.approvalNo!), \(totalPriceTF.text!), \(payat.cardInstallmentMonth!), \(payat.cardName!), \(payat.plainCardNo!), \(payat.employeeName!)"
        }
        parameters.updateValue(payat.approval ? "1" : "0", forKey: "payatt")
        parameters.updateValue(payat.approval ? "1" : "0", forKey: "depositcheck")
        parameters.updateValue(confirm ?? "", forKey: "depositconfirm")
        ApiManager.requestToPost(Constants.URL.DETAIL_MODIFY_PAYAT, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 등록 요청
    func requestRegister() {
        let message = messageCheckValid()
        if message != nil {
            CommonUtil.showMessage(message!)
            return
        }
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(receiveSeq!, forKey: "receive_seq")
        parameters.updateValue(productType!, forKey: "m_receivetype")
        parameters.updateValue(repairTV.text!, forKey: "repairhistory")
        parameters.updateValue(treatTV.text!, forKey: "treathistory")
        parameters.updateValue(resultData?.engineerCharge ?? "", forKey: "engineercharge")

        for index in 0..<selectedSymptomMainList!.count {
            let data = selectedSymptomMainList![index]
            var key: String?
            if data.commCd != nil {
                if index == 0 {
                    key = "symptomgroupone"
                } else if index == 1 {
                    key = "symptomgroupthree"
                } else if index == 2 {
                    key = "symptomgroupfive"
                }
            }
            if key != nil {
                parameters.updateValue(data.commCd ?? "", forKey: key!)
            }
        }
        for index in 0..<selectedSymptomSubList!.count {
            let data = selectedSymptomSubList![index]
            var key: String?
            if data.detCd != nil {
                if index == 0 {
                    key = "symptomgrouptwo"
                } else if index == 1 {
                    key = "symptomgroupfour"
                } else if index == 2 {
                    key = "symptomgroupsix"
                }
            }
            if key != nil {
                parameters.updateValue(data.detCd ?? "", forKey: key!)
            }
        }

        parameters.updateValue(totalPriceTF.text ?? "", forKey: "costamount")
        parameters.updateValue(CommonObject.yetNoteType(symptomBtn.titleLabel?.text!), forKey: "exchange")
        let date = CommonUtil.changeDateFormat(preFormat: "yyyy-MM-dd", date: dateBtn.titleLabel?.text, changeFormat: "yyyyMMdd")
        parameters.updateValue(date!, forKey: "repaircompletedate")
        parameters.updateValue(endBtn.titleLabel?.text ?? "", forKey: "endt")
        parameters.updateValue(startBtn.titleLabel?.text ?? "", forKey: "ends")
        parameters.updateValue(CommonObject.indexPaymentType(depositBtn.titleLabel?.text!), forKey: "paymenttype")
        parameters.updateValue(CommonObject.indexProgressType(complainStateBtn.titleLabel?.text!), forKey: "progressno")
        parameters.updateValue(depositTF.text!, forKey: "depositdetailhistory")
        parameters.updateValue("0", forKey: "cashReceiptType") // 휴대폰으로 고정
        parameters.updateValue(receiptTF.text!, forKey: "cashReceiptCardNum")
        parameters.updateValue(complainBtn.isSelected == true ? "Y" : "N", forKey: "isComplain")

        let payat = PayatData.sharedInstance
        var confirm: String?
        if payat.approval {
            confirm = "\(payat.approvalTime!), \(payat.orderNo!), \(payat.approvalNo!), \(totalPriceTF.text!), \(payat.cardInstallmentMonth!), \(payat.cardName!), \(payat.plainCardNo!), \(payat.employeeName!)"
        }
        parameters.updateValue(payat.approval ? "1" : "0", forKey: "payatt")
        parameters.updateValue(payat.approval ? "1" : "0", forKey: "depositcheck")
        parameters.updateValue(confirm ?? "", forKey: "depositconfirm")
        var uploadImages = parameterUploadImages()
        if savedSignImage != nil {
            uploadImages.updateValue(savedSignImage!, forKey: "screen_sign")
        }

        for index in 0..<selectedSafety.count {
            var key = "safetyCheck"
            key.append(String(index+1))
            parameters.updateValue(selectedSafety[index], forKey: key)
        }
        
        if let datas = receiveTypeDatas, let title = receiveTypeBtn.titleLabel?.text {
            if let receiveTypeCd = CommonObject.codeFromData(datas, title: title), receiveTypeCd.isExist == true {
                parameters.updateValue(receiveTypeCd, forKey: "reception_type")
            }
        }
        
        if let usableType = usableBtn.titleLabel?.text, usableType.isExist == true {
            parameters.updateValue(usableType == "가능" ? "Y" : "N", forKey: "useproduct_type")
        }
        
        if let giftType = giftBtn.titleLabel?.text, giftType.isExist == true {
            parameters.updateValue(giftType == "증정" ? "Y" : "N", forKey: "gift_flag")
        }
        
        if let costType = costBtn.titleLabel?.text, costType.isExist == true {
            parameters.updateValue(costType == "유상" ? "1" : "2", forKey: "costNcost")
        }

        ApiManager.requestToMultipart(Constants.URL.DETAIL_REGISTER, parameters: parameters, uploadImages: uploadImages) { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
            self.closeViewController(self.target)
        }
    }
    
    /// 지역 코드에 따른 지역 이름 설정
    ///
    /// - Parameter value: 지역 코드
    func setViewArea(_ value: String) {
        let view = userInfoContentV.viewWithStringTag(Constants.Title.DETAIL_AREA) as! DetailInfoBaseTypeView
        view.valueLb.text = value
    }
    
    /// 접수 내역 리로드
    func reloadReceiveView() {
        reloadReceiveViewFromData(userInfoContentV)
        reloadReceiveViewFromData(askInfoContentV)
        reloadReceiveViewFromData(productInfoContentV)
    }
    
    /// 자재 리로드
    func reloadMaterialView() {
        guard let datas = materialList, datas.count > 0 else {
            materialHeightLC.isActive = true
            return
        }
        materialHeightLC.isActive = false
        materialTbV.reloadData()
    }
    
    /// 서비스내역 리로드
    func reloadServiceView() {
        repairTV.text = resultData?.repairHistory
        textViewDidChange(repairTV)
        treatTV.text = resultData?.treatHistory
        textViewDidChange(treatTV)
        draftTV.text = resultData?.salesTeamConfirm
        textViewDidChange(draftTV)

        let cost = resultData?.costNCost == "1" ? "유상" : "무상"
        costBtn.setTitle(cost, for: .normal)
        
        if let date = CommonUtil.dateFromString(resultData?.inProductDate, format: "yyyy-MM-dd") {
            let nextMonth = resultData?.serviceMonth
            let asDate = CommonUtil.nextMonthDate(date, nextMonth: Int(nextMonth!) ?? 0)
            asDateLb.text = CommonUtil.stringFromDate(asDate, format: "yyyy-MM-dd")
        } else {
            asDateLb.text = nil
        }

        var usableType: String?
        if let useProductType = resultData?.useProductType, useProductType.isExist == true {
            usableType = useProductType == "Y" ? "가능" : "불가"
        } else {
            usableType = Constants.Title.PRODUCT_USEABLE
        }
        usableBtn.setTitle(usableType, for: .normal)
        
        let progressNo = Int((resultData?.progressNo)!) ?? 0
        let title = CommonObject.titleProgressType(progressNo == 3 ? 0 : progressNo)
        complainStateBtn.setTitle(title, for: .normal)
         
        if var exchange = resultData?.exchange {
            let titles = Constants.Array.DETAIL_SYMPTOM_YET_LIST
            if titles.contains(exchange) == false {
                exchange = "공백"
            }
            changeYetTitle(exchange)
        }
        checkNeedInputData(title)
        checkNeedUsableInputData(title)
        resetMaterialView()
        setSelectedPhoto()
    }
        
    /// 필수 입력 데이터 여부
    /// - Parameter title: 타이틀
    func isNeedInputData(_ title: String?) -> Bool {
        let progressNo = Int((resultData?.progressNo)!) ?? 0
        let selectedProgressNo = CommonObject.indexProgressType(title)
        return progressNo != 3 && (selectedProgressNo != 0 && selectedProgressNo != 2 && selectedProgressNo != 3 && selectedProgressNo != 8) // 3: 미처리 8: 자재확인
    }
    
    func isNeedUsableInputData(_ title: String?) -> Bool {
        let selectedProgressNo = CommonObject.indexProgressType(title)
        return selectedProgressNo != 2
    }
    
    /// 필수 입력 데이터 체크
    /// - Parameter title: 타이틀
    func checkNeedInputData(_ title: String?) {
        let isHidden = !isNeedInputData(title)
        signCircleV.isHidden = isHidden
        safetyCircleV.isHidden = isHidden
    }
    
    func checkNeedUsableInputData(_ title: String?) {
        usableCircleV.isHidden = !isNeedUsableInputData(title)
    }
    
    /// 선택된 사진 설정
    func setSelectedPhoto() {
        let titles = Constants.Array.DETAIL_PHOTO_TITLE_LIST
        for title in titles {
            var imageUrl = imageUrlFromTitle(title)
            if imageUrl.isExist == true {
                imageUrl = CommonUtil.imageUrl(imageUrl)!
                if let data = try? Data(contentsOf: URL(string: imageUrl!)!) {
                    if let image = UIImage(data: data) {
                        self.selectedPhotos?.updateValue(image, forKey: title)
                        self.photoCV.reloadData()
                    }
                }
            }
        }
    }
    
    /// 이름에 따른 이미지 주소
    ///
    /// - Parameter title: 이름
    /// - Returns: 이미지 주소
    func imageUrlFromTitle(_ title: String) -> String? {
        if title == "시리얼번호" {
            return resultData?.picturePathOne
        } else if title == "전체사진" {
            return resultData?.picturePathTwo
        } else if title == "첨부1" {
            return resultData?.picturePathThree
        } else if title == "첨부2" {
            return resultData?.picturePathFour
        }
        return ""
    }
    
    /// 자재 재설정
    func resetMaterialView() {
        UIView.removeSubviews(materialV)
        let count = matDatas?.count ?? 0
        var preV: UIView?
        for index in 0..<count {
            let data = matDatas![index]
            let view = DetailMaterialView.instanceFromNib()
            materialV.addSubview(view)
            view.reloadData(data)
            
            view.snp.makeConstraints { (make) in
                if preV == nil {
                    make.top.equalTo(materialV)
                } else {
                    make.top.equalTo(preV!.snp.bottom).offset(8)
                }
                make.leading.trailing.equalTo(materialV)
                
                if index == count-1 {
                    make.bottom.equalTo(materialV)
                }
            }
            preV = view
        }
    }

    /// 접수내역 뷰 생성 및 타이틀 설정
    ///
    /// - Parameters:
    ///   - superview: 타입별 뷰
    ///   - list: 타입별 타이틀 리스트
    func setReceiveContentView(_ superview: UIView, list: [String]) {
        var preV: UIView?
        var isNextLine = false
        let count = list.count
        for index in 0..<count {
            let title = list[index]
            var view: UIView?
            if title == Constants.Title.DETAIL_ADDRESS {
                let inputV = DetailInfoInputTypeView.instanceFromNib()
                inputV.titleLb.text = title
                inputV.inputBtn.addTarget(self, action: #selector(pressedAddress(_:)), for: .touchUpInside)
                view = inputV
            } else {
                let baseV = DetailInfoBaseTypeView.instanceFromNib()
                baseV.titleLb.text = title
                view = baseV
            }
            
            view!.stringTag = title
            superview.addSubview(view!)
            var isOneLine = false
            switch(title) { // 해당 타이틀은 한줄 전체 영역을 잡음
            case Constants.Title.DETAIL_ADDRESS,
                 Constants.Title.DETAIL_CALL_TYPE,
                 Constants.Title.DETAIL_DELIVERY,
                 Constants.Title.DETAIL_ETC,
                 Constants.Title.DETAIL_RECEIVE_DETAIL,
                 Constants.Title.DETAIL_CONTACT_HISTORY,
                 Constants.Title.DETAIL_LONG_ASK,
                 Constants.Title.DETAIL_MODEL_NAME:
                isOneLine = true
            default:
                break
            }
            
            // 뷰 오토레이아웃 설정
            view!.snp.makeConstraints { (make) in
                if preV == nil {
                    make.top.leading.equalTo(superview)
                    if isOneLine == true {
                        make.top.equalTo(superview)
                        isNextLine = true
                    }
                } else {
                    if isOneLine == true {
                        make.top.equalTo((preV?.snp.bottom)!).offset(4)
                        make.leading.equalTo(superview)
                        isNextLine = true
                    } else if isNextLine == true {
                        make.top.equalTo((preV?.snp.bottom)!).offset(4)
                        make.leading.equalTo(superview)
                        isNextLine = false
                    } else {
                        make.top.equalTo((preV?.snp.top)!)
                        make.leading.equalTo((preV?.snp.trailing)!).offset(10)
                        make.width.height.equalTo(preV!)
                        isNextLine = true
                    }
                }
                
                if isNextLine == true {
                    make.trailing.equalTo(superview).offset(-10)
                }
                
                if index == count-1 {
                    make.bottom.equalTo(superview)
                }
            }
            preV = view
        }
    }
    
    /// 생성 되어있는 뷰를 찾아 그 안의 값들을 갱신
    ///
    /// - Parameters:
    ///   - data: 데이터
    ///   - superview: 속해있는 뷰
    func reloadReceiveViewFromData(_ superview: UIView) {
        var list: [String] = []
        if superview.isEqual(userInfoContentV) {
            list = Constants.Array.DETAIL_USER_INFO_TITLE_LIST
        } else if superview.isEqual(askInfoContentV) {
            list = Constants.Array.DETAIL_ASK_INFO_TITLE_LIST
        } else if superview.isEqual(productInfoContentV) {
            list = Constants.Array.DETAIL_PRODUCT_INFO_TITLE_LIST
        }
        
        guard let data = resultData else {
            return
        }
        
        for title in list {
            let value = CommonObject.valueFromData(data, title: title)
            let view = superview.viewWithStringTag(title)
            if view is DetailInfoBaseTypeView { // 기본 타입인 경우
                if let baseV = view as? DetailInfoBaseTypeView {
                    if title == Constants.Title.DETAIL_PHONE ||
                        title == Constants.Title.DETAIL_TEL { // 전화번호는 밑줄과 제스쳐 추가
                        baseV.valueLb.addUnderLine(string: value!)
                        baseV.valueTap.addTarget(self, action: #selector(gestureTapValue(_:)))
                    } else {
                        baseV.valueLb.text = value
                    }
                }
            } else if view is DetailInfoInputTypeView { // 뷰 타입이 입력인 경우 (주소)
                let textView = (view as! DetailInfoInputTypeView).valueTV!
                textView.text = value
                if let inputV = view as? DetailInfoInputTypeView {
                    inputV.textViewDidChange(textView)
                    if title == Constants.Title.DETAIL_ADDRESS {
                        if data.addr.isExist == true &&
                            data.jibun.isExist == true {
                            inputV.subValueLb.text = resultData?.jibun
                            inputV.subValueHeightLC.isActive = false
                        }
                    }
                }
            }
        }
    }
    
    /// 선택된 증상 데이터 초기화
    func initSelectedSymptomData() {
        if selectedSymptomMainList != nil {
            selectedSymptomMainList?.removeAll()
            selectedSymptomMainList = nil
        }
        if selectedSymptomSubList != nil {
            selectedSymptomSubList?.removeAll()
            selectedSymptomSubList = nil
        }
        selectedSymptomMainList = [CodeListData]()
        selectedSymptomSubList = [CodeListData]()
        
        for _ in 0..<3 {
            let mainData = initCodeListData("증상대분류")
            selectedSymptomMainList?.append(mainData)
            let subData = initCodeListData("증상소분류")
            selectedSymptomSubList?.append(subData)
        }
    }
    
    /// 코드 리스트 초기화
    ///
    /// - Parameter name: 이름
    func initCodeListData(_ name: String) -> CodeListData {
        var detCdNm: String?
        var commCdNm: String?
        if name == "증상대분류" {
            commCdNm = name
        } else if name == "증상소분류" {
            detCdNm = name
        }
        return CodeListData(insDt: nil,
                            detCd: nil,
                            description: nil,
                            detCdNm: detCdNm,
                            commCdNm: commCdNm,
                            updId: nil,
                            updDt: nil,
                            groupCd: nil,
                            insId: nil,
                            sortSeq: nil,
                            ref2: nil,
                            ref3: nil,
                            ref1: nil,
                            commCd: nil)
    }
    
    /// 증상 선택 버튼 생성
    func resetSymptomView() {
        UIView.removeSubviews(symptomV)
        let count = selectedSymptomMainList!.count
        var preV: UIView?
        for index in 0..<count {
            let firstBtn = UIButton(type: .system)
            firstBtn.common()
            firstBtn.contentHorizontalAlignment = .left
            symptomV.addSubview(firstBtn)
            firstBtn.tag = index
            firstBtn.addTarget(self, action: #selector(pressedMainSymptom(_:)), for: .touchUpInside)
            
            if let main = selectedSymptomMainList?[index] {
                firstBtn.setTitle(main.commCdNm, for: .normal)
            }
            
            firstBtn.snp.makeConstraints { (make) in
                if preV == nil {
                    make.leading.top.equalTo(symptomV)
                } else {
                    make.top.equalTo((preV?.snp.bottom)!).offset(4)
                    make.leading.equalTo(symptomV)
                }
                make.height.equalTo(30)
                if index == count-1 {
                    make.bottom.equalTo(symptomV).offset(-10)
                }
            }
            
            let secondBtn = UIButton(type: .system)
            secondBtn.common()
            secondBtn.contentHorizontalAlignment = .left
            symptomV.addSubview(secondBtn)
            secondBtn.tag = index
            secondBtn.addTarget(self, action: #selector(pressedSubSymptom(_:)), for: .touchUpInside)
            
            if let sub = selectedSymptomSubList?[index] {
                secondBtn.setTitle(sub.detCdNm, for: .normal)
            }
            
            secondBtn.snp.makeConstraints { (make) in
                make.top.equalTo(firstBtn)
                make.leading.equalTo(firstBtn.snp.trailing).offset(10)
                make.trailing.equalTo(symptomV)
                make.width.height.equalTo(firstBtn)
            }
            preV = secondBtn
        }
    }
    
    /// 버튼 셀렉트 상태 변경
    ///
    /// - Parameter btn: 호출된 버튼
    func changedDetailButton(_ btn: UIButton) {
        KeyboardManager.hide()
        btn.isSelected = !btn.isSelected
        if btn.isEqual(receiptBtn) {
            serviceBtn.isSelected = false
            baseSV = receiptSV
        } else if btn.isEqual(serviceBtn) {
            receiptBtn.isSelected = false
            baseSV = serviceSV
        }
    }
    
    /// 뷰 스크롤 변경
    ///
    /// - Parameter btn: 상단 버튼
    func changedDetailScroll(_ btn: UIButton) {
        if btn.isEqual(receiptBtn) {
            detailSV.scrollRectToVisible(receiptSV.frame, animated: true)
        } else if btn.isEqual(serviceBtn) {
            detailSV.scrollRectToVisible(serviceSV.frame, animated: true)
        }
    }
    
    /// 미처리 특이사항 변경
    ///
    /// - Parameter title: 타이틀
    func changeYetTitle(_ title: String) {
        symptomBtn.setTitle(title, for: .normal)
        draftVHeightLC.isActive = title == "공백"
    }
    
    func changeComplainStateTitle(_ title: String) {
        complainStateBtn.setTitle(title, for: .normal)
        checkNeedInputData(title)
        checkNeedUsableInputData(title)
    }
    
    func requestSavePhotoPermission(_ image: UIImage) {
        let oldStatus = PHPhotoLibrary.authorizationStatus()
        PHPhotoLibrary.requestAuthorization { (status) in
            DispatchQueue.main.async {
                if status == PHAuthorizationStatus.denied { // 거절
                    if oldStatus != PHAuthorizationStatus.notDetermined { // 설정 페이지
                        CommonUtil.openAppSetting()
                    }
                } else if status == PHAuthorizationStatus.authorized { // 허용
                    PHPhotoLibrary.shared().savePhoto(image)
                } else if status == PHAuthorizationStatus.restricted { // 권한 없음
                    CommonUtil.openAppSetting()
                }
            }
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedDetail(_ sender: UIButton) {
        if sender.isSelected == true {
            return
        }
        changedDetailButton(sender)
        changedDetailScroll(sender)
    }
    
    /// 전화번호 탭 시
    ///
    /// - Parameter sender: 탭 제스쳐
    @objc func gestureTapValue(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if view is UILabel {
            if let label = view as? UILabel {
                CommonUtil.openLink(.call, label.text!)
            }
        }
    }
    
    @objc func pressedAddress(_ sender: UIButton) {
        requestUpdateAddress()
    }
    
    @IBAction func pressedHistory(_ sender: UIButton) {
        requestHistory()
    }
    
    @IBAction func pressedDate(_ sender: UIButton) {
        dateTF.dateType = DateType.none.rawValue
        dateTF.showDatePickerView(.date)
        dateTF.contentDP?.minimumDate = Date()
    }
    
    @IBAction func pressedTime(_ sender: UIButton) {
        if sender.isEqual(endBtn) {
            dateTF.dateType = DateType.end.rawValue
        } else if sender.isEqual(startBtn) {
            dateTF.dateType = DateType.start.rawValue
        }
        dateTF.contentDP?.minimumDate = nil
        dateTF.showDatePickerView(.time)
    }
    
    @IBAction func pressedReceiveType(_ sender: UIButton) {
        showReceiveTypeList()
    }
    
    @IBAction func pressedUsable(_ sender: UIButton) {
        showUsableList()
    }
    
    @IBAction func pressedGift(_ sender: UIButton) {
        showGiftList()
    }
    
    @objc func pressedMainSymptom(_ sender: UIButton) {
        let titles = CommonObject.titlesFromCodeData(symptomMainList!)
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            if let _ = self.selectedSymptomMainList?[sender.tag] {
                self.selectedSymptomMainList?[sender.tag] = self.symptomMainList![index]
            } else {
                self.selectedSymptomMainList?.append(self.symptomMainList![index])
            }
            
            let subData = self.initCodeListData("증상소분류")
            self.selectedSymptomSubList?[sender.tag] = subData
            
            sender.setTitle(titles[index], for: .normal)
            self.resetSymptomView()
        }
    }
    
    @IBAction func pressedKeyword(_ sender: UIButton) {
        requestKeyword()
    }
    
    @IBAction func pressedSymptom(_ sender: UIButton) {
        let titles = Constants.Array.DETAIL_SYMPTOM_YET_LIST
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            let title = titles[index]
            self.changeYetTitle(title)
        }
    }
    
    @objc func pressedSubSymptom(_ sender: UIButton) {
        if let data = selectedSymptomMainList?[sender.tag] {
            if let code = data.commCd {
                requestCodeList(code, dataIndex: sender.tag)
            }
        }
    }
    
    @IBAction func pressedRepair(_ sender: UIButton) {
        requestRepairModify()
    }
    
    @IBAction func pressedTreat(_ sender: UIButton) {
        requestTreatModify()
    }
    
    @IBAction func pressedComplainState(_ sender: UIButton) {
        let progressNo = Int((resultData?.progressNo)!) ?? 0
        var stateList: [String]? = nil
        if progressNo == 3 ||
            progressNo == 8 {
            stateList = Constants.Array.DETAIL_COMPLAIN_UNTREAT_STATE_LIST
        } else {
            stateList = Constants.Array.DETAIL_COMPLAIN_STATE_LIST
        }
        if let titles = stateList {
            CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
                sender.setTitle(titles[index], for: .normal)
                let title = titles[index]
                self.changeComplainStateTitle(title)
            }
        }
    }
    
    @IBAction func pressedComplain(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func pressedMaterial(_ sender: UIButton) {
        openViewController(segue: "MaterialRequestSegue", sender: Constants.Title.TOPBAR_MATERIAL_RECEIVE)
    }
    
    /// 배송 조회
    ///
    /// - Parameter sender: button
    @IBAction func pressedTracking(_ sender: UIButton) {
        let type = resultData?.matDeliveryType
        let num = resultData?.matDeliveryNum
        if type.isExist == false ||
            num.isExist == false {
            CommonUtil.showMessage("등록된 운송장이 없습니다")
        } else {
            var webLink: String?
            if type == "경동" {
                webLink = Constants.URL.DELIVERY_KD_WEB
            } else if type == "한진" {
                webLink = Constants.URL.DELIVERY_HJ_WEB
            }
            let url = String(format: webLink!, num!)
            CommonUtil.openLink(.web, url)
        }
    }
    
    @IBAction func pressedDelivery(_ sender: UIButton) {
        openViewController(segue: "MaterialRequestSegue", sender: Constants.Title.TOPBAR_MATERIAL_DELIVERY)
    }
    
    @IBAction func pressedMaterialUse(_ sender: UIButton) {
        openViewController(segue: "MaterialUseSegue", sender: self)
    }
    
    @IBAction func pressedCoupon(_ sender: UIButton) {
        CommonUtil.showAlert(msg: "할인 쿠폰을 발행하시겠습니까?", target: self, cancel: {}) {
            self.requestCoupon()
        }
    }
    
    @objc func pressedPhoto(_ sender: UIButton) {
        selectedPhotoTitle = sender.stringTag
        let imagePC = UIImagePickerController()
        imagePC.delegate = self
        imagePC.sourceType = .camera
        self.present(imagePC, animated: true, completion: nil)
    }
    
    @IBAction func pressedPhotoSave(_ sender: UIButton) {
        requestImageUpload()
    }
    
    @IBAction func pressedSign(_ sender: UIButton) {
        openViewController(segue: "SignPopupSegue", sender: self)
    }
    
    @IBAction func pressedCost(_ sender: UIButton) {
        if let title = sender.titleLabel?.text,
            title == "유상" {
            CommonUtil.showMessage("유상에서 무상으로 변경할 수 없습니다")
        } else {
            let titles = Constants.Array.DETAIL_COST_TYPE_LIST
            CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
                sender.setTitle(titles[index], for: .normal)
            }
        }
    }
    
    @IBAction func pressedDeposit(_ sender: UIButton) {
        let titles = Constants.Array.DETAIL_DEPOSIT_TYPE_LIST
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            self.changedDeposit(index)
        }
    }
    
    @IBAction func pressedCashReceipt(_ sender: UIButton) {
        let titles = Constants.Array.DETAIL_RECEIPT_TYPE_LIST
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            sender.setTitle(titles[index], for: .normal)
        }
    }
    
    @IBAction func pressedPayment(_ sender: UIButton) {
        guard PayatSDkManager.canOpenPayat() else {
            CommonUtil.showMessage("페이엣 SDK를 사용하시려면 페이앳을 최신버전으로 설치해주세요")
            return
        }
        guard let totalPrice = totalPriceTF.text, totalPrice.isEmpty == false else {
            CommonUtil.showMessage("금액을 입력해주세요")
            return
        }
        
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        let customer = PayatSDkManager.createCustomerDictionary(userData?.payatId, andEmail: userData?.payatEmail, andPhone: "", andMobile: receiptTF.text)
        let productName = String(format: "%@ %@", (resultData?.userName)!, totalPrice)
        let price = Int(totalPrice)!
        let tax = PayatSDkManager.obtainTax(price)
        PayatSDkManager.sendPaymentDataCard(price - tax, andFee: 0, andTax: tax, andComment: productName, andCustomerInfo: customer as! [String: Any] , andAdditional_data: nil, andReceiptEnable: false)
    }
    
    @IBAction func pressedRegister(_ sender: UIButton) {
        let count = materialList?.count ?? 0
        if count > 0 {
            CommonUtil.showAlert(title: "자재사용 확인", msg: "요청자재가 있습니다\n완료등록 진행 하시겠습니까?", target: self, cancel: {
            }) {
                self.requestRegister()
            }
        } else {
            requestRegister()
        }
    }
    
    @objc func pressedCheck(_ sender: UIButton) {
        let index = sender.tag
        if selectedSafety[index] == "N" {
            selectedSafety[index] = "Y"
        } else {
            selectedSafety[index] = "N"
        }
        safetyTbV.reloadData()
    }
    
    // MARK: CTTextFieldDelegate
    func pressedDone(_ sender: Any, type: NSInteger) {
        if sender is UIDatePicker {
            let datePicker = sender as! UIDatePicker
            let time = CommonUtil.stringFromDate(datePicker.date, format: "HH:mm")
            if type == DateType.start.rawValue {
                startBtn.setTitle(time, for: .normal)
            } else if type == DateType.end.rawValue {
                endBtn.setTitle(time, for: .normal)
            }
        }
    }
    
    func pressedDone(_ sender: Any) {
        if sender is UIDatePicker {
            let datePicker = sender as! UIDatePicker
            let date = CommonUtil.stringFromDate(datePicker.date, format: "yyyy-MM-dd")
            dateBtn.setTitle(date, for: .normal)
        }
    }

    // MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let count = scrollView.bounds.width / selectedLineV.bounds.width
        let index = round(scrollView.contentOffset.x / scrollView.bounds.width)
        if index == 0 && serviceBtn.isSelected {
            changedDetailButton(receiptBtn)
        } else if index == 1 && receiptBtn.isSelected {
            changedDetailButton(serviceBtn)
        }
        selectedLineV.transform = CGAffineTransform(translationX: scrollView.contentOffset.x/count, y: 0)
    }
    
    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if textView.isEqual(repairTV) {
            let frame = textView.frame
            textView.sizeToFit()
            repairHeightLC.constant = textView.contentSize.height
            textView.frame = frame
        } else if textView.isEqual(draftTV) {
            let frame = textView.frame
            textView.sizeToFit()
            draftHeightLC.constant = textView.contentSize.height
            textView.frame = frame
        }
    }

    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.Array.DETAIL_PHOTO_TITLE_LIST.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        let title = Constants.Array.DETAIL_PHOTO_TITLE_LIST[indexPath.row]
        cell.photoBtn.stringTag = title
        cell.photoBtn.addTarget(self, action: #selector(pressedPhoto(_:)), for: .touchUpInside)
        let image = selectedPhotos?[title]
        cell.setData(title: title, image: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        return CGSize(width: (screenWidth - screenWidth * 0.1) / 4 , height: 110)
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        guard var image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
        image = image.resizeImage((Constants.Define.APPDELEGATE.window?.bounds.width)!)
        let referenceUrl = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.referenceURL)]
        if referenceUrl == nil {
            image = image.drawTodayText()
            requestSavePhotoPermission(image)
        }
    
        selectedPhotos?.updateValue(image, forKey: selectedPhotoTitle!)
        picker.dismiss(animated: true, completion: {
            self.photoCV.reloadData()
        })
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(materialTbV) {
            return materialList?.count ?? 0
        } else {
            return Constants.Array.SAFETY_CHECK_LIST.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.isEqual(materialTbV) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialInfoCell") as! MaterialInfoCell
            if let data = materialList?[indexPath.row] {
                cell.setCellFromData(data)
                cell.lineHeightLC.isActive = materialList!.count-1 <= indexPath.row
                cell.confirmBtnHandler = {
                    if data.state == "요청" {
                        let sender = ["requestNo" : data.requestNum as Any, "title" : Constants.Title.POPUP_MATERIAL_CANCEL] as [String : Any]
                        self.openViewController(segue: "InputPopupSegue", sender: sender)
                    } else if data.state == "준비완료" { // 수령확인
                        self.reqeustMaterialUpdateState(data.requestNum, "CA")
                    } else if data.state == "수령확인" { // 수령확인 취소
                        self.reqeustMaterialUpdateState(data.requestNum, "BA")
                    }
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SafetyListCell") as! SafetyListCell
            cell.titleLb.text = Constants.Array.SAFETY_CHECK_LIST[indexPath.row]
            cell.checkBtn.tag = indexPath.row
            cell.checkBtn.addTarget(self, action: #selector(pressedCheck(_:)), for: .touchUpInside)
            cell.checkBtn.isSelected = selectedSafety[indexPath.row] == "Y"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEqual(materialTbV) {
            
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! SafetyListCell
            pressedCheck(cell.checkBtn)
        }
    }
    
    // MARK: ListPopupDelegate
    func selectedData(_ data: Any) {
        if data is CodeListData {
            let codeListData = data as! CodeListData
            let count = selectedSymptomSubList!.count
            var changedIndex = NSNotFound
            for index in 0..<count {
                let sub = selectedSymptomSubList![index]
                if sub.detCdNm == "증상소분류" {
                    selectedSymptomSubList![index] = codeListData
                    changedIndex = index
                    break
                }
            }
            if changedIndex == NSNotFound { // 모두 선택되어 있는 경우 마지막 데이터 변경
                changedIndex = count
            }
            selectedSymptomSubList![changedIndex] = codeListData
            selectedSymptomMainList![changedIndex].commCd = codeListData.commCd
            selectedSymptomMainList![changedIndex].commCdNm = codeListData.commCdNm
            self.resetSymptomView()
        }
    }
    
    // MARK: InputPopupDelegate
    func reloadInputData() {
        requestMaterialList()
    }
    
    // MARK: SignPopupDelegate
    func setSignImage(_ image: UIImage?) {
        savedSignImage = image
        signBtn.isSelected = image != nil ? true : false
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
