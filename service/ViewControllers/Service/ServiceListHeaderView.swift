//
//  ServiceListHeaderView.swift
//  service
//
//  Created by insung on 2018. 7. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ServiceListHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func setHeaderView(_ data: [ServiceListData], sortType: String?) {
        if let firstData = data.first {
            let today = CommonUtil.todayFromDate(format: "yyyy-MM-dd")
            if firstData.visitDate == today {
                titleLb.text = "당일"
            } else if firstData.callTypeReqDt == today {
                titleLb.text = "긴급"
            } else if sortType == "방문예정일순" {
                titleLb.text = firstData.visitDate
            } else {
                titleLb.text = firstData.receiveDate
            }
            countLb.text = String(format: "(%d 건)", data.count)
        }
    }
    
    func setHeaderView(_ data: [ServiceListData]) {
        if let firstData = data.first {
            titleLb.text = firstData.receiveDate
            countLb.text = String(format: "(%d 건)", data.count)
        }
    }
}
