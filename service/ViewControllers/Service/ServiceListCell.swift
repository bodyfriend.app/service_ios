//
//  ServiceListCell.swift
//  service
//
//  Created by insung on 2018. 7. 26..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ServiceListCell: UITableViewCell {
    @IBOutlet weak var promiseBtn: UIButton!
    @IBOutlet weak var visitDateLb: UILabel!
    @IBOutlet weak var productLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var addressLb: UILabel!
    @IBOutlet weak var phoneBtn: UIButton!
    @IBOutlet weak var smsBtn: UIButton!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var productStateV: UIView!
    @IBOutlet weak var askStateV: UIView!
    @IBOutlet var smsHeightLC: NSLayoutConstraint!
    
    var promiseBtnHandler: (() -> Void)?
    var phoneBtnHandler: (() -> Void)?
    var smsBtnHandler: (() -> Void)?
    var mapBtnHandler: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initLayout() {
    }
    
    func setData(_ data: ServiceListData, isMapMode: Bool, isMapSelected: Bool) {
        smsHeightLC.isActive = isMapMode
        mapBtn.isSelected = isMapSelected
        
        var visitTime = "미정"
        var visitDate: String?
        promiseBtn.applyRoundBorder(radius: 20.0, color: COLOR_BORDER_GRAY, width: 2.0)
        promiseBtn.backgroundColor = COLOR_DARKGRAY
        if data.visitTime.isExist == true {
            let dataTime = data.visitTime
            let dataDate = data.visitDate
            visitTime = CommonUtil.changeDateFormat(preFormat: "HHmm", date: dataTime, changeFormat: "HH:mm")!
            visitDate = CommonUtil.changeDateFormat(preFormat: "yyyy-MM-dd", date: dataDate, changeFormat: "MM.dd")
            promiseBtn.applyRoundBorder(radius: 20.0, color: COLOR_BORDER_RED, width: 2.0)
            promiseBtn.backgroundColor = COLOR_RED
        }
        
        promiseBtn.setTitle(visitTime, for: .normal)
        visitDateLb.text = visitDate

        // 제품명 앞뒤에 띄어쓰기 제거
        productLb.text = data.oldProductName?.removeWhitespace()
        nameLb.text = data.userName
        addressLb.text = data.addr
        
        let hasMsg = data.has2Msg == "Y"
        let image = hasMsg ? UIImage(named: "ic_sms_on") : UIImage(named: "ic_sms_off")
        smsBtn.setImage(image, for: .normal)

        var color: UIColor?
        if data.matRequestConfirm == "2" { // 자재 요청 있음
            if data.matRequestConfirm2 == "Y" { // 자재 전체 완료
                color = COLOR_BACKGROUND_GREEN
            } else { // 자재 부분 완료
                color = COLOR_BACKGROUND_ORANGE
            }
        } else { // 자재 요청 없음
            if data.divisionEngineer.isExist == true {
                color = COLOR_BACKGROUND_RED
            } else {
                color = COLOR_WHITE
            }
        }
        contentView.backgroundColor = color
        
        if isMapMode == true {
            selectionStyle = .default
        } else {
            selectionStyle = .none
        }
        setProductStateView(data)
        setAskStateView(data)
    }
    
    func setProductStateView(_ data: ServiceListData) {
        UIView.removeSubviews(productStateV)
        var preV: UIView?
        var priority = 800
        if data.newFlag == "Y" {
            preV = setTextRoundView("NEW", color: COLOR_BORDER_RED, preView: nil, superview: productStateV, priority: priority)
            priority += 1
        }
        if data.costNCost == "1" {
            preV = setTextRoundView("유상", color: COLOR_BORDER_GRAY, preView: preV, superview: productStateV, priority: priority)
            priority += 1
        }
        
        if data.longTermReq.isExist == true {
            preV = setTextRoundView("장기", color: COLOR_BORDER_GRAY, preView: preV, superview: productStateV, priority: priority)
            priority += 1
        }
    }
    
    func setAskStateView(_ data: ServiceListData) {
        UIView.removeSubviews(askStateV)
        var preV: UIView?
        var priority = 800
        if data.isComplain == "Y" {
            preV = setTextRoundView("민원", color: COLOR_BORDER_BLUE, preView: nil, superview: askStateV, priority: priority)
            priority += 1
        }
        if data.progressNo == "3" {
            preV = setTextRoundView("미처리", color: COLOR_BORDER_GRAY, preView: preV, superview: askStateV, priority: priority)
            priority += 1
        }
        if data.etc == "초도" {
            preV = setTextRoundView("초도", color: COLOR_BORDER_GRAY, preView: preV, superview: askStateV, priority: priority)
            priority += 1
        }
        if data.matRequestConfirm == "2" {
            preV = setTextRoundView("자재", color: COLOR_BORDER_PURPLE, preView: preV, superview: askStateV, priority: priority)
            priority += 1
        }
        if data.upgrade == "N" {
            preV = setTextRoundView("온열", color: COLOR_BORDER_RED, preView: preV, superview: askStateV, priority: priority)
            priority += 1
        }
        if let sub = data.overhaulTypeSub, data.overhaulType == "1" {
            preV = setTextRoundView(sub, color: COLOR_BORDER_RED, preView: preV, superview: askStateV, priority: priority)
            priority += 1
        }
    }
    
    func setTextRoundView(_ title: String, color: UIColor, preView: UIView?, superview: UIView, priority: Int) -> TextRoundView {
        let view = TextRoundView.instanceFromNib()
        view.titleLb.textColor = color
        view.titleLb.text = title
        view.applyRoundBorder(radius: 8, color: color, width: 1.4)
        superview.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            if preView == nil {
                make.leading.equalTo(superview)
            } else {
                make.leading.equalTo(preView!.snp.trailing).offset(4)
            }
            make.centerY.equalTo(superview)
            make.trailing.equalTo(superview).priority(priority)
        }
        return view
    }
    
    // MARK: Action Event
    @IBAction func pressedPromise(_ sender: UIButton) {
        promiseBtnHandler?()
    }
    
    @IBAction func pressedPhone(_ sender: UIButton) {
        phoneBtnHandler?()
    }
    @IBAction func pressedSms(_ sender: UIButton) {
        smsBtnHandler?()
    }
    @IBAction func pressedMap(_ sender: UIButton) {
        mapBtnHandler?()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        visitDateLb.text = nil
        productLb.text = nil
        nameLb.text = nil
        addressLb.text = nil
        mapBtn.isSelected = false
    }
}
