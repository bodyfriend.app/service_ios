
//
//  ServiceListViewController.swift
//  service
//
//  Created by insung on 2018. 7. 23..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import MessageUI

class ServiceListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, SearchProtocol, MFMessageComposeViewControllerDelegate {
    
    var listType: ServiceListType?
    var receiveType: String?
    
    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var bottomMapV: UIView!
    @IBOutlet weak var noDataV: UIView!
    @IBOutlet var bottomHeightLC: NSLayoutConstraint!
    
    var requestUrl: String?
    var searchV: ServiceSearchView?
    var listDatas: [String : [ServiceListData]]?
    var productType: String?
    var preMonth: Int = 0
    var promiseSelectedData: ServiceListData?
    var promiseSelectedDate: String?
    var promiseSelectedTime: String?
    var mapSelectedData: [ServiceListData]? = [ServiceListData]()
    var inputTF = CTTextField()
    var isMapMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestCode(Constants.Define.CODE_LOCATION)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ServiceDetailSegue" {
            let vc = segue.destination as! ServiceDetailViewController
            vc.receiveSeq = sender as? String
            vc.target = self
            vc.productType = productType
        } else if segue.identifier == "SmsPopupSegue" {
            let vc = segue.destination as! SmsPopupViewController
            vc.data = sender as? ServiceListData
            vc.productType = productType
        } else if segue.identifier == "MapSegue" {
            let vc = segue.destination as! MapViewController
            vc.receiveData = sender as? [ServiceListData]
            vc.productType = productType
        }
    }
    
    override func initLayout() {
        super.initLayout()
        addPullToRefresh()
        
        if listType == .chair {
            setTopbarForTitle(Constants.Title.TOPBAR_CHAIR_LIST)
            receiveType = ProductType.chair.rawValue
            requestUrl = Constants.URL.SERVICE_LIST
            preMonth = 5
        } else if listType == .product {
            setTopbarForTitle(Constants.Title.TOPBAR_PRODUCT_LIST)
            receiveType = ProductType.product.rawValue
            requestUrl = Constants.URL.SERVICE_LIST
            preMonth = 5
        } else if listType == .yet {
            setTopbarForTitle(Constants.Title.TOPBAR_YET_LIST)
            receiveType = ProductType.yet.rawValue
            requestUrl = Constants.URL.SERVICE_LIST
            preMonth = 5
        } else if listType == .upgrade {
            setTopbarForTitle(Constants.Title.TOPBAR_UPGRADE_LIST)
            receiveType = ProductType.chair.rawValue
            requestUrl = Constants.URL.UPGRADE_LIST
            preMonth = 120
        } else if listType == .overheat {
            setTopbarForTitle(Constants.Title.TOPBAR_OVERHEAT_LIST)
            receiveType = ProductType.chair.rawValue
            requestUrl = Constants.URL.OVERHEAT_LIST
            preMonth = 120
        }
        showRightBtn(.search)
        showMapBtn()
        
        searchV = self.setServiceSearchView()
        searchV?.delegate = self
        
        let headerNib = UINib(nibName: "ServiceListHeaderView", bundle: nil)
        listTbV.register(headerNib, forHeaderFooterViewReuseIdentifier: "ServiceListHeaderView")
        let cellNib = UINib(nibName: "ServiceListCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "ServiceListCell")
        listTbV.tableFooterView = UIView()
        
        inputTF.accessoryDelegate = self
        view.addSubview(inputTF)
    }
    
    /// 뷰컨트롤러 리로드
    override func reloadViewController() {
        requestServiceList()
    }
    
    /// 코드 요청
    ///
    /// - Parameter code: 코드 값
    func requestCode(_ code: String?) {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(code!, forKey: "code")
        ApiManager.requestToPost(Constants.URL.CODE_LIST, parameters: parameters, completion: { (response) in
            guard let data = try? JSONDecoder().decode(CodeListResponse.self, from: response!).resultData! else {
                return
            }
            self.searchV?.initValue(data, preMonth: self.preMonth)
            self.requestServiceList()
        })
    }
    
    /// 검색 리스트 요청
    func requestServiceList() {
        guard var parameters = searchV?.sendParameters() else {
            return
        }
        parameters.updateValue(receiveType!, forKey: "m_receivetype")
        ApiManager.requestToPost(requestUrl!, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(ServiceListResponse.self, from: response!).resultData?.list!) as [ServiceListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            self.setListDatas(datas!)
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    /// 데이터 날짜별 정렬
    ///
    /// - Parameter datas: 데이터
    func setListDatas(_ datas: Array<ServiceListData>) {
        if listDatas != nil {
            listDatas?.removeAll()
            listDatas = nil;
        }
        listDatas = [String : [ServiceListData]]()

        let today = CommonUtil.todayFromDate(format: "yyyy-MM-dd")
        let sortDatas = datas.sorted { $0.visitTime! < $1.visitTime! }
        for data in sortDatas {
            var sortKey = data.receiveDate
            if data.visitDate == today {
                sortKey = "당일"
            } else if data.callTypeReqDt == today {
                sortKey = "긴급"
            } else if searchV?.sortBtn.titleLabel?.text == "방문예정일순" {
                sortKey = data.visitDate
            }
            
            if let key = sortKey {
                var lists = listDatas?[key]
                if lists == nil {
                    lists = [ServiceListData]()
                }
                lists?.append(data)
                listDatas?.updateValue(lists!, forKey: key)
            }
        }
    }
    
    /// 당일, 긴급 키값 추가
    ///
    /// - Returns: 키값 데이터
    func restoreKeys() -> [String] {
        var keys = Array(listDatas!.keys)
        keys.sort { $0 < $1 }
        
        var restoreKeys = [String]()
        if keys.contains("긴급") {
            restoreKeys.append("긴급")
        }
        if keys.contains("당일") {
            restoreKeys.append("당일")
        }
        for key in keys {
            if key != "당일" && key != "긴급" {
                restoreKeys.append(key)
            }
        }
        return restoreKeys
    }
    
    /// 날짜 수정 요청
    ///
    /// - Parameter parameters: 파라미터
    func requestDateModify(_ parameters: Dictionary<String, Any>) {
        ApiManager.requestToPost(Constants.URL.PROMISE_DATE_MODIFY, parameters: parameters, isLoading: false, completion: { (response) in
            guard let _ = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            self.inputTF.showDatePickerView(.time)
        })
    }
    
    /// 시간 수정 요청
    ///
    /// - Parameter parameters: 파라미터
    func requestTimeModify(_ parameters: Dictionary<String, Any>) {
        ApiManager.requestToPost(Constants.URL.PROMISE_TIME_MODIFY, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            self.sendSms()
        })
    }
    
    /// 약속 취소 요청
    func requestPromiseCancel() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
        parameters.updateValue(self.receiveType!, forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.PROMISE_CANCEL, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 데이터에 타입이 있으면 사용하고 없으면 'M'
    ///
    /// - Parameter data: 사용 데이터
    /// - Returns: 타입
    func receiveTypeData(_ data: ServiceListData) -> String {
        if let receiveType = data.receiveType {
            return receiveType
        } else {
            if let mReceiveType = data.mReceiveType {
                return mReceiveType
            } else {
                return "M"
            }
        }
    }
    
    /// 메세지 전송
    func sendSms() {
        guard let data = promiseSelectedData else {
            CommonUtil.showMessage("메세지를 전송할수 없습니다")
            return
        }
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var text = String(format: Constants.Format.SMS_INFO, data.userName ?? "", (userData?.adminNm)!)
        if let phoneNo = promiseSelectedData?.phoneNo {
            if let visitDate = promiseSelectedDate, promiseSelectedDate.isExist == true {
                let date = CommonUtil.changeDateFormat(preFormat: "yyyy-MM-dd", date: visitDate, changeFormat: "MM월 dd일")
                if let visitTime = promiseSelectedTime, promiseSelectedTime.isExist == true {
                    let time = CommonUtil.dateFromString(visitTime, format: "HHmm")
                    let preDate = time?.addingTimeInterval(-1800) // 30분 전
                    let nextDate = time?.addingTimeInterval(1800) // 30분 후
                    let sPreTime = CommonUtil.stringFromDate(preDate!, format: "a hh시 mm분")
                    let sNextTime = CommonUtil.stringFromDate(nextDate!, format: "a hh시 mm분")
                    text.append(String(format: Constants.Format.SMS_BOOK, "", date!, sPreTime, sNextTime))
                    
                    // 메세지 전송
                    let messageC = MFMessageComposeViewController()
                    messageC.messageComposeDelegate = self
                    messageC.recipients = [phoneNo]
                    messageC.body = text
                    self.present(messageC, animated: true, completion: {
                        if let seq = data.receiveSeq {
                            self.requestAppLog(seq: seq, tel: phoneNo, msg: text, msg_gb: "2", productType: self.receiveType ?? "")
                        }
                    })
                }
            }
        }
    }
    
    /// 선택된 맵 데이터
    ///
    /// - Parameter data: 데이터
    func selectedMapData(_ data: ServiceListData) {
        let index = mapSelectedData?.firstIndex(where: { $0 == data } )
        if index == nil {
            mapSelectedData?.append(data)
        } else {
            mapSelectedData?.remove(at: index!)
        }
        listTbV.reloadData()
    }
    
    // MARK: Action Event
    @objc override func refreshData(_ sender: UIRefreshControl) {
        super.refreshData(sender)
        requestServiceList()
    }

    @IBAction func pressedRetry(_ sender: UIButton) {
        requestServiceList()
    }
    
    override func pressedMapEdit(_ sender: UIButton) {
        isMapMode = !isMapMode
        if isMapMode {
            sender.setTitle("취소", for: .normal)
        } else {
            mapSelectedData?.removeAll()
            sender.setTitle("지도", for: .normal)
        }
        bottomHeightLC.isActive = !isMapMode
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
        listTbV.reloadData()
    }
    
    @IBAction func pressedMapSee(_ sender: UIButton) {
        guard let data = mapSelectedData else {
            return
        }
        guard let firstData = data.first else {
            return
        }
        
        productType = receiveTypeData(firstData)
        openViewController(segue: "MapSegue", sender: data)
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate 
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let keys = listDatas?.keys else {
            return 0
        }
        listTbV.isHidden = keys.count == 0
        noDataV.isHidden = !listTbV.isHidden
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listDatas != nil {
            let keys = restoreKeys()
            guard let data = listDatas?[keys[section]] else {
                return 0
            }
            return data.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ServiceListHeaderView") as! ServiceListHeaderView
        let keys = restoreKeys()
        guard let data = listDatas?[keys[section]] else {
            return header
        }
        let sortType = searchV?.sortBtn.titleLabel?.text
        header.setHeaderView(data, sortType: sortType)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceListCell") as! ServiceListCell
        let keys = restoreKeys()
        let key = keys[indexPath.section]
        guard let datas = listDatas?[key] else {
            return cell
        }
        let data = datas[indexPath.row]
        
        cell.promiseBtnHandler = {
            self.promiseSelectedData = datas[indexPath.row]
            var titles = ["지정일 지정", "약속 시간 변경"]
            if self.promiseSelectedData?.visitTime.isExist == true {
                titles.append("약속 취소")
            }
            
            CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
                if index == 0 {
                    self.inputTF.showDatePickerView(.date)
                } else if index == 1 {
                    self.inputTF.showDatePickerView(.time)
                } else if index == 2 {
                    CommonUtil.showAlert(msg: "약속을 취소하시겠습니까?", target: self, cancel: {}, confirm: {
                        self.requestPromiseCancel()
                    })
                }
            }
        }
        
        cell.phoneBtnHandler = {
            if let phoneNo = data.phoneNo {
                CommonUtil.openLink(.call, phoneNo)
            } else {
                CommonUtil.showMessage("연락처가 없습니다")
            }
        }
        
        cell.smsBtnHandler = {
            if let _ = data.phoneNo {
                self.productType = self.receiveTypeData(data)
                if MFMessageComposeViewController.canSendText() {
                    self.openViewController(segue: "SmsPopupSegue", sender: data)
                } else {
                    CommonUtil.showMessage("메세지를 사용할 수 없습니다")
                }
            } else {
                CommonUtil.showMessage("연락처가 없습니다")
            }
        }
        
        cell.mapBtnHandler = {
            if self.isMapMode == true {
                self.selectedMapData(data)
            } else {
                self.productType = self.receiveTypeData(data)
                self.openViewController(segue: "MapSegue", sender: [data])
            }
        }
        
        let isSelected = mapSelectedData?.contains(where: { $0 == data })
        
        cell.setData(data, isMapMode: isMapMode, isMapSelected: isSelected!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keys = restoreKeys()
        guard let datas = listDatas?[keys[indexPath.section]] else {
            return
        }
        let data = datas[indexPath.row]
        if isMapMode {
            tableView.deselectRow(at: indexPath, animated: false)
            selectedMapData(data)
        } else {
            let receiveSeq = data.receiveSeq
            productType = receiveTypeData(data)
            openViewController(segue: "ServiceDetailSegue", sender: receiveSeq!)
        }
    }
    
    // MARK: SearchProtocol
    func requestSearch() {
        requestServiceList()
    }
    
    // MARK: CTTextFieldDelegate
    func pressedDone(_ sender: Any) {
        if sender is UIDatePicker {
            let dp = sender as! UIDatePicker
            if dp.datePickerMode == .date {
                var parameters = Dictionary<String, Any>()
                parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
                parameters.updateValue(self.receiveType!, forKey: "m_receivetype")
                let date = CommonUtil.stringFromDate(dp.date, format: "yyyy-MM-dd")
                promiseSelectedDate = date
                parameters.updateValue(date, forKey: "visitdate")
                self.requestDateModify(parameters)
            } else if dp.datePickerMode == .time {
                var parameters = Dictionary<String, Any>()
                parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
                parameters.updateValue(self.receiveType!, forKey: "m_receivetype")
                let time = CommonUtil.stringFromDate(dp.date, format: "HHmm")
                if promiseSelectedDate.isExist == false {
                    promiseSelectedDate = promiseSelectedData?.visitDate
                }
                promiseSelectedTime = time
                if let hour = Int(CommonUtil.stringFromDate(dp.date, format: "HH")) {
                    if hour > 7 && hour < 22 {
                        parameters.updateValue(time, forKey: "visittime")
                        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
                        parameters.updateValue((userData?.adminNm)!, forKey: "userName")
                        if let phoneNo = promiseSelectedData?.phoneNo {
                            parameters.updateValue(phoneNo, forKey: "phoneNumber")
                        }
                        self.requestTimeModify(parameters)
                    } else {
                        CommonUtil.showMessage("8시부터 21시까지만 설정이 가능합니다")
                        return
                    }
                }
            }
        }
    }
    
    // MARK: MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        if result == .sent {
            CommonUtil.showMessage("메세지를 전송했습니다")
        } else if result == .failed {
            CommonUtil.showMessage("메세지를 전송하지 못했습니다")
        }
        controller.dismiss(animated: true, completion: {
            self.requestServiceList()
        })
    }
}
