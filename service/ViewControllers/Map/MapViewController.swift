//
//  MapViewController.swift
//  service
//
//  Created by insung on 2018. 8. 28..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

let DEFAULT_POSITION = CLLocationCoordinate2D(latitude: 37.6, longitude: 127)

class MapViewController: BaseViewController, GMSMapViewDelegate {
    var receiveData: [ServiceListData]?
    var productType: String?
    
    @IBOutlet weak var mapV: GMSMapView!
    @IBOutlet weak var descriptionV01: UIView!
    @IBOutlet weak var descriptionV02: UIView!
    @IBOutlet weak var descriptionV03: UIView!
    @IBOutlet weak var descriptionV04: UIView!
    @IBOutlet weak var bottomV: UIView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var addressLb: UILabel!
    @IBOutlet weak var promiseBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet var bottomHeightLC: NSLayoutConstraint!
    
    var promiseSelectedData: ServiceListData?
    var inputTF = CTTextField()
    var mapBounds = GMSCoordinateBounds()
    var positionList = [CLLocationCoordinate2D]()

    override func viewDidLoad() {
        super.viewDidLoad()
        requestGeograph()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ServiceDetailSegue" {
            let vc = segue.destination as! ServiceDetailViewController
            vc.receiveSeq = sender as? String
            vc.target = self
            vc.productType = productType
        }
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("지도보기")
        
        descriptionV01.applyRoundBorder(descriptionV01.bounds.height/2)
        descriptionV02.applyRoundBorder(descriptionV02.bounds.height/2)
        descriptionV03.applyRoundBorder(descriptionV03.bounds.height/2)
        descriptionV04.applyRoundBorder(descriptionV04.bounds.height/2)
        
        promiseBtn.applyRoundBorder()
        callBtn.applyRoundBorder()
        detailBtn.applyRoundBorder()
        
        mapV.delegate = self
        mapV.settings.compassButton = true
        mapV.settings.myLocationButton = true
        mapV.isMyLocationEnabled = true

        inputTF.accessoryDelegate = self
        view.addSubview(inputTF)
        
        cameraInitPostion()
        showBottomMenu(false)
    }
    
    /// 하단 메뉴 뷰
    ///
    /// - Parameter isShow: 보여짐 여부
    func showBottomMenu(_ isShow: Bool) {
        self.bottomHeightLC.isActive = !isShow
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    /// 지오코딩 요청
    func requestGeograph() {
        mapV.clear()
        for data in receiveData! {
            requestGeocoding(data)
        }
    }
    
    /// 네이버 지오코딩 요청
    ///
    /// - Parameter data: 데이터
    func requestGeocoding(_ data: ServiceListData) {
        guard let address = data.addr else {
            return
        }
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(address, forKey: "fullAddr")
        let headers: HTTPHeaders = ["appKey": Constants.Define.MAP_CLIENT_KEY]
        Alamofire.request(Constants.URL.MAP_GEOCODING, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
              .responseJSON { (response) in
                switch response.result {
                case .success:
                        if let resp = response.result.value as? Dictionary<String, Any> {
                            if let result = resp["coordinateInfo"] as? Dictionary<String, Any> {
                                if let items = result["coordinate"] as? Array<Any> {
                                    if let item = items.first as? Dictionary<String, Any> {
                                        var longitude: Double?
                                        var latitude: Double?
                                        if let lon = item["lon"] as? String, lon.isExist {
                                            longitude = Double(lon)
                                        } else {
                                            if let newLon = item["newLon"] as? String, newLon.isExist {
                                                longitude = Double(newLon)
                                            }
                                        }
                                        if let lat = item["lat"] as? String, lat.isExist {
                                            latitude = Double(lat)
                                        } else {
                                            if let newLat = item["newLat"] as? String, newLat.isExist {
                                                latitude = Double(newLat)
                                            }
                                        }
                                        self.reloadSearchMarker(CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!), data: data)
                                    }
                                }
                            } else {
                                self.failedAddressSearch("통신이 원활하지 않습니다")
                            }
                        }
                    break
                case .failure:
                    self.failedAddressSearch("통신이 원활하지 않습니다")
                    break
                }
        }
    }
    
    /// 지도 검색 실패
    ///
    /// - Parameter message: 메세지
    func failedAddressSearch(_ message: String) {
        cameraInitPostion()
        CommonUtil.showMessage(message)
    }
    
    /// 카메라 위치 초기화
    func cameraInitPostion() {
        let camera = GMSCameraPosition.camera(withTarget: DEFAULT_POSITION, zoom: 10)
        mapV.animate(to: camera)
    }
    
    /// 마커 리로드
    ///
    /// - Parameters:
    ///   - position: 위치
    ///   - data: 데이터
    func reloadSearchMarker(_ position: CLLocationCoordinate2D, data: ServiceListData) {
        let marker = GMSMarker()
        marker.userData = data
        let markerV = MarkerView.init(frame: .zero)
        marker.icon = markerV.iconFromData(data)
        marker.position = position
        marker.map = mapV
        positionList.append(position)
        if receiveData?.last == data {
            cameraUpdate()
        }
    }
    
    /// 카메라 위치 업데이트
    func cameraUpdate() {
        for position in positionList {
            mapBounds = mapBounds.includingCoordinate(position)
        }
        let cameraUpdate = GMSCameraUpdate.fit(mapBounds, withPadding: 60.0)
        mapV.moveCamera(cameraUpdate)
    }
    
    /// 지정일 수정 요청
    ///
    /// - Parameter parameters: 파라미터
    func requestDateModify(_ parameters: Dictionary<String, Any>) {
        ApiManager.requestToPost(Constants.URL.PROMISE_DATE_MODIFY, parameters: parameters, isLoading: false, completion: { (response) in
            guard let _ = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                return
            }
            self.inputTF.showDatePickerView(.time)
        })
    }
    
    /// 약속 시간 설정 요청
    ///
    /// - Parameter parameters: 파라미터
    func requestTimeModify(_ parameters: Dictionary<String, Any>) {
        ApiManager.requestToPost(Constants.URL.PROMISE_TIME_MODIFY, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 약속 취소 요청
    func requestPromiseCancel() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
        parameters.updateValue(self.productType!, forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.PROMISE_CANCEL, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: Action Event
    @IBAction func pressedPromise(_ sender: UIButton) {
        var titles = ["지정일 지정", "약속 시간 변경"]
        if promiseSelectedData?.visitTime.isExist == true {
            titles.append("약속 취소")
        }
        
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            if index == 0 {
                self.inputTF.showDatePickerView(.date)
            } else if index == 1 {
                self.inputTF.showDatePickerView(.time)
            } else if index == 2 {
                CommonUtil.showAlert(msg: "약속을 취소하시겠습니까?", target: self, cancel: {}, confirm: {
                    self.requestPromiseCancel()
                })
            }
        }
    }
    
    @IBAction func pressedCall(_ sender: UIButton) {
        guard let phoneNo = promiseSelectedData?.phoneNo else {
            return
        }
        CommonUtil.openLink(.call, phoneNo)
    }
    
    @IBAction func pressedDetail(_ sender: UIButton) {
        let receiveSeq = promiseSelectedData?.receiveSeq
        openViewController(segue: "ServiceDetailSegue", sender: receiveSeq!)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let data = marker.userData as! ServiceListData
        promiseSelectedData = marker.userData as? ServiceListData
        nameLb.text = data.userName
        addressLb.text = data.addr
        showBottomMenu(true)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        showBottomMenu(false)
    }
    
    // MARK: CTTextFieldDelegate
    func pressedDone(_ sender: Any) {
        if sender is UIDatePicker {
            let dp = sender as! UIDatePicker
            if dp.datePickerMode == .date {
                var parameters = Dictionary<String, Any>()
                parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
                parameters.updateValue(self.productType!, forKey: "m_receivetype")
                let time = CommonUtil.stringFromDate(dp.date, format: "yyyy-MM-dd")
                parameters.updateValue(time, forKey: "visitdate")
                self.requestDateModify(parameters)
            } else if dp.datePickerMode == .time {
                var parameters = Dictionary<String, Any>()
                parameters.updateValue((promiseSelectedData?.receiveSeq)!, forKey: "receive_seq")
                parameters.updateValue(self.productType!, forKey: "m_receivetype")
                let time = CommonUtil.stringFromDate(dp.date, format: "HHmm")
                if let hour = Int(CommonUtil.stringFromDate(dp.date, format: "HH")) {
                    if hour > 7 && hour < 22 {
                        parameters.updateValue(time, forKey: "visittime")
                        self.requestTimeModify(parameters)
                    } else {
                        CommonUtil.showMessage("8시부터 21시까지만 설정이 가능합니다")
                        return
                    }
                }
            }
        }
    }
}
