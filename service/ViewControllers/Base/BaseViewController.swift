//
//  BaseViewController.swift
//  service
//
//  Created by insung on 2018. 6. 15..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import SideMenu

enum TopbarType: Int {
    case main
    case search
    case video
}

class BaseViewController: UIViewController, UIScrollViewDelegate, InputTextProtocol {
    @IBOutlet var topbarBackV: UIView!
    @IBOutlet var baseSV: UIScrollView?
    var topbarV: TopbarView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initValue()
        initLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        KeyboardManager.addObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        KeyboardManager.hide()
        KeyboardManager.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    public func initValue() {}
    public func initLayout() {}
    public func reloadViewController() {}
    
    /// 상단바 설정 (메인)
    public func setTopbarForMain() {
        setTopbar()
        topbarV?.menuBtn.isHidden = false
        topbarV?.promotionBtn.isHidden = false
        topbarV?.noticeBtn.isHidden = false
//        topBarV.delegate = self;
    }
    
    /// 상단바 설정 (타이틀)
    ///
    /// - Parameter title: title
    public func setTopbarForTitle(_ title: String?) {
        setTopbar()
        topbarV?.titleLb.text = title
        topbarV?.titleLb.isHidden = false
        topbarV?.backBtn.isHidden = false
    }
    
    /// 상단바 생성
    func setTopbar() {
        if topbarV != nil {
            return
        }
        topbarV = TopbarView.instanceFromNib()
        topbarBackV.addSubview(topbarV!)

        topbarV?.applyAutoLayoutFromSuperview(topbarBackV)
        
        topbarV?.menuBtn.addTarget(self, action: #selector(pressedMenu(_:)), for: .touchUpInside)
        topbarV?.backBtn.addTarget(self, action: #selector(pressedBack(_:)), for: .touchUpInside)
        topbarV?.promotionBtn.addTarget(self, action: #selector(pressedPromotion(_:)), for: .touchUpInside)
        topbarV?.noticeBtn.addTarget(self, action: #selector(pressedNotice(_:)), for: .touchUpInside)
        topbarV?.searchBtn.addTarget(self, action: #selector(pressedSearch(_:)), for: .touchUpInside)
        topbarV?.videoBtn.addTarget(self, action: #selector(pressedVideo(_:)), for: .touchUpInside)
        topbarV?.mapBtn.addTarget(self, action: #selector(pressedMapEdit(_:)), for: .touchUpInside)
    }
    
    /// 당겨서 새로고침 추가
    func addPullToRefresh() {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refreshData(_ :)), for: .valueChanged)
        baseSV?.addSubview(rc)
        baseSV?.sendSubviewToBack(rc)
    }
    
    /// 검색 뷰 생성
    ///
    /// - Returns: 검색 뷰
    func setServiceSearchView() -> ServiceSearchView {
        let searchV = ServiceSearchView.instanceFromNib()
        searchV.target = self
        topbarV?.optionV.addSubview(searchV)
        topbarV?.optionHeightLC.isActive = false
        searchV.applyAutoLayoutFromSuperview((topbarV?.optionV)!)
        return searchV
    }
    
    /// 우측 버튼 노출
    ///
    /// - Parameter type: 상단바 타입
    func showRightBtn(_ type: TopbarType) {
        topbarV?.noticeBtn.isHidden = true
        topbarV?.searchBtn.isHidden = true
        topbarV?.videoBtn.isHidden = true
        if type == .main {
            topbarV?.noticeBtn.isHidden = false
        } else if type == .search {
            topbarV?.searchBtn.isHidden = false
        } else if type == .video {
            topbarV?.videoBtn.isHidden = false
        }
    }
    
    /// 지도 버튼 노출
    func showMapBtn() {
        topbarV?.mapBtn.isHidden = false
    }
    
    /// 뒤로 버튼 숨김
    func hideBackBtn() {
        topbarV?.backBtn.isHidden = true
    }
    
    /// 뷰컨트롤러 이동
    ///
    /// - Parameters:
    ///   - segue: segue
    ///   - sender: 전달 객체
    func openViewController(segue: String, sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: segue, sender: sender)
        }
    }
    
    /// 뷰컨트롤러 닫기
    func closeViewController() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /// 뷰컨트롤러 닫기 후 페이지 리로드
    ///
    /// - Parameter preVC: 리로드 될 페이지
    func closeViewController(_ preVC: BaseViewController?) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                if let vc = preVC {
                    vc.reloadViewController()
                }
            })
        }
    }
    
    /// 앱로그 전송
    ///
    /// - Parameters:
    ///   - seq: seq
    ///   - tel: tel
    ///   - msg: msg
    ///   - msg_gb: msg_gb
    ///   - productType: 제품 타입
    func requestAppLog(seq: String, tel: String, msg: String, msg_gb: String, productType: String) {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var parameters = Dictionary<String, Any>()
        parameters.updateValue((userData?.adminId)!, forKey: "admin_id")
        parameters.updateValue((userData?.adminNm)!, forKey: "admin_nm")
        parameters.updateValue(seq, forKey: "svc_seq")
        parameters.updateValue(tel, forKey: "tel_no")
        parameters.updateValue(userData?.groupCd ?? "", forKey: "group_cd")
        parameters.updateValue(userData?.perCd ?? "", forKey: "per_cd")
        parameters.updateValue(msg, forKey: "msg")
        parameters.updateValue(msg_gb, forKey: "msg_gb")
        parameters.updateValue(productType, forKey: "product_type")
        ApiManager.requestToPost(Constants.URL.APP_LOG, parameters: parameters) { (data) in }
    }
    
    /// 데이터 모델링 실패 시
    ///
    /// - Parameter msg: message
    func failedDataModeling(_ msg: String) {
        CommonUtil.showMessage(msg)
        LoadingView.hideLoadingView()
    }
    
    /// 지도 수정 모드
    ///
    /// - Returns: 수정 모드 여부
    func mapEditMode() -> Bool {
        return (topbarV?.mapBtn.isSelected)!
    }
    
    /// 당겨서 새로고침 시
    ///
    /// - Parameter sender: sender
    @objc func refreshData(_ sender: UIRefreshControl) {
        sender.endRefreshing()
    }
    
    @objc func pressedMenu(_ sender: UIButton) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @objc func pressedBack(_ sender: UIButton) {
        closeViewController()
    }
    
    @objc func pressedPromotion(_ sender: UIButton) {
        openViewController(segue: "PromotionSegue", sender: self)
    }
    
    @objc func pressedNotice(_ sender: UIButton) {
        openViewController(segue: "NoticeListSegue", sender: self)
    }
    
    @objc func pressedSearch(_ sender: UIButton) {
        let isShow = topbarV?.optionHeightLC.isActive
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.topbarV?.optionHeightLC.isActive = !isShow!
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func pressedVideo(_ sender: UIButton) {}
    
    @objc func pressedMapEdit(_ sender: UIButton) {}
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        let info = notification.userInfo
        let originalKeyboardFrame = info![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let keyboardFrame = UIApplication.shared.keyWindow?.convert(originalKeyboardFrame, to: baseSV)
        let top = baseSV?.contentInset.top
        let contentInsets = UIEdgeInsets(top: top ?? 0, left: 0, bottom: keyboardFrame!.height, right: 0)
        if baseSV?.contentInset != contentInsets {
            baseSV?.contentInset = contentInsets
            baseSV?.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        let top = baseSV?.contentInset.top
        baseSV?.contentInset = UIEdgeInsets(top: top ?? 0, left: 0, bottom: 0, right: 0)
        baseSV?.scrollIndicatorInsets = .zero
    }
}

