//
//  MainViewController.swift
//  service
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import SideMenu

class MainViewController: BaseViewController {
    @IBOutlet weak var chairLb: UILabel!
    @IBOutlet weak var serviceLb: UILabel!
    @IBOutlet weak var unprocessLb: UILabel!
    @IBOutlet weak var yetLb: UILabel!
    @IBOutlet weak var vipLb: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestMainInfo()
//        requestSalary()
        registerPaySDK()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ServiceListSegue" {
            let vc = segue.destination as! ServiceListViewController
            vc.listType = sender as? ServiceListType
        } else if segue.identifier == "MapSegue" {
            let vc = segue.destination as! MapViewController
            vc.receiveData = sender as? [ServiceListData]
            vc.productType = "M"
        } else if segue.identifier == "MaterialRequestSegue" {
            let vc = segue.destination as! MaterialRequestViewController
            vc.target = self
            vc.topbarTitle = sender as? String
        }
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForMain()
        reloadSideMenu()
        addPullToRefresh()
    }
    
    override func reloadViewController() {
        requestMainInfo()
    }
    
    /// 사이드 메뉴 리로드
    func reloadSideMenu() {
        let menuNavi = SideMenuManager.default.menuLeftNavigationController
        if let menuVC = menuNavi?.topViewController as? SideMenuViewController {
            menuVC.reloadData()
        }
    }
    
    func registerPaySDK() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        PayatSDkManager.initialize()
        PayatSDkManager.initPayatSDKStoreId("bodyfriendgo", andEmployee_ID: userData?.payatId)
    }
    
    /// 메인 정보 요청
    func requestMainInfo() {
        ApiManager.requestToPost(Constants.URL.MAIN_INFO, parameters: nil, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MainResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    CommonUtil.showMessage(decode.resultMsg)
                    LoadingView.hideLoadingView()
                    return
                }
                self.chairLb.text = data.cntM
                self.serviceLb.text = data.cntH
//                self.unprocessLb.text = data.cntU
                self.yetLb.text = data.cntU
                self.vipLb.text = data.cntA
//                self.requestMainUpgrade()
                self.requestPrepareMaterial()
            } else {
                LoadingView.hideLoadingView()
            }
        })
    }
    
    /// 수당 조회
    func requestSalary() {
        var parameters = Dictionary<String, Any>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminNm)!, forKey: "engineerNm")
        let year = CommonUtil.stringFromDate(Date(), format: "yyyy")
        let month = CommonUtil.stringFromDate(Date(), format: "M")
        parameters.updateValue(year, forKey: "year")
        parameters.updateValue(month, forKey: "month")
        ApiManager.requestToPost(Constants.URL.SALARY_LIST, parameters: parameters, isLoading: false, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(SalaryResponse.self, from: response!).resultData else {
                LoadingView.hideLoadingView()
                return
            }
            self.setTotalPrice(datas)
        })
    }

    /// 수당 총합 계산
    ///
    /// - Parameter listDatas: 수당 리스트
    func setTotalPrice(_ listDatas: [SalaryListData]?) {
        guard let datas = listDatas else {
            return
        }
        var totalPrice = 0
        for data in datas {
            totalPrice += data.distTot! + data.weekDaysTot! + data.weekendTot!
        }
        let salary = String(totalPrice).commaToDecimal
        topbarV?.salaryLb.text = String(format: "%@원", salary)
        topbarV?.salaryLb.isHidden = false
    }

//    /// 메인 업그레이드 요청
//    func requestMainUpgrade() {
//        ApiManager.requestToPost(Constants.URL.MAIN_UPGRADE, parameters: nil, completion: { (response) in
//            if let decode = try? JSONDecoder().decode(MainResponse.self, from: response!) {
//                guard let data = decode.resultData else {
//                    CommonUtil.showMessage(decode.resultMsg)
//                    return
//                }
//                self.upgradeLb.text = data.cntUH
//            }
//            LoadingView.hideLoadingView()
//        })
//    }
    
    func requestPrepareMaterial() {
        ApiManager.requestToPost(Constants.URL.PREPARE_MATERIAL, parameters: nil, completion: { (response) in
            if let decode = try? JSONDecoder().decode(PrepareMaterialResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    LoadingView.hideLoadingView()
                    return
                }
                let msg = String(format: "섹터 준비완료건 : %@건\n신청 준비완료건 : %@건", data.cntArea ?? "0", data.cntEngineer ?? "0")
                CommonUtil.showAlert(title: "자재 준비완료", msg: msg, target: self, cancel: nil, confirm: nil)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 지도 데이터 요청
    func requestMapList() {
        var parameters = Dictionary<String, Any>()
        let startDate = CommonUtil.todayFromDate(format: "yyyy-MM-dd")
        let nextDate = CommonUtil.nextMonthDate(Date(), nextMonth: 2)
        let endDate = CommonUtil.stringFromDate(nextDate, format: "yyyy-MM-dd")
        parameters.updateValue(startDate, forKey: "m_sdate")
        parameters.updateValue(endDate, forKey: "m_edate")
        parameters.updateValue("M", forKey: "m_receivetype")
        ApiManager.requestToPost(Constants.URL.SERVICE_LIST, parameters: parameters, completion: { (response) in
            guard let datas = try? JSONDecoder().decode(ServiceListResponse.self, from: response!).resultData?.list! else {
                LoadingView.hideLoadingView()
                return
            }
            self.openViewController(segue: "MapSegue", sender: datas)
            LoadingView.hideLoadingView()
        })
    }
    
    /// 로그아웃 요청
    func requestLogout() {
        ApiManager.requestToPost(Constants.URL.LOGOUT, parameters: nil, completion: { (response) in
            UserManager.removeUserData()
            CommonUtil.moveLoginViewController()
            LoadingView.hideLoadingView()
        })
    }

    // MARK: Action Event
    @objc override func refreshData(_ sender: UIRefreshControl) {
        super.refreshData(sender)
        reloadViewController()
    }
    
    @IBAction func tappedChair(_ sender: Any) {
        openViewController(segue: "ServiceListSegue", sender: ServiceListType.chair)
    }
    
    @IBAction func tappedProduct(_ sender: Any) {
        openViewController(segue: "ServiceListSegue", sender: ServiceListType.product)
    }
    
    @IBAction func tappedYet(_ sender: Any) {
        openViewController(segue: "ServiceListSegue", sender: ServiceListType.yet)
    }
        
    @IBAction func tappedVip(_ sender: Any) {
        openViewController(segue: "VipListSegue", sender: self)
    }
    
    @IBAction func pressedSchedule(_ sender: Any) {
        openViewController(segue: "ScheduleSegue", sender: self)
    }
    
    @IBAction func pressedInfo(_ sender: Any) {
        openViewController(segue: "ProductInfoSegue", sender: self)
    }
    
    @IBAction func pressedMap(_ sender: Any) {
        requestMapList()
    }
    
    @IBAction func pressedSell(_ sender: Any) {
        openViewController(segue: "SaleSegue", sender: self)
    }

}
