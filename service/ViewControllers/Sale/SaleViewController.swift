//
//  SaleViewController.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SaleViewController: BaseViewController {
    
    @IBOutlet weak var modelBtn: UIButton!
    @IBOutlet weak var saleTypeBtn: UIButton!
    @IBOutlet weak var newTypeBtn: UIButton!
    @IBOutlet weak var nameTF: CTTextField!
    @IBOutlet weak var serviceNameLb: UILabel!
    @IBOutlet weak var phoneTF: CTTextField!
    @IBOutlet weak var telTF: CTTextField!
    @IBOutlet weak var etcTF: CTTextField!
    
    var saleTypeList = Constants.Array.SALE_TYPE_LIST
    var newTypeList = Constants.Array.SALE_NEW_TYPE_LIST
    var productData: [ProductInfoData]?
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestProductList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("상품판매")
        
        modelBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        saleTypeBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        newTypeBtn.applyRoundBorder(COLOR_BORDER_GRAY)

        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        serviceNameLb.text = userData?.adminNm
    }

    /// 제품 리스트 요청
    func requestProductList() {
        var parameters = Dictionary<String, String>()
        parameters.updateValue("M", forKey: "producttype")
        ApiManager.requestToPost(Constants.URL.PRODUCT_INFO_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(ProductInfoResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    CommonUtil.showMessage(decode.resultMsg)
                    LoadingView.hideLoadingView()
                    return
                }
                if self.productData != nil {
                    self.productData = nil
                }
                self.productData = data.list
                self.changeModelTitle(0)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 제품 등록 요청
    func requestRegister() {
        var message: String?
        if modelBtn.titleLabel?.text.isExist == false {
            message = "제품을 선택해주세요"
        } else if nameTF.text.isExist == false {
            message = "고객명을 입력해주세요"
        } else if telTF?.text.isExist == false && phoneTF?.text.isExist == false {
            message = "연락처를 선택해주세요"
        } else if saleTypeBtn.titleLabel?.text.isExist == false {
            message = "렌탈/판매를 선택해주세요"
        } else if newTypeBtn.titleLabel?.text.isExist == false {
            message = "기존/신규를 선택해주세요"
        }
        
        if message.isExist == true {
            CommonUtil.showMessage(message!)
            return
        }
        
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var parameters = Dictionary<String, Any>()
        parameters.updateValue((userData?.adminId)!, forKey: "managerid")
        parameters.updateValue((userData?.adminNm)!, forKey: "managername")
        parameters.updateValue(ProductType.chair.rawValue, forKey: "producttype")
        parameters.updateValue(saleTypeBtn.titleLabel?.text ?? "", forKey: "salestype")
        parameters.updateValue(newTypeBtn.titleLabel?.text ?? "", forKey: "ntype")
        parameters.updateValue(modelBtn.titleLabel?.text ?? "", forKey: "productname")
        parameters.updateValue(nameTF.text ?? "", forKey: "inusername")
        parameters.updateValue(telTF?.text ?? "", forKey: "intel")
        parameters.updateValue(phoneTF?.text ?? "", forKey: "inhandphone")
        parameters.updateValue(etcTF.text ?? "", forKey: "etcclausal")
        
        ApiManager.requestToPost(Constants.URL.SALE_REGISTER, parameters: parameters, completion: { (response) in
            guard let data = try? JSONDecoder().decode(ProductInfoDetailResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            CommonUtil.showMessage(data.resultMsg)
            self.closeViewController()
            LoadingView.hideLoadingView()
        })
    }

    /// 모델명 변경시
    ///
    /// - Parameter index: index
    func changeModelTitle(_ index: Int) {
        guard let data = productData else {
            return
        }
        guard let productName = data[index].productName else {
            return
        }
        modelBtn.setTitle(productName, for: .normal)
    }
    
    /// 판매 타이틀 변경
    ///
    /// - Parameter index: index
    func changeSaleTitle(_ index: Int) {
        saleTypeBtn.setTitle(saleTypeList[index], for: .normal)
    }
    
    /// 기존/신규 타이틀 변경
    ///
    /// - Parameter index: index
    func changeNewTitle(_ index: Int) {
        newTypeBtn.setTitle(newTypeList[index], for: .normal)
    }

    // MARK: Action Eevet
    @IBAction func pressedModel(_ sender: UIButton) {
        let titles = CommonObject.titlesFromData(productData!)
        CommonUtil.showActionSheet(datas: titles, target: self) { (index) in
            self.changeModelTitle(index)
        }
    }
    
    @IBAction func pressedSaleType(_ sender: UIButton) {
        CommonUtil.showActionSheet(datas: saleTypeList, target: self) { (index) in
            self.changeSaleTitle(index)
        }
    }
    
    @IBAction func pressedNewType(_ sender: UIButton) {
        CommonUtil.showActionSheet(datas: newTypeList, target: self) { (index) in
            self.changeNewTitle(index)
        }
    }
    
    @IBAction func pressedRegister(_ sender: UIButton) {
        requestRegister()
    }
}
