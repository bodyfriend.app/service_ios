//
//  CertificationViewController.swift
//  service
//
//  Created by insung on 2018. 9. 11..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

let TIMER_MAX = 180

class CertificationViewController: BaseViewController {
    
    var certMsgId: String?
    @IBOutlet weak var certificationTF: CTTextField!
    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var timerLb: UILabel!
    @IBOutlet weak var certificationBtn: UIButton!
    
    var countTimer: Timer?
    var startDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("휴대폰인증")
        hideBackBtn()
        
        resendBtn.applyRoundBorder()
        certificationBtn.applyRoundBorder()
        initTimer()
    }
    
    /// 타이머 초기화
    func initTimer() {
        if countTimer != nil {
            countTimer?.invalidate()
            countTimer = nil
        }

        startDate = Date()
        countTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer(_ :)), userInfo: nil, repeats: true)
        countTimer?.fire()
        RunLoop.main.add(countTimer!, forMode: RunLoop.Mode.common)
    }
    
    /// 타이머 업데이트
    ///
    /// - Parameter timer: timer
    @objc func updateTimer(_ timer: Timer) {
        let interval = Date().timeIntervalSince(startDate!)
        let timerCount = TIMER_MAX - Int(interval)
        if timerCount < 0 {
            timerLb.text = "인증번호 재전송을 눌러주세요"
            timer.invalidate()
        } else {
            let minute = timerCount / 60
            let second = (timerCount - minute * 60) % 60
            let timerString = String(format: "%.2d:%.2d", minute, second)
            timerLb.text = timerString
        }
        certificationBtn.isEnabled = timerCount > 0
    }
    
    /// 재전송 요청
    func requestResend() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        guard let telNo = userData?.telNo else {
            return
        }
        var parameters = Dictionary<String, Any>()
        let telNoData = telNo.data(using: .utf8)
        let encrypt = NSData(data: telNoData!).aes256Encrypt(withKey: Constants.Define.CERTIFICATION_AES_KEY)
        let phoneNo = encrypt?.base64EncodedString()
        parameters.updateValue(phoneNo!, forKey: "phoneNumber")
        parameters.updateValue((userData?.adminId)!, forKey: "userId")
        ApiManager.requestToCertification(Constants.URL.CERTIFICATION_SEND, parameters: parameters, completion: { response in
            guard let result = try? JSONDecoder().decode(CertResponse.self, from: response!) else {
                return
            }
            if let msgId = result.data?.msgid {
                self.certMsgId = msgId
                self.initTimer()
                CommonUtil.showMessage("인증번호가 재전송되었습니다")
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 인증 요청
    func requestCertification() {
        guard let certNo = certificationTF.text, certificationTF.text.isExist == true else {
            CommonUtil.showMessage("인증번호를 입력해주세요")
            return
        }
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(certMsgId!, forKey: "msgid")
        parameters.updateValue(certNo, forKey: "authCode")
        ApiManager.requestToCertification(Constants.URL.CERTIFICATION_CONFIRM, parameters: parameters, completion: { response in
            var userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
            userData?.certMsgId = self.certMsgId!
            guard let encodeData = try? JSONEncoder().encode(userData) else {
                return
            }
            UserManager.saveData(data: encodeData, key: Constants.Key.KEY_USER_DATA)
            CommonUtil.moveMainViewController()
            CommonUtil.showMessage("인증이 완료되었습니다")
            LoadingView.hideLoadingView()
        })
    }

    @IBAction func pressedResend(_ sender: UIButton) {
        requestResend()
    }
    
    @IBAction func pressedCertification(_ sender: UIButton) {
        requestCertification()
    }
}
