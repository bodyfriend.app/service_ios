//
//  LoginViewController.swift
//  service
//
//  Created by insung on 2018. 6. 15..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    var preVC: BaseViewController?
    
    @IBOutlet weak var autoBtn: UIButton!
    @IBOutlet weak var idTF: UITextField!
    @IBOutlet weak var pwTF: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestVersionCheck()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CertSegue" {
            let vc = segue.destination as! CertificationViewController
            vc.certMsgId = sender as? String
        }
    }
    
    override func initLayout() {
        super.initLayout()
        loginBtn.applyRoundBorder()
    }
    
    /// 버전 체크
    func requestVersionCheck() {
        var parameters = Dictionary<String, Any>()
        parameters.updateValue("I", forKey: "deviceType")
        parameters.updateValue("service", forKey: "svType")

        ApiManager.requestToPost(Constants.URL.APP_VERSION, parameters: parameters, completion: { response in
            if let decode = try? JSONDecoder().decode(VersionCheckResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    self.failedDataModeling(decode.resultMsg)
                    return
                }
                let info = Bundle.main.infoDictionary
                if let version = Int(info!["CFBundleVersion"] as! String) {
                    if let dataVersion = Int(data.version!) {
                        if version < dataVersion {
                            let isUpdateNeed = data.updateType == "Y"
                            if isUpdateNeed == true {
                                let msg = "앱 업데이트가 필요합니다\n\n내용 : \(data.msg!)"
                                CommonUtil.showAlert(title: "업데이트", msg: msg, target: self, cancel: nil, confirm: {
                                    CommonUtil.openLink(.web, Constants.URL.APP_DOWNLOAD)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                                        exit(0)
                                    })
                                })
                            } else {
                                let msg = "앱 업데이트가 있습니다\n\n내용 : \(data.msg!)"
                                CommonUtil.showAlert(title: "업데이트", msg: msg, target: self, cancel: {
                                    self.loginCheck()
                                }, confirm: {
                                    CommonUtil.openLink(.web, Constants.URL.APP_DOWNLOAD)
                                })
                            }
                        } else {
                            self.loginCheck()
                        }
                    }
                }
            }
            LoadingView.hideLoadingView()
        })
    }

    /// 로그인 여부 체크
    func loginCheck() {
        if let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA) {
            autoBtn.isSelected = userData.autoLogin == "Y"
            if userData.autoLogin == "Y" ||
                preVC != nil {
                idTF.text = userData.adminId
                pwTF.text = userData.adminPw
                requestLogin()
            }
        } else {
            autoBtn.isSelected = false
            idTF.text = nil
            pwTF.text = nil
        }
    }
    
    /// 로그인 요청
    func requestLogin() {
//        idTF.text = "bodyyh"
//        idTF.text = "ydybody"
//        pwTF.text = "body596!"
        
        guard let userId = idTF.text, idTF.text.isExist == true else {
            CommonUtil.showMessage("아이디를 입력해주세요")
            return
        }
        guard let userPw = pwTF.text, pwTF.text.isExist == true else {
            CommonUtil.showMessage("비밀번호를 입력해주세요")
            return
        }
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(userId, forKey: "admin_id")
        parameters.updateValue(userPw, forKey: "admin_pwd")
        parameters.updateValue("0", forKey: "admin_telno")
        parameters.updateValue("0", forKey: "version")
        
        ApiManager.requestToPost(Constants.URL.LOGIN, parameters: parameters, completion: { response in
            if let decode = try? JSONDecoder().decode(LoginResponse.self, from: response!) {
                guard var result = decode.resultData else {
                    CommonUtil.showMessage(decode.resultMsg)
                    LoadingView.hideLoadingView()
                    return
                }
                if (result.perCd == "104" ||
                    result.perCd == "114" ||
                    result.perCd == "120") == false ||
                    result.groupCd != "SV" { // 아이디의 사용 권한 체크
                    CommonUtil.showMessage("사용 권한이 없습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                result.adminPw = self.pwTF.text
                result.autoLogin = self.autoBtn.isSelected ? "Y" : "N"
                if self.autoBtn.isSelected == true {
                    let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
                    result.certMsgId = userData?.certMsgId
                }
                guard let encodeData = try? JSONEncoder().encode(result) else {
                    return
                }
                UserManager.saveData(data: encodeData, key: Constants.Key.KEY_USER_DATA)
                self.checkCertification()
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 인증 여부 체크
    func checkCertification() {
        if let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA) {
            let adminId = userData.adminId
            if adminId == Constants.Identifier.TEST_ID {
                showNextViewController()
                return
            }
            
            // 인증 제거
            showNextViewController()
//            #if DEBUG
//            showNextViewController()
//            #else
//            let certMsgId = userData.certMsgId
//            if certMsgId.isExist == false {
//                requestSendCertification()
//            } else {
//                requestValidation()
//            }
//            #endif
        }
    }
    
    /// 로그인값 초기화
    func initLoginValue() {
        UserManager.removeUserData()
        loginCheck()
    }
    
    /// 인증 값 유효성
    func requestValidation() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var parameters = Dictionary<String, Any>()
        let certMsgId = userData?.certMsgId ?? ""
        parameters.updateValue(certMsgId, forKey: "msgid")
        ApiManager.requestToCertification(Constants.URL.VALIDATION, parameters: parameters, completion: { response in
            if response == nil {
                self.initLoginValue()
                LoadingView.hideLoadingView()
                return
            }
            self.showNextViewController()
            LoadingView.hideLoadingView()
        })
    }
    
    /// 인증번호 전송
    func requestSendCertification() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var parameters = Dictionary<String, Any>()
        guard let telNo = userData?.telNo else {
            return
        }

        let telNoData = telNo.data(using: .utf8)
        let encrypt = NSData(data: telNoData!).aes256Encrypt(withKey: Constants.Define.CERTIFICATION_AES_KEY)
        let phoneNo = encrypt?.base64EncodedString()
        parameters.updateValue(phoneNo!, forKey: "phoneNumber")
        parameters.updateValue((userData?.adminId)!, forKey: "userId")
        ApiManager.requestToCertification(Constants.URL.CERTIFICATION_SEND, parameters: parameters, completion: { response in
            guard let result = try? JSONDecoder().decode(CertResponse.self, from: response!) else {
                return
            }
            if let msgId = result.data?.msgid {
                self.checkNextViewController(msgId)
                CommonUtil.showMessage("인증번호가 전송되었습니다")
            }
            LoadingView.hideLoadingView()
        })
    }
    
    /// 다음 페이지 확인
    ///
    /// - Parameter msgId: 인증 값
    func checkNextViewController(_ msgId: String) {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        let certMsgId = userData?.certMsgId
        if certMsgId.isExist == false ||
            certMsgId != msgId {
            openViewController(segue: "CertSegue", sender: msgId)
        } else {
            showNextViewController()
        }
    }
    
    /// 다음 페이지 이동
    func showNextViewController() {
        if let vc = preVC {
            closeViewController(vc)
        } else {
            CommonUtil.moveMainViewController()
        }
    }

    @IBAction func pressedAuto(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func pressedLogin(_ sender: UIButton) {
        requestLogin()
    }
}
