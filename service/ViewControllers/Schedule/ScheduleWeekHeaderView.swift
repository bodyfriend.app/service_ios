//
//  ScheduleWeekHeaderView.swift
//  service
//
//  Created by insung on 2018. 9. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ScheduleWeekHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var date01Lb: UILabel!
    @IBOutlet weak var date02Lb: UILabel!
    @IBOutlet weak var date03Lb: UILabel!
    @IBOutlet weak var date04Lb: UILabel!
    @IBOutlet weak var date05Lb: UILabel!
    @IBOutlet weak var date06Lb: UILabel!
    @IBOutlet weak var date07Lb: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(_ list: [Date]?) {
        guard let nextDateList = list else {
            return
        }
        for index in 0..<nextDateList.count {
            if let label = viewWithTag(100+index) as? UILabel {
                let nextDate = nextDateList[index]
                let date = CommonUtil.stringFromDate(nextDate, format: "dd일")
                label.text = date
                let weekday = CommonUtil.getWeekend(nextDate)
                if weekday == 1 {
                    label.textColor = COLOR_RED
                } else if weekday == 7 {
                    label.textColor = COLOR_BLUE
                } else {
                    label.textColor = COLOR_WHITE
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        date01Lb.text = nil
        date02Lb.text = nil
        date03Lb.text = nil
        date04Lb.text = nil
        date05Lb.text = nil
        date06Lb.text = nil
        date07Lb.text = nil
    }
}
