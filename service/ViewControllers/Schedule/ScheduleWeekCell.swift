//
//  ScheduleWeekCell.swift
//  service
//
//  Created by insung on 2018. 9. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ScheduleWeekCell: UITableViewCell {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var value01Btn: UIButton!
    @IBOutlet weak var value02Btn: UIButton!
    @IBOutlet weak var value03Btn: UIButton!
    @IBOutlet weak var value04Btn: UIButton!
    @IBOutlet weak var value05Btn: UIButton!
    @IBOutlet weak var value06Btn: UIButton!
    @IBOutlet weak var value07Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initLayout() {
        value01Btn.titleLabel?.numberOfLines = 2
        value02Btn.titleLabel?.numberOfLines = 2
        value03Btn.titleLabel?.numberOfLines = 2
        value04Btn.titleLabel?.numberOfLines = 2
        value05Btn.titleLabel?.numberOfLines = 2
        value06Btn.titleLabel?.numberOfLines = 2
        value07Btn.titleLabel?.numberOfLines = 2
        
        value01Btn.titleLabel?.textAlignment = .center
        value02Btn.titleLabel?.textAlignment = .center
        value03Btn.titleLabel?.textAlignment = .center
        value04Btn.titleLabel?.textAlignment = .center
        value05Btn.titleLabel?.textAlignment = .center
        value06Btn.titleLabel?.textAlignment = .center
        value07Btn.titleLabel?.textAlignment = .center
    }
    
    func setData(_ datas: [ServiceListData], dateList: [Date]?) {
        guard let nextDateList = dateList else {
            return
        }
        if let firstData = datas.first {
            let title = CommonUtil.changeDateFormat(preFormat: "HHmm", date: firstData.visitTime, changeFormat: "HH시")
            titleLb.text = title
        }
        for data in datas {
            let time = CommonUtil.changeDateFormat(preFormat: "HHmm", date: data.visitTime, changeFormat: "HH:mm")
            for index in 0..<nextDateList.count {
                if let btn = viewWithTag(100+index) as? UIButton {
                    let nextDate = nextDateList[index]
                    let date = CommonUtil.stringFromDate(nextDate, format: "yyyy-MM-dd")
                    if data.visitDate == date {
                        btn.backgroundColor = COLOR_BORDER_GRAY
                        let value = String(format: "%@\n%@", data.userName!, time!)
                        btn.setTitle(value, for: .normal)
                    }
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        value01Btn.backgroundColor = COLOR_WHITE
        value02Btn.backgroundColor = COLOR_WHITE
        value03Btn.backgroundColor = COLOR_WHITE
        value04Btn.backgroundColor = COLOR_WHITE
        value05Btn.backgroundColor = COLOR_WHITE
        value06Btn.backgroundColor = COLOR_WHITE
        value07Btn.backgroundColor = COLOR_WHITE
        value01Btn.setTitle(nil, for: .normal)
        value02Btn.setTitle(nil, for: .normal)
        value03Btn.setTitle(nil, for: .normal)
        value04Btn.setTitle(nil, for: .normal)
        value05Btn.setTitle(nil, for: .normal)
        value06Btn.setTitle(nil, for: .normal)
        value07Btn.setTitle(nil, for: .normal)
    }
}
