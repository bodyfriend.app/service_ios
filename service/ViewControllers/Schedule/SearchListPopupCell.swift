//
//  SearchListPopupCell.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SearchListPopupCell: UITableViewCell {
    @IBOutlet weak var titleLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ title: String) {
        titleLb.text = title
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = ""
    }
}
