//
//  ScheduleViewController.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ScheduleViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, SearchProtocol {
    
    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var noDataV: UIView!
    
    var searchV: DateSearchView?
    var timeList = [String]()
    var listDatas = [String : [ServiceListData]]()
    var dateList = [Date]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestSearch()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ServiceDetailSegue" {
            let vc = segue.destination as! ServiceDetailViewController
            vc.receiveSeq = sender as? String
            vc.target = self
            vc.productType = "M"
        }
    }
    
    override func initValue() {
        super.initValue()
        for i in 8...20 {
            timeList.append(String(format: "%.2d", i))
        }
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("스케쥴")
    
        let dayCellNib = UINib(nibName: "ScheduleDayCell", bundle: nil)
        listTbV.register(dayCellNib, forCellReuseIdentifier: "ScheduleDayCell")
        let weekHeaderNib = UINib(nibName: "ScheduleWeekHeaderView", bundle: nil)
        listTbV.register(weekHeaderNib, forHeaderFooterViewReuseIdentifier: "ScheduleWeekHeaderView")
        let weekCellNib = UINib(nibName: "ScheduleWeekCell", bundle: nil)
        listTbV.register(weekCellNib, forCellReuseIdentifier: "ScheduleWeekCell")
        
        listTbV.tableFooterView = UIView()
        
        setOptionView()
    }
    
    /// 상단뷰 검색 뷰 설정
    func setOptionView() {
        searchV = DateSearchView.instanceFromNib()
        searchV?.delegate = self
        topbarV?.optionV.addSubview(searchV!)
        topbarV?.optionHeightLC.isActive = false
        searchV?.applyAutoLayoutFromSuperview((topbarV?.optionV)!)
    }
    
    /// 다음 날짜 리스트 설정
    func setNextDateList() {
        dateList.removeAll()
        if var nextDate = searchV?.searchDate {
            for _ in 0..<7 {
                dateList.append(nextDate)
                nextDate = CommonUtil.nextDayDate(nextDate, nextDay: 1)
            }
        }
    }

    /// 검색 요청
    func requestSearch() {
        guard let parameters = searchV?.sendParameters() else {
            return
        }
        ApiManager.requestToPost(Constants.URL.SERVICE_LIST, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(ServiceListResponse.self, from: response!).resultData?.list) as [ServiceListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            self.setNextDateList()
            self.setListDatas(datas!)
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    /// 리스트 데이터 정렬
    ///
    /// - Parameter datas: 데이터
    func setListDatas(_ datas: [ServiceListData]) {
        listDatas.removeAll()
        
        let sortDatas = datas.sorted { $0.visitTime! < $1.visitTime! }
        for data in sortDatas {
            if let time = CommonUtil.changeDateFormat(preFormat: "HHmm", date: data.visitTime, changeFormat: "HH") {
                if data.visitTime?.hasPrefix(time) == true {
                    var lists = listDatas[time]
                    if lists == nil {
                        lists = [ServiceListData]()
                    }
                    lists?.append(data)
                    listDatas.updateValue(lists!, forKey: time)
                }
            }
        }
    }

    /// 방문일에 따른 데이터
    ///
    /// - Parameter title: 방문일
    /// - Returns: 데이터 리스트
    func datasFromVisitTime(_ title: String) -> [ServiceListData]? {
        if let datas = listDatas[title] {
            var lists = [ServiceListData]()
            for data in datas {
                if data.visitTime?.hasPrefix(title) == true {
                    lists.append(data)
                }
            }
            return lists
        }
        return nil
    }
    
    /// 키값 정렬
    ///
    /// - Returns: 정렬된 키값
    func restoreKeys() -> [String] {
        var keys = Array(listDatas.keys)
        keys.sort { $0 < $1 }
        return keys
    }
    
    /// cell 태그 설정
    ///
    /// - Parameters:
    ///   - cell: cell
    ///   - index: index
    func setCellTag(_ cell: ScheduleWeekCell, index: Int) {
        cell.value01Btn.stringTag = String(index)
        cell.value02Btn.stringTag = String(index)
        cell.value03Btn.stringTag = String(index)
        cell.value04Btn.stringTag = String(index)
        cell.value05Btn.stringTag = String(index)
        cell.value06Btn.stringTag = String(index)
        cell.value07Btn.stringTag = String(index)
        cell.value01Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value02Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value03Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value04Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value05Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value06Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
        cell.value07Btn.addTarget(self, action: #selector(pressedValue(_ :)), for: .touchUpInside)
    }
    
    // MARK: Action Event
    @objc func pressedValue(_ sender: UIButton) {
        let cellIndex = Int(sender.stringTag!) ?? 0
        let keys = restoreKeys()
        let key = keys[cellIndex]
        if let datas = listDatas[key] {
            for data in datas {
                let index = sender.tag - 100
                if index >= 0 {
                    let date = CommonUtil.stringFromDate(dateList[index], format: "yyyy-MM-dd")
                    if data.visitDate?.hasPrefix(date) == true {
                        if let receiveSeq = data.receiveSeq {
                            openViewController(segue: "ServiceDetailSegue", sender: receiveSeq)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listTbV.isHidden = listDatas.count == 0
        noDataV.isHidden = !listTbV.isHidden
        if listDatas.count == 0 {
            return 0
        }
        if searchV?.isSelectedDay == true {
            tableView.allowsSelection = true
            return timeList.count
        } else {
            tableView.allowsSelection = false
            let keys = Array(listDatas.keys)
            return keys.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if searchV?.isSelectedDay == true {
            return 0
        } else {
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if searchV?.isSelectedDay == true {
            return UITableViewHeaderFooterView()
        } else {
            let headerV = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ScheduleWeekHeaderView") as! ScheduleWeekHeaderView
            headerV.setData(dateList)
            return headerV
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchV?.isSelectedDay == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDayCell", for: indexPath) as! ScheduleDayCell
            let title = timeList[indexPath.row]
            let datas = datasFromVisitTime(title)
            cell.setData(datas?.first, title: title)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleWeekCell", for: indexPath) as! ScheduleWeekCell
            setCellTag(cell, index: indexPath.row)
            let keys = restoreKeys()
            let key = keys[indexPath.row]
            if let datas = listDatas[key] {
                cell.setData(datas, dateList: dateList)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchV?.isSelectedDay == true {
            tableView.deselectRow(at: indexPath, animated: true)
            let title = timeList[indexPath.row]
            let datas = datasFromVisitTime(title)
            if let receiveSeq = datas?.first?.receiveSeq {
                openViewController(segue: "ServiceDetailSegue", sender: receiveSeq)
            }
        }
    }
}
