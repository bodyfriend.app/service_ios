//
//  ScheduleDayCell.swift
//  service
//
//  Created by insung on 2018. 9. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ScheduleDayCell: UITableViewCell {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var valueLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(_ listData: ServiceListData?, title: String) {
        if let data = listData {
            nameLb.text = data.userName
            valueLb.text = data.addr
        } else {
            nameLb.text = nil
            valueLb.text = nil
        }
        
        titleLb.text = title
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = nil
        nameLb.text = nil
        valueLb.text = nil
    }
}
