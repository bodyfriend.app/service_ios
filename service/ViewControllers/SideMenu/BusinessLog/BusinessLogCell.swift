//
//  BusinessLogCell.swift
//  service
//
//  Created by insung on 2018. 9. 21..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class BusinessLogCell: UITableViewCell {
    @IBOutlet weak var backV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initLayout() {
        backV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: DetailData, cellIndex: Int) {
        let titleTag = 100
        let valueTag = 200
        let titles = Constants.Array.BUSINESS_LOG_TITLE_LIST
        for index in 0..<titles.count {
            let title = titles[index]
            if let titleLb = backV.viewWithTag(titleTag + index) as? UILabel {
                titleLb.text = title
            }
            if let valueLb = backV.viewWithTag(valueTag + index) as? UILabel {
                valueLb.text = valueFromTitle(data, title, cellIndex)
            }
        }
    }
    
    /// 타이틀에 따른 값 반환
    ///
    /// - Parameters:
    ///   - data: 데이터
    ///   - listTitle: 타이틀
    ///   - index: 데이터 인덱스
    /// - Returns: 값
    func valueFromTitle(_ data: DetailData, _ listTitle: String?, _ index: Int) -> String?{
        guard let title = listTitle else {
            return ""
        }
        var value: String?
        switch title {
        case Constants.Title.BUSINESS_LOG_NUMBER:
            value = String(index+1)
            break
        case Constants.Title.BUSINESS_LOG_COMPLETE_DATE:
            value = data.repairCompleteDate
            break
        case Constants.Title.BUSINESS_LOG_NAME:
            value = data.userName
            break
        case Constants.Title.BUSINESS_LOG_PURCHASE_DATE:
            value = data.inProductDate
            break
        case Constants.Title.BUSINESS_LOG_AS:
            value = data.serviceMonth
            break
        case Constants.Title.BUSINESS_LOG_MANUFACTURE:
            value = data.makeProductDate
            break
        case Constants.Title.BUSINESS_LOG_ADDRESS:
            value = data.addr
            break
        case Constants.Title.BUSINESS_LOG_RECEIVE_DATE:
            value = data.receiveDate
            break
        case Constants.Title.BUSINESS_LOG_VISIT_DATE:
            value = data.visitDate
            break
        case Constants.Title.BUSINESS_LOG_TEL1:
            value = data.hPhoneNo
            break
        case Constants.Title.BUSINESS_LOG_TEL2:
            value = data.telNo
            break
        case Constants.Title.BUSINESS_LOG_SN:
            value = data.productSn
            break
        case Constants.Title.BUSINESS_LOG_CONSULTANT:
            value = data.modConsultant
            break
        case Constants.Title.BUSINESS_LOG_PRODUCT_NAME:
            value = data.oldProductName
            break
        case Constants.Title.BUSINESS_LOG_COST:
            value = data.costNCost == "1" ? "유상" : "무상"
            break
        case Constants.Title.BUSINESS_LOG_DIVIDE:
            value = "완결"
            break
        case Constants.Title.BUSINESS_LOG_MAT_REQUEST:
            value = data.matRequest
            break
        case Constants.Title.BUSINESS_LOG_REPAIR_HISTORY:
            value = data.repairHistory
            break
        case Constants.Title.BUSINESS_LOG_END_TIME:
            value = data.endTime
            break
        case Constants.Title.BUSINESS_LOG_START_TIME:
            value = data.startTime
            break
        case Constants.Title.BUSINESS_LOG_ARRIVAL:
            value = ""
            break
        case Constants.Title.BUSINESS_LOG_DISTANCE:
            value = ""
            break
        case Constants.Title.BUSINESS_LOG_DEPOSIT:
            value = data.costAmount
            break
        default:
            value = ""
            break
        }
        
        return value
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        for subview in backV.subviews {
            if subview is UILabel {
                (subview as! UILabel).text = nil
            }
        }
    }
}
