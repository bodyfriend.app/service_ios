//
//  BusinessLogViewController.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class BusinessLogViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, SearchProtocol {
    
    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var noDataV: UIView!

    var searchV: BusinessLogSearchView?
    var listDatas: [DetailData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestSearch()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("업무일지")
        
        let cellNib = UINib(nibName: "BusinessLogCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "BusinessLogCell")
        listTbV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        
        setOptionView()
    }
    
    /// 상단바에 검색 옵션 뷰 설정
    func setOptionView() {
        searchV = BusinessLogSearchView.instanceFromNib()
        searchV?.delegate = self
        topbarV?.optionV.addSubview(searchV!)
        topbarV?.optionHeightLC.isActive = false
        searchV?.applyAutoLayoutFromSuperview((topbarV?.optionV)!)
    }
    
    /// 검색 요청
    func requestSearch() {
        guard let parameters = searchV?.sendParameters() else {
            return
        }
        ApiManager.requestToPost(Constants.URL.BUSINESS_LOG_LIST, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(BusinessLogResponse.self, from: response!).resultData?.list) as [DetailData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            self.listDatas = datas
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        noDataV.isHidden = !listTbV.isHidden
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessLogCell", for: indexPath) as! BusinessLogCell
        guard let data = listDatas else {
            return cell
        }
        cell.setData(data[indexPath.row], cellIndex: indexPath.row)
        return cell
    }
}
