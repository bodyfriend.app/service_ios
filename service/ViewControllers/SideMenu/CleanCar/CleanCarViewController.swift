//
//  CleanCarViewController.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class CleanCarViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var contentCV: UICollectionView!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet var contentHeightLC: NSLayoutConstraint!
    
    var selectedPhotos: [String : UIImage]? = [:]
    var selectedPhotoTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("세차사진 등록")
        
        let cellNib = UINib(nibName: "PhotoCell", bundle: nil)
        contentCV.register(cellNib, forCellWithReuseIdentifier: "PhotoCell")
        
        contentV.applyRoundBorder()
        registerBtn.applyRoundBorder()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        contentHeightLC.constant = contentCV.contentSize.height
    }

    /// 업로드 요청
    func requestUpload() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        guard let carNumber = userData?.carNumber else {
            return
        }
        guard let uploadImages = parameterUploadImages() else {
            return
        }
        
        var parameters = Dictionary<String, String>()
        parameters.updateValue(carNumber, forKey: "car_number")

        ApiManager.requestToMultipart(Constants.URL.CLEAN_CAR, parameters: parameters, uploadImages: uploadImages) { (response) in
            guard let datas = try? JSONDecoder().decode(CommonResponse.self, from: response!) else {
                LoadingView.hideLoadingView()
                return
            }
            if let msg = datas.resultMsg {
                CommonUtil.showMessage(msg)
            }
            self.closeViewController()
        }
    }
    
    /// 이미지 업로드 파라미터 설정
    ///
    /// - Returns: 파라미터
    func parameterUploadImages() -> Dictionary<String, UIImage>? {
        var uploadImages = Dictionary<String, UIImage>()
        for (key, value) in selectedPhotos! {
            var uploadKey: String?
            if key == "전면" {
                uploadKey = "screen1"
            } else if key == "후면" {
                uploadKey = "screen2"
            } else if key == "운전석" {
                uploadKey = "screen3"
            } else if key == "좌측면" {
                uploadKey = "screen4"
            } else if key == "우측면" {
                uploadKey = "screen5"
            } else if key == "트렁크" {
                uploadKey = "screen6"
            }
            if uploadKey.isExist == true {
                uploadImages.updateValue(value, forKey: uploadKey!)
            }
        }
        return uploadImages
    }
    
    @IBAction func pressedRegister(_ sender: UIButton) {
        requestUpload()
    }
    
    @objc func pressedPhoto(_ sender: UIButton) {
        selectedPhotoTitle = sender.stringTag
        let options = Constants.Array.CAMERA_OPTION_LIST
        CommonUtil.showActionSheet(datas: options, target: self) { (index) in
            let imagePC = UIImagePickerController()
            imagePC.delegate = self
            if index == 0 {
                imagePC.sourceType = .camera
            } else if index == 1 {
                imagePC.sourceType = .photoLibrary
            }
            self.present(imagePC, animated: true, completion: nil)
        }
    }

    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.Array.CLEAN_CAR_PHOTO_TITLE_LIST.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        let title = Constants.Array.CLEAN_CAR_PHOTO_TITLE_LIST[indexPath.row]
        cell.photoBtn.stringTag = title
        cell.photoBtn.addTarget(self, action: #selector(pressedPhoto(_:)), for: .touchUpInside)
        let image = selectedPhotos?[title]
        cell.setData(title: title, image: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth / 3 - 20
        return CGSize(width: width, height: width + 24)
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        var image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        image = image.resizeImage((Constants.Define.APPDELEGATE.window?.bounds.width)!)
        selectedPhotos?.updateValue(image, forKey: selectedPhotoTitle!)
        picker.dismiss(animated: true, completion: {
            self.contentCV.reloadData()
        })
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
