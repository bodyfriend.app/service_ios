//
//  PriceTagCell.swift
//  service
//
//  Created by insung on 2018. 9. 12..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class PriceTagCell: UITableViewCell {
    @IBOutlet weak var backV: UIView!
    @IBOutlet weak var modelLb: UILabel!
    @IBOutlet weak var manufacturerLb: UILabel!
    @IBOutlet weak var divideLb: UILabel!
    @IBOutlet weak var itemLb: UILabel!
    @IBOutlet weak var priceLb: UILabel!
    @IBOutlet weak var rentalLb: UILabel!
    @IBOutlet weak var businessLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        backV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: PriceTagListData) {
        modelLb.text = data.modelNames
        manufacturerLb.text = data.manufacturer
        divideLb.text = data.classification
        itemLb.text = data.item
        priceLb.text = String(data.priceSeq!)
        rentalLb.text = data.rentalPeriod
        businessLb.text = data.workplace
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        modelLb.text = nil
        manufacturerLb.text = nil
        divideLb.text = nil
        itemLb.text = nil
        priceLb.text = nil
        rentalLb.text = nil
        businessLb.text = nil
    }
}
