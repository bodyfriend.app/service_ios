//
//  PriceTagViewController.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class PriceTagViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, SearchProtocol {

    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var noDataV: UIView!
    
    var searchV: PriceSearchView?
    var keyDatas: PriceSearchKeyData?
    var listDatas: [PriceTagListData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestPriceSearchKey()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        self.setTopbarForTitle("서비스 가격표")
        
        let cellNib = UINib.init(nibName: "PriceTagCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "PriceTagCell")
        listTbV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        
        setOptionView()
    }
    
    /// 상단바 검색 뷰 설정
    func setOptionView() {
        searchV = PriceSearchView.instanceFromNib()
        searchV?.target = self
        searchV?.delegate = self
        topbarV?.optionV.addSubview(searchV!)
        topbarV?.optionHeightLC.isActive = false
        searchV?.applyAutoLayoutFromSuperview((topbarV?.optionV)!)
    }
    
    /// 가격 검색 요청
    func requestPriceSearchKey() {
        ApiManager.requestToPost(Constants.URL.PRICE_SEARCH_KEY, parameters: nil, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(PriceSearchKeyResponse.self, from: response!).resultData) as PriceSearchKeyData??) else {
                LoadingView.hideLoadingView()
                return
            }
            if self.keyDatas != nil {
                self.keyDatas = nil
            }
            self.keyDatas = datas
            self.searchV?.receiveData = datas
            self.searchV?.initSelectedKeys()
            self.requestSearch()
        })
    }
    
    /// 검색 요청
    func requestSearch() {
        guard let parameters = searchV?.sendParameters() else {
            return
        }
        
        ApiManager.requestToPost(Constants.URL.PRICE_TAG_LIST, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(PriceTagResponse.self, from: response!).resultData) as [PriceTagListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            if self.listDatas != nil {
                self.listDatas?.removeAll()
                self.listDatas = nil
            }
            self.listDatas = datas
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        noDataV.isHidden = !listTbV.isHidden
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceTagCell", for: indexPath) as! PriceTagCell
        guard let data = listDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
}

