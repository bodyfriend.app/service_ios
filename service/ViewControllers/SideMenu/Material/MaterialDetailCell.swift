//
//  MaterialDetailCell.swift
//  service
//
//  Created by insung on 31/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialDetailCell: UICollectionViewCell {

    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var requestNoLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var materialNoLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var deliveryDateLb: UILabel!
    @IBOutlet weak var stateLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    @IBOutlet weak var memoLb: UILabel!
    @IBOutlet weak var deliveryDateHeightLC: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        contentV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: MaterialDetailData) {
        requestNoLb.text = data.requestNum
        nameLb.text = data.materialName
        materialNoLb.text = data.materialNum
        dateLb.text = data.requestDate
        deliveryDateLb.text = data.receiveDate
        
        if data.requestType == "A" ||
            data.requestType == "D" {
            stateLb.text = data.deliveryState
            deliveryDateHeightLC.isActive = data.receiveDate != nil
        } else {
            stateLb.text = data.state
            deliveryDateHeightLC.isActive = true
        }
        
        countLb.text = String(data.count)
        memoLb.text = data.memo
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        requestNoLb.text = nil
        nameLb.text = nil
        materialNoLb.text = nil
        dateLb.text = nil
        stateLb.text = nil
        deliveryDateLb.text = nil
        countLb.text = nil
        memoLb.text = nil
    }
}
