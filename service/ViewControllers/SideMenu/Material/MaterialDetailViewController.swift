//
//  MaterialDetailViewController.swift
//  service
//
//  Created by insung on 31/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialDetailViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, InputPopupDelegate {

    @IBOutlet weak var listCV: UICollectionView!
    @IBOutlet weak var requestBtn: UIButton!
    
    var target: BaseViewController?
    var requestNo: String?
    var listData: MaterialDetailData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestList()
    }

    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "InputPopupSegue" {
            let vc = segue.destination as! InputPopupViewController
            vc.delegate = self
            vc.receiveData = sender as? [String : Any]
        }
    }
    
    override func initLayout() {
        super.initLayout()
        super.setTopbarForTitle("자재 접수 상세")
    
        let cellNib = UINib(nibName: "MaterialDetailCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "MaterialDetailCell")
    }

    func requestList() {
        var parameters = Dictionary<String, String>()
        parameters.updateValue(requestNo!, forKey: "request_no")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_DETAIL_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialDetailResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    self.listData = decode.data?.first
                    self.listCV.reloadData()
                    self.checkState()
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func isDeliveryType() -> Bool {
        guard let data = listData else {
            return false
        }
        if data.requestType == "A" ||
            data.requestType == "D" {
            return true
        }
        return false
    }
    
    func checkState() {
        if isDeliveryType() == true {
            requestBtn.setTitle("요청 취소", for: .normal)
            requestBtn.backgroundColor = COLOR_DARKGRAY
        } else {
            if listData?.state == "준비완료" {
                requestBtn.setTitle("수령 확인", for: .normal)
                requestBtn.backgroundColor = COLOR_RED
            } else {
                requestBtn.setTitle("주문 취소", for: .normal)
                requestBtn.backgroundColor = COLOR_DARKGRAY
            }
        }
    }
    
    func cancelRequest() {
        let sender = ["requestNo" : requestNo as Any, "title" : Constants.Title.POPUP_MATERIAL_CANCEL] as [String : Any]
        openViewController(segue: "InputPopupSegue", sender: sender)
    }
    
    func reqeustMaterialUpdateState() {
        guard let requestNum = listData?.requestNum else {
            CommonUtil.showMessage("수령할 자재가 없습니다")
            return
        }
        
        var parameters = Dictionary<String, String>()
        parameters.updateValue(requestNum, forKey: "requestNum")
        parameters.updateValue("CA", forKey: "state")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_UPDATE_STATE, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialCommonResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("수령확인 되었습니다")
                    self.closeViewController(self.target)
                    return
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: Action Event
    @IBAction func pressedRequest(_ sender: UIButton) {
        if isDeliveryType() == true {
            cancelRequest()
        } else {
            if listData?.state == "준비완료" {
                reqeustMaterialUpdateState()
            } else {
                cancelRequest()
            }
        }
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let _ = listData else {
            return 0
        }
        listCV.isHidden = false
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MaterialDetailCell", for: indexPath) as! MaterialDetailCell
        guard let data = listData else {
            return cell
        }
        cell.setData(data)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let data = listData else {
            return .zero
        }
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth - 20
        if isDeliveryType() == true {
            if let date = data.receiveDate, date.isEmpty == false {
                print("data.receiveDate : \(data.receiveDate)")
                return CGSize(width: width, height: 170)
            }
        }
        return CGSize(width: width, height: 148)
    }
    
    // MARK: InputPopupDelegate
    func reloadInputData() {
        closeViewController(target)
    }
}
