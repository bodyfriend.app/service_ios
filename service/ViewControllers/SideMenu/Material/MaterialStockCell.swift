//
//  MaterialStockCell.swift
//  service
//
//  Created by insung on 01/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialStockCell: UICollectionViewCell {

    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    @IBOutlet weak var returnCountLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        contentV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: MaterialStockData) {
        nameLb.text = data.materialName
        countLb.text = data.count
        returnCountLb.text = data.returnCount
        dateLb.text = data.update
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLb.text = nil
        countLb.text = nil
        returnCountLb.text = nil
        dateLb.text = nil
    }
}
