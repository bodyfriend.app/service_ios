//
//  MaterialStockDetailViewController.swift
//  service
//
//  Created by insung on 17/12/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialStockDetailViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var numLb: UILabel!
    @IBOutlet weak var manufacturerLb: UILabel!
    @IBOutlet weak var materialNameLb: UILabel!
    @IBOutlet weak var productNameLb: UILabel!
    
    @IBOutlet weak var listCV: UICollectionView!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var target: BaseViewController?
    var materialNum: String?
    var listData: [MaterialReturnData]?
    var selectedListData = [MaterialReturnData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestDetail()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        super.setTopbarForTitle("보유 자재 상세")
        
        let cellNib = UINib(nibName: "MaterialStockDetailCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "MaterialStockDetailCell")
    }
    
    func requestDetail() {
        guard let num = materialNum else {
            CommonUtil.showMessage("자제 정보가 없습니다")
            return
        }
        var parameters = Dictionary<String, String>()
        parameters.updateValue(num, forKey: "materialNum")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "admin_id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_STOCK_DETAIL, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialReturnResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    self.reloadData(decode.data?.detail)
                    self.listData = decode.data?.returnList
                    self.listCV.reloadData()
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func reloadData(_ stockData: MaterialStockData?) {
        if let data = stockData {
            numLb.text = data.materialNum
            manufacturerLb.text = data.materialManufacturer
            materialNameLb.text = data.materialName
            productNameLb.text = data.productName
        }
    }
    
    func requestCancel() {
        if selectedListData.count < 0 {
            CommonUtil.showMessage("요청을 선택해주세요")
            return
        }

        var datas = Array<Any>()
        for data in selectedListData {
            var parameters = Dictionary<String, Any>()
            parameters.updateValue(data.requestNum, forKey: "requestNum")
            datas.append(parameters)
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: datas, options: .prettyPrinted)
        let jsonString = String(data: jsonData, encoding: .utf8)
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(jsonString ?? "", forKey: "list")
        
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_CANCEL, parameters: parameters) { (response) in
            if let decode = try? JSONDecoder().decode(MaterialResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("요청이 완료되었습니다")
                }
            }
            LoadingView.hideLoadingView()
            self.closeViewController(self.target)
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedCancel(_ sender: UIButton) {
        requestCancel()
    }
    
    @objc func pressedCheck(_ sender: UIButton) {
        if let data = listData?[sender.tag] {
            let index = selectedListData.firstIndex(where: { $0 == data } )
            if index == nil {
                selectedListData.append(data)
            } else {
                selectedListData.remove(at: index!)
            }
            listCV.reloadData()
        }
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = listData else {
            return 0
        }
        listCV.isHidden = false
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MaterialStockDetailCell", for: indexPath) as! MaterialStockDetailCell
        guard let data = listData else {
            return cell
        }
        cell.checkBtn.tag = indexPath.row
        cell.checkBtn.addTarget(self, action: #selector(pressedCheck(_:)), for: .touchUpInside)
        cell.checkBtn.isSelected = selectedListData.contains(data[indexPath.row])
        
        cell.setData(data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth - 20
        return CGSize(width: width, height: 76)
    }
    
}
