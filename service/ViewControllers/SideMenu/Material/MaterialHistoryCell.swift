//
//  MaterialHistoryCell.swift
//  service
//
//  Created by insung on 25/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialHistoryCell: UICollectionViewCell {

    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var nameTitleLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var materialLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var deliveryDateLb: UILabel!
    @IBOutlet weak var typeLb: UILabel!
    @IBOutlet weak var stateLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    @IBOutlet weak var deliveryDateHeightLC: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }

    func initLayout() {
        contentV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: MaterialRequestData) {
        
        // R: 여유자재, N: 반납, S: 서비스접수 자재요청 
        if data.requestDivCd == "R" {
            nameTitleLb.text = "주문타입"
            nameLb.text = "여유자재"
        } else if data.requestDivCd == "N" {
            nameTitleLb.text = "주문타입"
            nameLb.text = "반납"
        } else {
            nameTitleLb.text = "고객명"
            nameLb.text = data.userName
        }
        
        // C, V: 자재요청, A, D: 택배요청
        if data.requestType == "A" ||
            data.requestType == "D" {
            typeLb.text = "택배요청"
            deliveryDateHeightLC.isActive = data.receiveDate != nil
        } else {
            typeLb.text = "자재요청"
            deliveryDateHeightLC.isActive = true
        }
        
        titleLb.text = data.requestNum
        materialLb.text = data.materialName
        dateLb.text = data.requestDate
        deliveryDateLb.text = data.receiveDate
        if data.state == "준비완료" {
            contentV.backgroundColor = COLOR_BACKGROUND_ORANGE
        } else {
            contentV.backgroundColor = COLOR_BACKGROUND_GREEN
        }
        stateLb.text = data.state
        countLb.text = String(data.count)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameTitleLb.text = nil
        nameLb.text = nil
        materialLb.text = nil
        titleLb.text = nil
        dateLb.text = nil
        deliveryDateLb.text = nil
        stateLb.text = nil
        countLb.text = nil
    }
}
