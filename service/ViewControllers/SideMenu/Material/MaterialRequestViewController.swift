//
//  MaterialRequestViewController.swift
//  service
//
//  Created by insung on 2018. 10. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class MaterialRequestViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, ListPopupDelegate, InputPopupDelegate {
    
    @IBOutlet weak var warehouseBtn: UIButton!
    @IBOutlet weak var warehouseLb: UILabel!
    @IBOutlet weak var manufacturerBtn: UIButton!
    @IBOutlet weak var productV: UIView!
    @IBOutlet weak var productBtn: UIButton!
    @IBOutlet weak var materialV: UIView!
    @IBOutlet weak var materialBtn: UIButton!
    @IBOutlet weak var countTF: CTTextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var listTbV: UITableView!

    var target: BaseViewController?
    var topbarTitle: String?
    var serviceSeq: String?
    var areaCode: String?
    
    var selectedWarehouseData: WarehouseData?
    var selectedManufacturerData: ManufacturerData?
    var selectedProductData: ProductData?
    var selectedMaterialData: MaterialData?
    var addedDatas: [Dictionary<String, Any>]? = []
    var isEtcMaterial = false // 기타자재 여부
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListPopupSegue" {
            let vc = segue.destination as! ListPopupViewController
            vc.receiveData = sender as? [String : Any]
            vc.delegate = self
        } else if segue.identifier == "InputPopupSegue" {
            let vc = segue.destination as! InputPopupViewController
            vc.delegate = self
            vc.receiveData = sender as? [String : Any]
        }
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle(topbarTitle)
        
        warehouseBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        manufacturerBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        productBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        materialBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        addBtn.applyRoundBorder()
        
        let cellNib = UINib(nibName: "MaterialRequestCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "MaterialRequestCell")
        listTbV.tableFooterView = UIView()
    }
    
    func requestWarehouseList() {
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_WAREHOUSE_LIST, parameters: nil, completion: { (response) in
            if let decode = try? JSONDecoder().decode(WarehouseResponse.self, from: response!) {
                guard let datas = decode.result else {
                    CommonUtil.showMessage("데이터가 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                let sender = ["data" : datas as Any, "title" : Constants.Title.POPUP_WAREHOUSE] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
//                self.selecteWarehouse(datas)
            }
            LoadingView.hideLoadingView()
        })
    }
    
//    func selecteWarehouse(_ datas: [WarehouseData]) {
//        var selectedWarehouse: String = "본사"
//        if let code = self.areaCode, code.isExist {
//            if code != "401" &&
//                code != "402" &&
//                code != "403" &&
//                code != "408" &&
//                code != "417" {
//                selectedWarehouse = "음성"
//            }
//        }
//
//        for data in datas {
//            if data.warehouseName == selectedWarehouse {
//                self.changeBtnTitle(data)
//                break
//            }
//        }
//    }
    
    func requestManufacturerList() {
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_MANUFACTURE_LIST, parameters: nil, completion: { (response) in
            if let decode = try? JSONDecoder().decode(ManufacturerResponse.self, from: response!) {
                guard let data = decode.result else {
                    CommonUtil.showMessage("데이터가 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                
                let sender = ["data" : data as Any, "title" : Constants.Title.POPUP_MANUFACTURER] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func requestProductList() {
        guard let data = selectedManufacturerData else {
            CommonUtil.showMessage("제조사를 선택해주세요")
            return
        }
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(data.manufacturerCode, forKey: "manufacturerCode")
        
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_PRODUCT_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(ProductResponse.self, from: response!) {
                guard let data = decode.result else {
                    CommonUtil.showMessage("제품이 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                let sender = ["data" : data as Any, "title" : Constants.Title.POPUP_PRODUCT] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func requestMaterialList() {
        guard let warehouseData = selectedWarehouseData else {
            CommonUtil.showMessage("창고를 선택해주세요")
            return
        }
        guard let manufacturerData = selectedManufacturerData else {
            CommonUtil.showMessage("제조사를 선택해주세요")
            return
        }
        guard let productData = selectedProductData else {
            CommonUtil.showMessage("제품을 선택해주세요")
            return
        }
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(warehouseData.warehouseSeq, forKey: "warehouseSeq")
        parameters.updateValue(manufacturerData.manufacturerName, forKey: "manufacturerName")
        parameters.updateValue(productData.productName, forKey: "productName")
        
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialResponse.self, from: response!) {
                guard let data = decode.result else {
                    CommonUtil.showMessage("자재가 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                let sender = ["data" : data as Any, "title" : Constants.Title.POPUP_MATERIAL] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
        
    func changeBtnTitle(_ data: Any) {
        if let warehouseData = data as? WarehouseData {
            selectedWarehouseData = warehouseData
            warehouseLb.text = warehouseData.warehouseName
            warehouseBtn.setTitle(warehouseData.warehouseName, for: .normal)
        } else if let manufacturerData = data as? ManufacturerData {
            selectedManufacturerData = manufacturerData
            let name = manufacturerData.manufacturerName
            isEtcMaterial = name == "기타자재"
            productV.isHidden = isEtcMaterial
            materialV.isHidden = isEtcMaterial
            manufacturerBtn.setTitle(name, for: .normal)
            selectedProductData = nil
            selectedMaterialData = nil
            productBtn.setTitle(nil, for: .normal)
            materialBtn.setTitle(nil, for: .normal)
        } else if let productData = data as? ProductData {
            selectedProductData = productData
            selectedMaterialData = nil
            productBtn.setTitle(productData.productName, for: .normal)
            materialBtn.setTitle(nil, for: .normal)
        } else if let materialData = data as? MaterialData {
            selectedMaterialData = materialData
            materialBtn?.setTitle(materialData.materialName, for: .normal)
        }
    }
    
    func initInputView() {
        materialBtn.setTitle(nil, for: .normal)
        countTF.text = nil
        selectedMaterialData = nil
        KeyboardManager.hide()
    }
    
    func requestMaterialRequest() {
        guard let datas = addedDatas else {
            return
        }

        let jsonData = try! JSONSerialization.data(withJSONObject: datas, options: .prettyPrinted)
        let jsonString = String(data: jsonData, encoding: .utf8)

        var parameters = Dictionary<String, Any>()
        parameters.updateValue(jsonString ?? "", forKey: "list")
     
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_REQUEST, parameters: parameters) { (response) in
            if let decode = try? JSONDecoder().decode(MaterialResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("요청이 완료되었습니다")
                }
            }
            LoadingView.hideLoadingView()
            self.closeViewController(self.target)
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedWarehouse(_ sender: UIButton) {
        requestWarehouseList()
    }
    
    @IBAction func pressedManufacturer(_ sender: UIButton) {
        requestManufacturerList()
    }
    
    @IBAction func pressedProduct(_ sender: UIButton) {
        requestProductList()
    }
    
    @IBAction func pressedMaterial(_ sender: UIButton) {
        requestMaterialList()
    }
    
    @IBAction func pressedAdd(_ sender: UIButton) {
        guard let _ = selectedManufacturerData else {
            CommonUtil.showMessage("제조사를 선택해주세요")
            return
        }
        if isEtcMaterial == false {
            guard let _ = selectedProductData else {
                CommonUtil.showMessage("제품을 선택해주세요")
                return
            }
            guard let _ = selectedMaterialData else {
                CommonUtil.showMessage("자재를 선택해주세요")
                return
            }
        }
        guard let count = Int(countTF.text ?? "0"), count > 0 else {
            CommonUtil.showMessage("수량을 입력해주세요")
            return
        }
        
        let sender = ["title" : Constants.Title.POPUP_MATERIAL_MEMO] as [String : Any]
        openViewController(segue: "InputPopupSegue", sender: sender)
    }
    
    @IBAction func pressedRequest(_ sender: UIButton) {
        if let data = addedDatas, data.count > 0 {
            requestMaterialRequest()
        } else {
            CommonUtil.showMessage("요청할 자재를 추가해주세요")
        }
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = addedDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialRequestCell", for: indexPath) as! MaterialRequestCell
        guard let data = addedDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
    
    // MARK: ListPopupDelegate
    func selectedData(_ data: Any) {
        changeBtnTitle(data)
    }
    
    // MARK: InputPopupDelegate
    func reloadInputData(_ data: String) {
        var parameters = Dictionary<String, Any>()
        if let manufacturerData = selectedManufacturerData {
            parameters.updateValue(manufacturerData.manufacturerName, forKey: "manufacturerName")
        }
        if let productData = selectedProductData {
            parameters.updateValue(productData.productName, forKey: "productName")
        }
        if let materialData = selectedMaterialData {
            parameters.updateValue(materialData.materialName, forKey: "materialName")
            parameters.updateValue(materialData.materialNum, forKey: "materialNum")
        }
        if let warehouseData = selectedWarehouseData {
            parameters.updateValue(warehouseData.warehouseSeq, forKey: "warehouseSeq")
        }
        if let count = countTF.text {
            parameters.updateValue(count, forKey: "count")
        }
        if let seq = serviceSeq, seq.isExist {
            parameters.updateValue(seq, forKey: "serviceSeq")
        }

        // V: 미처리요청 A: 택배요청 S: 여유자재요청
        var type: String = ""
        if topbarTitle == Constants.Title.TOPBAR_MATERIAL_REMAIN {
            type = "S"
        } else if topbarTitle == Constants.Title.TOPBAR_MATERIAL_DELIVERY {
            type = "A"
        } else if topbarTitle == Constants.Title.TOPBAR_MATERIAL_RECEIVE {
            type = "V"
        }
        parameters.updateValue(type, forKey: "requestType")
        parameters.updateValue(data, forKey: "memo")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue(userData?.adminId ?? "", forKey: "requestName")
        
        addedDatas?.append(parameters)
        listTbV.reloadData()
        initInputView()
    }
}
