//
//  MaterialReturnViewController.swift
//  service
//
//  Created by insung on 08/11/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialReturnViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, ListPopupDelegate, InputPopupDelegate {

    @IBOutlet weak var warehouseBtn: UIButton!
    @IBOutlet weak var materialBtn: UIButton!
    @IBOutlet weak var countTF: CTTextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var listTbV: UITableView!
    
    var target: BaseViewController?
    
    var selectedWarehouseData: WarehouseData?
    var selectedMaterialData: MaterialStockData?
    var addedDatas: [Dictionary<String, Any>]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListPopupSegue" {
            let vc = segue.destination as! ListPopupViewController
            vc.receiveData = sender as? [String : Any]
            vc.delegate = self
        } else if segue.identifier == "InputPopupSegue" {
            let vc = segue.destination as! InputPopupViewController
            vc.delegate = self
            vc.receiveData = sender as? [String : Any]
        }
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("자재 반납 신청")
        
        warehouseBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        materialBtn.applyRoundBorder(COLOR_BORDER_GRAY)
        addBtn.applyRoundBorder()
        
        let cellNib = UINib(nibName: "MaterialRequestCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "MaterialRequestCell")
        listTbV.tableFooterView = UIView()
    }
    
    func requestWarehouseList() {
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_WAREHOUSE_LIST, parameters: nil, completion: { (response) in
            if let decode = try? JSONDecoder().decode(WarehouseResponse.self, from: response!) {
                guard let data = decode.result else {
                    CommonUtil.showMessage("데이터가 존재하지 않습니다")
                    LoadingView.hideLoadingView()
                    return
                }
                let sender = ["data" : data as Any, "title" : Constants.Title.POPUP_WAREHOUSE] as [String : Any]
                self.openViewController(segue: "ListPopupSegue", sender: sender)
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func requestMaterialList() {
        var parameters = Dictionary<String, String>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "admin_id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_STOCK_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialStockResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    let sender = ["data" : decode.data as Any, "title" : Constants.Title.POPUP_MATERIAL_RETURN] as [String : Any]
                    self.openViewController(segue: "ListPopupSegue", sender: sender)
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func changeBtnTitle(_ data: Any) {
        if let warehouseData = data as? WarehouseData {
            selectedWarehouseData = warehouseData
            warehouseBtn.setTitle(warehouseData.warehouseName, for: .normal)
        } else if let materialData = data as? MaterialStockData {
            selectedMaterialData = materialData
            materialBtn?.setTitle(materialData.materialName, for: .normal)
        }
    }
    
    func initInputView() {
        materialBtn.setTitle(nil, for: .normal)
        countTF.text = nil
        selectedMaterialData = nil
        KeyboardManager.hide()
    }
    
    func requestReturn() {
        guard let datas = addedDatas else {
            return
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: datas, options: .prettyPrinted)
        let jsonString = String(data: jsonData, encoding: .utf8)
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(jsonString ?? "", forKey: "list")
        
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_RETURN, parameters: parameters) { (response) in
            if let decode = try? JSONDecoder().decode(MaterialResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("요청이 완료되었습니다")
                }
            }
            LoadingView.hideLoadingView()
            self.closeViewController(self.target)
        }
    }

    // MARK: Action Event
    @IBAction func pressedWarehouse(_ sender: UIButton) {
        requestWarehouseList()
    }
    
    @IBAction func pressedMaterial(_ sender: UIButton) {
        requestMaterialList()
    }
    
    @IBAction func pressedAdd(_ sender: UIButton) {
        guard let materialData = selectedMaterialData else {
            CommonUtil.showMessage("자재를 선택해주세요")
            return
        }
        guard let _ = selectedWarehouseData else {
            CommonUtil.showMessage("창고를 선택해주세요")
            return
        }
        guard let count = Int(countTF.text ?? "0"), count > 0 else {
            CommonUtil.showMessage("수량을 입력해주세요")
            return
        }
        
        if let dataCount = Int(materialData.count ?? "0"), dataCount < count {
            CommonUtil.showMessage("보유 자재가 부족합니다")
            return
        }
        
        let sender = ["title" : Constants.Title.POPUP_MATERIAL_MEMO] as [String : Any]
        openViewController(segue: "InputPopupSegue", sender: sender)
    }
    
    @IBAction func pressedRequest(_ sender: UIButton) {
        if let data = addedDatas, data.count > 0 {
            requestReturn()
        } else {
            CommonUtil.showMessage("반납할 자재를 추가해주세요")
        }
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = addedDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialRequestCell", for: indexPath) as! MaterialRequestCell
        guard let data = addedDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
    
    // MARK: ListPopupDelegate
    func selectedData(_ data: Any) {
        changeBtnTitle(data)
    }
    
    // MARK: InputPopupDelegate
    func reloadInputData(_ data: String) {
        var parameters = Dictionary<String, Any>()
        if let materialData = selectedMaterialData {
            parameters.updateValue(materialData.materialName, forKey: "materialName")
            parameters.updateValue(materialData.materialNum, forKey: "materialNum")
            if let manufacturer = materialData.materialManufacturer {
                parameters.updateValue(manufacturer, forKey: "manufacturerName")
            }
            if let productName = materialData.productName {
                parameters.updateValue(productName, forKey: "productName")
            }
        }
        if let warehouseData = selectedWarehouseData {
            parameters.updateValue(warehouseData.warehouseSeq, forKey: "warehouseSeq")
        }
        if let count = countTF.text {
            parameters.updateValue(count, forKey: "count")
        }

        parameters.updateValue(data, forKey: "memo")
        parameters.updateValue("N", forKey: "requestType")
        parameters.updateValue("AA", forKey: "state")
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "id")
        parameters.updateValue(userData?.adminId ?? "", forKey: "requestName")
        
        addedDatas?.append(parameters)
        listTbV.reloadData()
        initInputView()
    }
}
