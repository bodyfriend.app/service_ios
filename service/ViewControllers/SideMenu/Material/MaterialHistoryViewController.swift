//
//  MaterialHistoryViewController.swift
//  service
//
//  Created by insung on 2018. 10. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class MaterialHistoryViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var listCV: UICollectionView!
    @IBOutlet weak var noDataV: UIView!
    
    var listDatas: [MaterialRequestData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestList()
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MaterialDetailSegue" {
            let vc = segue.destination as! MaterialDetailViewController
            vc.target = self
            vc.requestNo = sender as? String
        }
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("자재 접수 내역")
        addPullToRefresh()
        
        let cellNib = UINib(nibName: "MaterialHistoryCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "MaterialHistoryCell")
    }
    
    override func reloadViewController() {
        requestList()
    }
    
    func requestList() {
        var parameters = Dictionary<String, String>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "request_id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_REQUEST_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialRequestResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    if self.listDatas != nil {
                        self.listDatas = nil
                    }
                    self.listDatas = decode.data
                    self.listCV.reloadData()
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    func isDeliveryType(_ data: MaterialRequestData) -> Bool {
        if data.requestType == "A" ||
            data.requestType == "D" {
            return true
        }
        return false
    }
    
    // MARK: Action Event
    @objc override func refreshData(_ sender: UIRefreshControl) {
        super.refreshData(sender)
        reloadViewController()
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listCV.isHidden = datas.count == 0
        noDataV.isHidden = !listCV.isHidden
        return datas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MaterialHistoryCell", for: indexPath) as! MaterialHistoryCell
        guard let datas = listDatas else {
            return cell
        }
        cell.setData(datas[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let datas = listDatas else {
            return .zero
        }
        
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth - 20
        let data = datas[indexPath.row]
        if isDeliveryType(data) == true {
            if let date = data.receiveDate, date.isEmpty == false {
                return CGSize(width: width, height: 170)
            }
        }
        return CGSize(width: width, height: 148)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let datas = listDatas else {
            return
        }
        let data = datas[indexPath.row]
        openViewController(segue: "MaterialDetailSegue", sender: data.requestNum)
    }
}
