//
//  MaterialRequestCell.swift
//  service
//
//  Created by insung on 29/10/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialRequestCell: UITableViewCell {

    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: Dictionary<String, Any>) {
        var name: String?
        print("data :", data)
        
        if let materialName = data["materialName"] as? String {
            if let manufacturerName = data["manufacturerName"] as? String {
                name = manufacturerName
                if let productName = data["productName"] as? String {
                    name?.append(" ")
                    name?.append(productName)
                }
                name?.append(" ")
                name?.append(materialName)
            } else {
                name = materialName
            }
        } else {
            if let manufacturerName = data["manufacturerName"] as? String {
                name = manufacturerName
            }
        }
        titleLb.text = name
        if let count = data["count"] as? String {
            countLb.text = count
        } else {
            if let count = data["spendCount"] as? String {
                countLb.text = count
            }
        }
    }

    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = nil
        countLb.text = nil
    }
}
