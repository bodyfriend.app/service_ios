//
//  MaterialStockViewController.swift
//  service
//
//  Created by insung on 2018. 10. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class MaterialStockViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var listCV: UICollectionView!
    @IBOutlet weak var noDataV: UIView!
    
    var listDatas: [MaterialStockData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestList()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MaterialReturnSegue" {
            let vc = segue.destination as! MaterialReturnViewController
            vc.target = self
        } else if segue.identifier == "StockDetailSegue" {
            let vc = segue.destination as! MaterialStockDetailViewController
            vc.materialNum = sender as? String
            vc.target = self
        }
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("보유 자재")
        addPullToRefresh()
        
        let cellNib = UINib(nibName: "MaterialStockCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "MaterialStockCell")
    }
    
    override func reloadViewController() {
        requestList()
    }
    
    func requestList() {
        var parameters = Dictionary<String, String>()
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        parameters.updateValue((userData?.adminId)!, forKey: "admin_id")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_STOCK_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(MaterialStockResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    if self.listDatas != nil {
                        self.listDatas = nil
                    }
                    self.listDatas = decode.data
                    self.listCV.reloadData()
                }
            }
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: Action Event
    @objc override func refreshData(_ sender: UIRefreshControl) {
        super.refreshData(sender)
        reloadViewController()
    }
    
    @IBAction func pressedReturn(_ sender: UIButton) {
        openViewController(segue: "MaterialReturnSegue", sender: self)
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listCV.isHidden = datas.count == 0
        noDataV.isHidden = !listCV.isHidden
        return datas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MaterialStockCell", for: indexPath) as! MaterialStockCell
        guard let datas = listDatas else {
            return cell
        }
        cell.setData(datas[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth - 20
        return CGSize(width: width, height: 92)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let datas = listDatas else {
            return
        }
        openViewController(segue: "StockDetailSegue", sender: datas[indexPath.row].materialNum)
    }
}
