//
//  MaterialStockDetailCell.swift
//  service
//
//  Created by insung on 17/12/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit

class MaterialStockDetailCell: UICollectionViewCell {

    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var numLb: UILabel!
    @IBOutlet weak var warehouseNameLb: UILabel!
    @IBOutlet weak var countLb: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        contentV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    func setData(_ data: MaterialReturnData) {
        numLb.text = data.requestNum
        warehouseNameLb.text = data.warehouseName
        countLb.text = String(data.count)
    }
}
