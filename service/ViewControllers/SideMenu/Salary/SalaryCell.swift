//
//  SalaryCell.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SalaryCell: UITableViewCell {
    @IBOutlet weak var weeksLb: UILabel!
    @IBOutlet weak var weekdayLb: UILabel!
    @IBOutlet weak var weekendLb: UILabel!
    @IBOutlet weak var distanceLb: UILabel!
    @IBOutlet weak var totalLb: UILabel!
    @IBOutlet weak var salaryLb01: UILabel!
    @IBOutlet weak var salaryLb02: UILabel!
    @IBOutlet weak var salaryLb03: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: SalaryListData) {
        weeksLb.text = String(data.weekOfMonth!)
        weekdayLb.text = String(data.weekCnt!)
        weekendLb.text = String(data.weekendCnt!)
        distanceLb.text = String(data.distScore!)
        totalLb.text = String(data.weekDaysTot! + data.weekendTot!).commaToDecimal
//        salaryLb01.text = String(data.weekDaysTot!).commaToDecimal
//        salaryLb02.text = String(data.weekendTot!).commaToDecimal
//        salaryLb03.text = String(data.distTot!).commaToDecimal
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weeksLb.text = nil
        weekdayLb.text = nil
        weekendLb.text = nil
        distanceLb.text = nil
        totalLb.text = nil
//        salaryLb01.text = nil
//        salaryLb02.text = nil
//        salaryLb03.text = nil
    }
}
