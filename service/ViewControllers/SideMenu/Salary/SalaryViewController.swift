//
//  SalaryViewController.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SalaryViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, SearchProtocol {
    
//    @IBOutlet weak var dateBtn: UIButton!
//    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var totalLb: UILabel!
    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var noDataV: UIView!
    
    var searchV: SalarySearchView?

    var listDatas: [SalaryListData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestSearch()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initValue() {
        super.initValue()
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("실적조회")
        setOptionView()
        
        let headerNib = UINib(nibName: "SalaryHeaderView", bundle: nil)
        listTbV.register(headerNib, forHeaderFooterViewReuseIdentifier: "SalaryHeader")
        let cellNib = UINib(nibName: "SalaryCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "SalaryCell")
    }
    
    /// 상단바 검색 뷰 설정
    func setOptionView() {
        searchV = SalarySearchView.instanceFromNib()
        searchV?.delegate = self
        topbarV?.optionV.addSubview(searchV!)
        topbarV?.optionHeightLC.isActive = false
        searchV?.applyAutoLayoutFromSuperview((topbarV?.optionV)!)
    }
    
    /// 검색 요청
    func requestSearch() {
        guard let parameters = searchV?.sendParameters() else {
            return
        }
        ApiManager.requestToPost(Constants.URL.SALARY_LIST, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(SalaryResponse.self, from: response!).resultData) as [SalaryListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            
            if self.listDatas != nil {
                self.listDatas?.removeAll()
                self.listDatas = nil
            }
            self.listDatas = datas
            self.setTotalPrice()
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    /// 수당 총합 
    func setTotalPrice() {
        guard let datas = listDatas else {
            return
        }
        var count: Int = 0
        var total: Float = 0.0
        for data in datas {
            count += data.weekCnt! + data.weekendCnt!
            total += data.distScore!
        }
        totalLb.text = String("총 건수 : \(count) / 총 거리제 점수 : \(total)")
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        noDataV.isHidden = !listTbV.isHidden
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerV = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SalaryHeader")
        return headerV
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalaryCell", for: indexPath) as! SalaryCell
        guard let data = listDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
}
