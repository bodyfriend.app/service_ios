//
//  ManualViewController.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ManualViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var listCV: UICollectionView!
    var listDatas: [String : Bool] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PDFReaderSegue" {
            let vc = segue.destination as! PDFReaderViewController
            vc.pdfUrl = sender as? String
        }
    }

    override func initLayout() {
        super.initLayout()
        self.setTopbarForTitle("서비스메뉴얼")
        let cellNib = UINib.init(nibName: "ManualCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "ManualCell")
        
        checkExistFile()
    }
    
    /// 파일 있는지 체크
    func checkExistFile() {
        let datas = Constants.Array.MANUAL_LIST
        for data in datas {
            let fileName = fileNameFromTitle(data)
            let filePath = CommonUtil.documentUrl().appendingPathComponent(String(format: "%@.pdf", fileName))
            if let fileExists = try? filePath.checkResourceIsReachable() {
                listDatas.updateValue(fileExists, forKey: fileName)
            }
        }
        listCV.reloadData()
    }
    
    /// 이름에 따른 파일이름 설정
    ///
    /// - Parameter title: 이름
    /// - Returns: 파일이름
    func fileNameFromTitle(_ title: String) -> String {
        var fileName = String()
        switch title {
        case Constants.Title.MANUAL_PHARAOH:
            fileName = "phantom"
            break
        case Constants.Title.MANUAL_REGINA:
            fileName = "regina"
            break
        case Constants.Title.MANUAL_PRESIDENT:
            fileName = "president"
            break
        case Constants.Title.MANUAL_FIRSTLADY:
            fileName = "lady"
            break
        case Constants.Title.MANUAL_IROBO:
            fileName = "IRobot"
            break
        case Constants.Title.MANUAL_ROSEMARY:
            fileName = "rosemary"
            break
        case Constants.Title.MANUAL_CRUZE:
            fileName = "cruze"
            break
        default:
            fileName = "phantom"
        }
        return fileName
    }
    
    // MARK: Action Event
    @objc func pressedDownload(_ sender: UIButton) {
        let title = Constants.Array.MANUAL_LIST[sender.tag]
        let fileName = fileNameFromTitle(title)
        ApiManager.downloadFile(String(format: Constants.URL.MANUAL_DOWNLOAD_FORMAT, fileName)) { (response) in
            guard let directoryURL = response else {
                return
            }
            self.checkExistFile()
            self.openViewController(segue: "PDFReaderSegue", sender: directoryURL.absoluteString)
        }
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDatasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.Array.MANUAL_LIST.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManualCell", for: indexPath) as! ManualCell
        let datas = Constants.Array.MANUAL_LIST
        let fileName = fileNameFromTitle(datas[indexPath.row])
        if listDatas[fileName] == true {
            cell.downloadBtn.isHidden = true
        } else {
            cell.downloadBtn.isHidden = false
            cell.downloadBtn.tag = indexPath.row
            cell.downloadBtn.addTarget(self, action: #selector(pressedDownload(_ :)), for: .touchUpInside)
        }
        cell.setData(datas[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth / 2 - 20
        return CGSize(width: width, height: 112)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let title = Constants.Array.MANUAL_LIST[indexPath.row]
        let fileName = fileNameFromTitle(title)
        let filePath = CommonUtil.documentUrl().appendingPathComponent(String(format: "%@.pdf", fileName))
        guard let _ = try? filePath.checkResourceIsReachable() else {
            CommonUtil.showMessage("파일을 불러올수 없습니다")
            return
        }
        openViewController(segue: "PDFReaderSegue", sender: filePath.absoluteString)
    }
}
