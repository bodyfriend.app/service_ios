//
//  ManualCell.swift
//  service
//
//  Created by insung on 2018. 8. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ManualCell: UICollectionViewCell {
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var backIV: UIImageView!
    @IBOutlet weak var downloadBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backIV.applyRoundBorder()
        downloadBtn.applyRoundBorder(radius: 12.0, color: COLOR_WHITE, width:1.0)
    }

    func setData(_ title: String) {
        nameLb.text = title
        var imageName: String?
        switch title {
        case Constants.Title.MANUAL_PHARAOH:
            imageName = "img_pp"
            break
        case Constants.Title.MANUAL_REGINA:
            imageName = "img_rg"
            break
        case Constants.Title.MANUAL_PRESIDENT:
            imageName = "img_pr"
            break
        case Constants.Title.MANUAL_FIRSTLADY:
            imageName = "img_fl"
            break
        case Constants.Title.MANUAL_IROBO:
            imageName = "img_ir"
            break
        case Constants.Title.MANUAL_ROSEMARY:
            imageName = "img_rm"
            break
        case Constants.Title.MANUAL_CRUZE:
            imageName = "img_cp"
            break
        default:
            break
        }
        if let image = imageName, imageName.isExist == true {
            backIV.image = UIImage(named: image)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        backIV.image = nil
        nameLb.text = nil
    }
}
