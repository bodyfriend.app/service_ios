//
//  PDFReaderViewController.swift
//  service
//
//  Created by insung on 2018. 9. 4..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import WebKit

class PDFReaderViewController: BaseViewController {
    
    var pdfUrl: String?
    @IBOutlet weak var contentV: UIView!
    var webView: UIWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("서비스메뉴얼")
        
        let webView = UIWebView()
        contentV.addSubview(webView)
        
        webView.applyAutoLayoutFromSuperview(contentV)

        guard let url = pdfUrl else {
            CommonUtil.showMessage("파일을 불러오지 못했습니다")
            closeViewController()
            return
        }
        let request = URLRequest(url: URL(string: url)!)
        webView.loadRequest(request)
    }
}
