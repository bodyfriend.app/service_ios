//
//  SideMenuViewController.swift
//  service
//
//  Created by insung on 2018. 7. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var versionLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var companyLb: UILabel!
    @IBOutlet weak var listTbV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        listTbV.delegate = self
        listTbV.dataSource = self
        
        let cellNib = UINib(nibName: "SideMenuCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "SideMenuCell")
        listTbV.tableFooterView = UIView()
        
        let info = Bundle.main.infoDictionary
        if let version = info!["CFBundleShortVersionString"] as? String {
            versionLb.text = version
        }
    }
    
    /// 데이터 리로드
    func reloadData() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        nameLb.text = userData?.adminId
        companyLb.text = userData?.companyNm
     }

    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.Array.SIDE_MENU_TITLE_LIST.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.iconIV.image = UIImage(named: Constants.Array.SIDE_MENU_IMAGE_LIST[indexPath.row])
        cell.titleLb.text = Constants.Array.SIDE_MENU_TITLE_LIST[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dismiss(animated: true) {
            let mainVC = Constants.Define.APPDELEGATE.window?.rootViewController as! MainViewController
            switch Constants.Array.SIDE_MENU_TITLE_LIST[indexPath.row] {
            case Constants.Title.SIDE_MENU_MANUAL:
                mainVC.openViewController(segue: "ManualSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_PRICETAG:
                mainVC.openViewController(segue: "PriceTagSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_SALARY:
                mainVC.openViewController(segue: "SalarySegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_CLEAN_CAR:
                let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
                let carNumber = userData?.carNumber
                if carNumber.isExist == false {
                    CommonUtil.showMessage("등록된 차량번호가 없습니다\n관리자에게 문의하시기 바랍니다")
                    return
                }
                mainVC.openViewController(segue: "CleanCarSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_JOURNAL:
                mainVC.openViewController(segue: "JournalSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_RECIEVE:
                mainVC.openViewController(segue: "RecieveSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_MATERIAL_HISTORY:
                mainVC.openViewController(segue: "MaterialHistorySegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_MATERIAL_REMAIN:
                mainVC.openViewController(segue: "MaterialRequestSegue", sender: Constants.Title.TOPBAR_MATERIAL_REMAIN)
                break
            case Constants.Title.SIDE_MENU_MATERIAL_STOCK:
                mainVC.openViewController(segue: "MaterialStockSegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_SALE_HISTORY:
                mainVC.openViewController(segue: "SaleHistorySegue", sender: self)
                break
            case Constants.Title.SIDE_MENU_LOGOUT:
                CommonUtil.showAlert(msg: Constants.Desc.LOGOUT, target: mainVC, cancel: {}, confirm: {
                    mainVC.requestLogout()
                })
                break
            default:
                break
            }
        }
    }
}
