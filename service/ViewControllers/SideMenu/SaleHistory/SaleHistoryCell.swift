//
//  SaleHistoryCell.swift
//  service
//
//  Created by insung on 25/02/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit

class SaleHistoryCell: UITableViewCell {
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var typeLb: UILabel!
    @IBOutlet weak var numLb: UILabel!
    @IBOutlet weak var stateLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: SaleHistoryData) {
        dateLb.text = data.regDate
        nameLb.text = data.inUserName
        typeLb.text = data.productTypeNm
        numLb.text = data.inTel
        if let salesConfirm = data.salesConfirm, salesConfirm.isExist == true {
            stateLb.text = salesConfirm
        } else {
            stateLb.text = "실패"
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dateLb.text = nil
        nameLb.text = nil
        typeLb.text = nil
        numLb.text = nil
        stateLb.text = nil
    }
}
