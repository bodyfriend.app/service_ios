//
//  SaleHistoryViewController.swift
//  service
//
//  Created by insung on 22/02/2019.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit

class SaleHistoryViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet weak var noDataV: UIView!
    
    var listDatas: [SaleHistoryData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestHistory()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initValue() {
        super.initValue()
    }
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("제품판매 조회")
        
        let headerNib = UINib(nibName: "SaleHistoryHeaderView", bundle: nil)
        listTbV.register(headerNib, forHeaderFooterViewReuseIdentifier: "SaleHistoryHeader")
        let cellNib = UINib(nibName: "SaleHistoryCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "SaleHistoryCell")
    }

    /// 검색 요청
    func requestHistory() {
        let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
        var parameters = Dictionary<String, Any>()
        parameters.updateValue((userData?.adminId)!, forKey: "managerid")
        
        ApiManager.requestToPost(Constants.URL.SALE_HISTORY, parameters: parameters, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(SaleHistoryResponse.self, from: response!).resultData.list) as [SaleHistoryData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            
            if self.listDatas != nil {
                self.listDatas?.removeAll()
                self.listDatas = nil
            }
            self.listDatas = datas
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        listTbV.isHidden = datas.count == 0
        noDataV.isHidden = !listTbV.isHidden
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerV = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SaleHistoryHeader")
        return headerV
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleHistoryCell", for: indexPath) as! SaleHistoryCell
        guard let data = listDatas else {
            return cell
        }
        cell.setData(data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if let receivePs = listDatas?[indexPath.row].receivePs, receivePs.isExist == true {
            CommonUtil.showAlert(title: "상담내용", msg: receivePs, target: self, cancel: nil, confirm: nil)
        }
    }
}
