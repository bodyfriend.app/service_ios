//
//  NoticeListCell.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class NoticeListCell: UITableViewCell {
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: NoticeListData) {
        titleLb.text = data.noticeTitle
        dateLb.text = data.rgDt
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = ""
        dateLb.text = ""
    }
}
