//
//  NoticeListViewController.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class NoticeListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var listTbV: UITableView!
    
    var noticeDatas: [NoticeListData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestNotice()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NoticeDetailSegue" {
            let vc = segue.destination as! NoticeDetailViewController
            vc.noticeData = sender as? NoticeListData
        }
    }

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("공지사항")
        
        let cellNib = UINib(nibName: "NoticeListCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "NoticeListCell")
        
        listTbV.tableFooterView = UIView()
    }
    
    /// 공지 요청
    func requestNotice() {
        ApiManager.requestToPost(Constants.URL.NOTICE_LIST, parameters: nil, completion: { (response) in
            guard let datas = ((try? JSONDecoder().decode(NoticeResponse.self, from: response!).resultData?.list) as [NoticeListData]??) else {
                LoadingView.hideLoadingView()
                return
            }
            
            if self.noticeDatas != nil {
                self.noticeDatas?.removeAll()
                self.noticeDatas = nil
            }
            self.noticeDatas = datas
            self.listTbV.reloadData()
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noticeDatas?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticeListCell", for: indexPath) as! NoticeListCell
        guard let data = noticeDatas?[indexPath.row] else {
            return cell
        }
        cell.setData(data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let data = noticeDatas?[indexPath.row] else {
            return
        }
        openViewController(segue: "NoticeDetailSegue", sender: data)
    }
}
