//
//  NoticeDetailViewController.swift
//  service
//
//  Created by insung on 2018. 8. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class NoticeDetailViewController: BaseViewController {
    
    var noticeData: NoticeListData?
    @IBOutlet weak var contentIV: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var contentLb: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("공지사항")
        
        if let imageUrl = noticeData?.pictureOne, noticeData?.pictureOne.isExist == true {
            contentIV.setImageForDynamic(imageUrl)
        }
        titleLb.text = noticeData?.noticeTitle
        contentLb.text = noticeData?.noticeContents
    }
}
