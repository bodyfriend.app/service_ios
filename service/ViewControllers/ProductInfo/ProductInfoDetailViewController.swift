//
//  ProductInfoDetailViewController.swift
//  service
//
//  Created by insung on 2018. 9. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ProductInfoDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    var productCode: String?
    
    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var productIV: UIImageView!
    @IBOutlet weak var listTbV: UITableView!
    
    var detailData: ProductInfoData?

    override func viewDidLoad() {
        super.viewDidLoad()
        requestDetail()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("제품상세 설명")
        showRightBtn(.video)
        
        let cellNib = UINib(nibName: "ProductInfoDetailCell", bundle: nil)
        listTbV.register(cellNib, forCellReuseIdentifier: "ProductInfoDetailCell")
        contentV.applyRoundBorder(COLOR_DARKGRAY)
    }
    
    /// 상세데이터 요청
    func requestDetail() {
        guard let code = productCode else {
            return
        }
        
        var parameters = Dictionary<String, String>()
        parameters.updateValue(code, forKey: "productcode")
        ApiManager.requestToPost(Constants.URL.PRODUCT_INFO_DETAIL, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(ProductInfoDetailResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    CommonUtil.showMessage(decode.resultMsg)
                    LoadingView.hideLoadingView()
                    return
                }
                
                if self.detailData != nil {
                    self.detailData = nil
                }
                self.detailData = data
                self.productIV.setImageForDynamic(data.productImageL)
                self.listTbV.reloadData()
            }
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: Action Eevet
    override func pressedVideo(_ sender: UIButton) {
        CommonUtil.openLink(.web, (detailData?.productIntro)!)
    }

    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = detailData else {
            return 0
        }
        return Constants.Array.PRODUCT_INFO_DETAIL_TITLE_LIST.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductInfoDetailCell", for: indexPath) as! ProductInfoDetailCell
        let titles = Constants.Array.PRODUCT_INFO_DETAIL_TITLE_LIST
        cell.setData(detailData!, title: titles[indexPath.row])
        cell.lineV.isHidden = indexPath.row == titles.count-1
        return cell
    }
}
