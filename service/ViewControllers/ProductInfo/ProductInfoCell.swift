//
//  ProductInfoCell.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ProductInfoCell: UICollectionViewCell {
    @IBOutlet weak var productIV: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    var cachedImages = [String: UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
    
    func initLayout() {
        self.applyRoundBorder()
    }

    func setData(_ data: ProductInfoData) {
        if let imageUrl = data.productImageL {
            if let image = cachedImages[imageUrl] {
                productIV.image = image
            }
            else {
                productIV.setImageFromUrl(imageUrl) { (image) in
                    self.cachedImages.updateValue(image, forKey: imageUrl)
                }
            }
        }
        titleLb.text = data.productName
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productIV.image = nil
        titleLb.text = nil
    }
}
