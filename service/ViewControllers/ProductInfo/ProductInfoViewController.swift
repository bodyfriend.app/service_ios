//
//  ProductInfoViewController.swift
//  service
//
//  Created by insung on 2018. 9. 5..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ProductInfoViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var listCV: UICollectionView!
    var listDatas: [ProductInfoData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailSegue" {
            let vc = segue.destination as! ProductInfoDetailViewController
            vc.productCode = sender as? String
        }
    }
    
    override func initLayout() {
        super.initLayout()
        self.setTopbarForTitle("제품 설명")
        
        let cellNib = UINib.init(nibName: "ProductInfoCell", bundle: nil)
        listCV.register(cellNib, forCellWithReuseIdentifier: "ProductInfoCell")
        listCV.reloadData()
    }
    
    /// 리스트 요청
    func requestList() {
        var parameters = Dictionary<String, String>()
        parameters.updateValue("M", forKey: "producttype")
        ApiManager.requestToPost(Constants.URL.PRODUCT_INFO_LIST, parameters: parameters, completion: { (response) in
            if let decode = try? JSONDecoder().decode(ProductInfoResponse.self, from: response!) {
                guard let data = decode.resultData else {
                    CommonUtil.showMessage(decode.resultMsg)
                    LoadingView.hideLoadingView()
                    return
                }
                if self.listDatas != nil {
                    self.listDatas = nil
                }
                self.listDatas = data.list
                self.listCV.reloadData()
            }
            LoadingView.hideLoadingView()
        })
    }
    
    // MARK: UICollectionViewDatasource & UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let datas = listDatas else {
            return 0
        }
        return datas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductInfoCell", for: indexPath) as! ProductInfoCell
        guard let data = listDatas?[indexPath.row] else {
            return cell
        }
        cell.setData(data)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let width = screenWidth / 2 - 20
        return CGSize(width: width, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = listDatas?[indexPath.row] else {
            return
        }
        openViewController(segue: "DetailSegue", sender: data.productCode!)
    }

}
