//
//  ProductInfoDetailCell.swift
//  service
//
//  Created by insung on 2018. 9. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class ProductInfoDetailCell: UITableViewCell {

    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var valueLb: UILabel!
    @IBOutlet weak var lineV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: ProductInfoData, title: String) {
        titleLb.text = title
        var value: String?
        switch title {
        case "제품명":
            value = data.productName
            break
        case "제품스펙":
            value = data.productSpec
            break
        case "제품설명":
            value = data.productDetail
            break
        case "주의사항":
            if let caution1 = data.productCaution1 {
                value = caution1
                if let caution2 = data.productCaution2 {
                    value = value?.appendingFormat("\n%@", caution2)
                    if let caution3 = data.productCaution3 {
                        value = value?.appendingFormat("\n%@", caution3)
                        if let caution4 = data.productCaution4 {
                            value = value?.appendingFormat("\n%@", caution4)
                            if let caution5 = data.productCaution5 {
                                value = value?.appendingFormat("\n%@", caution5)
                            }
                        }
                    }
                }
            }
            break
        default:
            value = ""
            break
        }
        valueLb.text = value
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLb.text = nil
        valueLb.text = nil
    }
}
