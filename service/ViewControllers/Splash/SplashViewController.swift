//
//  SplashViewController.swift
//  service
//
//  Created by insung on 2018. 8. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    @IBOutlet weak var logoIV: UIImageView!
    
    private let kAnimationKey = "rotation"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        showLoadingView()
        delayMoveViewController()
    }
    
    /// 로딩뷰 노출
    func showLoadingView() {
        if logoIV.layer.animation(forKey: self.kAnimationKey) == nil {
            let animation = CABasicAnimation(keyPath: "transform.rotation")
            animation.fromValue = 0.0
            animation.toValue = Float.pi * 2.0
            animation.duration = 2
            animation.repeatCount = .infinity
            logoIV.layer.add(animation, forKey: kAnimationKey)
        }
    }
    
    /// Splash 페이지 2초 노출
    func delayMoveViewController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            CommonUtil.moveLoginViewController()
        }
    }
}
