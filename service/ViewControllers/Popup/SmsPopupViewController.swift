//
//  SmsPopupViewController.swift
//  service
//
//  Created by insung on 2018. 8. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import MessageUI

class SmsPopupViewController: BaseViewController, UITextViewDelegate, MFMessageComposeViewControllerDelegate {
    
    var data: ServiceListData?
    var productType: String?
    
    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var phoneLb: UILabel!
    @IBOutlet weak var inputV: UIView!
    @IBOutlet weak var inputTV: UITextView!
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var materialBtn: UIButton!
    @IBOutlet weak var vacancyBtn: UIButton!
    @IBOutlet weak var directBtn: UIButton!
    @IBOutlet weak var careBtn: UIButton!
    @IBOutlet weak var decompositionBtn: UIButton!
    @IBOutlet weak var assemblyBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!

    @IBOutlet var inputHeightLC: NSLayoutConstraint!
    @IBOutlet var serviceHeightLC: NSLayoutConstraint!
    
    var serviceBtnGroup: Array<UIButton>?
    var inputBtnGroup: Array<UIButton>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func initValue() {
        super.initValue()
        serviceBtnGroup = [decompositionBtn, assemblyBtn, clearBtn]
        inputBtnGroup = [bookBtn, materialBtn, vacancyBtn, directBtn, careBtn]
    }

    override func initLayout() {
        super.initLayout()
        contentV.applyRoundBorder()
        inputV.applyRoundBorder()
    
        serviceHeightLC.isActive = !(data?.visitTime.isExist)!
        bookBtn.isHidden = !(data?.visitTime.isExist)!
        materialBtn.isHidden = data?.matRequestConfirm != "2"
        careBtn.isHidden = !(data?.visitTime.isExist)!
        
        nameLb.text = data?.userName
        phoneLb.text = data?.phoneNo
        
        pressedServiceGroup(clearBtn)
        pressedInputGroup(directBtn)
    }
    
    /// 내용 변경
    func changeContentView() {
        var text: String?
        if directBtn.isSelected == true { // 직접입력
            serviceHeightLC.isActive = true
            text = Constants.Desc.SMS_DIRECT
        } else {
            let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
            text = String(format: Constants.Format.SMS_INFO, data?.userName ?? "", (userData?.adminNm)!)
            if bookBtn.isSelected == true { // 예약 시
                serviceHeightLC.isActive = !(data?.visitTime.isExist)!
                
                var service: String?
                if decompositionBtn.isSelected == true {
                    service = "분해 작업을 "
                } else if assemblyBtn.isSelected == true {
                    service = "조립 작업을 "
                } else {
                    service = ""
                }
                let date = CommonUtil.dateFromString(data?.visitDate, format: "yyyy-MM-dd")
                let sDate = CommonUtil.stringFromDate(date!, format: "MM월 dd일")
                let time = CommonUtil.dateFromString(data?.visitTime, format: "HHmm")
                let preDate = time?.addingTimeInterval(-1800) // 30분 전
                let nextDate = time?.addingTimeInterval(1800) // 30분 후
                let sPreTime = CommonUtil.stringFromDate(preDate!, format: "a hh시 mm분")
                let sNextTime = CommonUtil.stringFromDate(nextDate!, format: "a hh시 mm분")                
                text?.append(String(format: Constants.Format.SMS_BOOK, sDate, service!, sPreTime, sNextTime))
            } else if careBtn.isSelected == true { // 해피케어 시
                serviceHeightLC.isActive = true
                text?.append(Constants.Desc.SMS_HAPPYCARE)
            } else if materialBtn.isSelected == true { // 자재 시
                serviceHeightLC.isActive = true
                text?.append(Constants.Desc.SMS_MATERIAL)
            } else if vacancyBtn.isSelected == true { // 부재중 시
                serviceHeightLC.isActive = true
                text?.append(Constants.Desc.SMS_VACANCY)
            }
        }
        
        inputTV.text = text
        textViewDidChange(inputTV)
    }
    
    // MARK: Action Eevet
    @IBAction func pressedServiceGroup(_ sender: UIButton) {
        if sender.isSelected == true {
            return
        }
        UIButton.selectToGroup(sender, group: serviceBtnGroup!)
        changeContentView()
    }
    
    @IBAction func pressedInputGroup(_ sender: UIButton) {
        if sender.isSelected == true {
            return
        }
        UIButton.selectToGroup(sender, group: inputBtnGroup!)
        changeContentView()
    }
    
    @IBAction func pressedPopupClose(_ sender: UIButton) {
        closeViewController()
    }
    
    @IBAction func pressedSend(_ sender: UIButton) {
        guard let phoneNo = data?.phoneNo else {
            CommonUtil.showMessage("등록된 전화번호가 없습니다")
            return
        }
        // 메세지 전송
        LoadingView.showLoadingView()
        let messageC = MFMessageComposeViewController()
        messageC.messageComposeDelegate = self
        messageC.recipients = [phoneNo]
        messageC.body = inputTV.text
        self.present(messageC, animated: true, completion: {
            if let seq = self.data?.receiveSeq {
                self.requestAppLog(seq: seq, tel: self.data?.phoneNo ?? "", msg: self.inputTV.text, msg_gb: "2", productType: self.productType ?? "")
            }
        })
    }
    
    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if textView.isEqual(inputTV) {
            let frame = textView.frame
            textView.sizeToFit()
            inputHeightLC.constant = textView.bounds.height
            textView.frame = frame
        }
    }
    
    // MARK: MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        if result == .sent {
            CommonUtil.showMessage("메세지를 전송했습니다")
            closeViewController()
        } else if result == .failed {
            CommonUtil.showMessage("메세지를 전송하지 못했습니다")
        }
        KeyboardManager.hide()
        LoadingView.hideLoadingView()
        controller.dismiss(animated: true, completion: nil)
    }
}
