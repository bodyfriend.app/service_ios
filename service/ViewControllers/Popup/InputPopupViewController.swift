//
//  InputPopupViewController.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

protocol InputPopupDelegate {
    func reloadInputData()
    func reloadInputData(_ data: String)
}

extension InputPopupDelegate {
    func reloadInputData() {}
    func reloadInputData(_ data: String) {}
}

class InputPopupViewController: BaseViewController {
    
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var contentTV: CTTextView!
    
    var delegate: InputPopupDelegate?
    var receiveData: [String : Any]?
    var popupTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        popupTitle = receiveData!["title"] as? String
        titleLb.text = popupTitle
        
        contentV.applyRoundBorder(COLOR_BORDER_GRAY)
        contentTV.applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    /// 자재 주문 취소
    func requestRegister() {
        guard let data = receiveData,
              let requestNo = data["requestNo"] as? String else {
            return
        }
        
        var parameters = Dictionary<String, Any>()
        parameters.updateValue(requestNo, forKey: "requestNum")
        parameters.updateValue(contentTV.text!, forKey: "memo")
        ApiManager.requestToMaterial(Constants.URL.MATERIAL_REQUEST_CANCEL, parameters: parameters) { (response) in
            if let decode = try? JSONDecoder().decode(MaterialCommonResponse.self, from: response!) {
                if decode.error == true {
                    if let msg = decode.message {
                        CommonUtil.showMessage(msg)
                    }
                } else {
                    CommonUtil.showMessage("수정이 완료되었습니다")
                }
            }
            
            LoadingView.hideLoadingView()
            self.closePopupViewController()
        }
    }
    
    func closePopupViewController() {
        self.dismiss(animated: true, completion: {
            self.delegate?.reloadInputData()
        })
    }
    
    // MARK: Action Evnet
    @IBAction func pressedPopupClose(_ sender: UIButton) {
        closeViewController()
    }
    
    @IBAction func pressedRegister(_ sender: UIButton) {
        if popupTitle == Constants.Title.POPUP_MATERIAL_CANCEL {
            requestRegister()
        } else if popupTitle == Constants.Title.POPUP_MATERIAL_MEMO {
            delegate?.reloadInputData(contentTV.text)
            closeViewController()
        }
    }
}
