//
//  HistoryListPopupCell.swift
//  service
//
//  Created by insung on 2018. 8. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class HistoryListPopupCell: UITableViewCell {
    @IBOutlet weak var noLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var serviceLb: UILabel!
    @IBOutlet weak var receiveLb: UILabel!
    @IBOutlet weak var completeLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: HistoryListData, count: Int) {
        noLb.text = String(count)
        nameLb.text = data.userName
        serviceLb.text = String("\(data.serviceMonth!)개월")
        receiveLb.text = data.receiveDate
        completeLb.text = data.repairCompleteDate
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        noLb.text = ""
        nameLb.text = ""
        serviceLb.text = ""
        receiveLb.text = ""
        completeLb.text = ""
    }
}
