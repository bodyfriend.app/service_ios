//
//  ListPopupViewController.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

protocol ListPopupDelegate {
    func selectedData(_ data: Any)
}

class ListPopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, InputTextProtocol {
    
    @IBOutlet weak var contentV: UIView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var searchTF: CTTextField!
    @IBOutlet weak var listTbV: UITableView!
    @IBOutlet var searchHeightLC: NSLayoutConstraint!
    
    var receiveData: [String : Any]?
    var delegate: ListPopupDelegate?
    var popupTitle: String?
    var listData: [Any]?
    var searchData: [Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HistoryDetailSegue" {
            let vc = segue.destination as! HistoryDetailViewController
            vc.receiveSeq = sender as? String
            if let productType = receiveData!["type"] as? String {
                vc.productType = productType
            }
        }
    }
 
    func initLayout() {
        popupTitle = receiveData!["title"] as? String
        
        titleLb.text = popupTitle
        contentV.applyRoundBorder()
        
        listTbV.tableFooterView = UIView()
        
        searchTF.accessoryDelegate = self
        searchHeightLC.isActive = true
        
        if let data = receiveData!["data"] {
            if popupTitle == Constants.Title.POPUP_DETAIL_SYMPTOM {
                listData = data as! [CodeListData]
                let cellNib = UINib(nibName: "SearchListPopupCell", bundle: nil)
                listTbV.register(cellNib, forCellReuseIdentifier: "SearchListPopupCell")
            } else if popupTitle == Constants.Title.POPUP_DETAIL_HISTORY {
                listData = data as! [HistoryListData]
                let cellNib = UINib(nibName: "HistoryListPopupCell", bundle: nil)
                listTbV.register(cellNib, forCellReuseIdentifier: "HistoryListPopupCell")
            } else if popupTitle == Constants.Title.POPUP_WAREHOUSE ||
                popupTitle == Constants.Title.POPUP_MANUFACTURER ||
                popupTitle == Constants.Title.POPUP_PRODUCT ||
                popupTitle == Constants.Title.POPUP_MATERIAL ||
                popupTitle == Constants.Title.POPUP_MATERIAL_USE ||
                popupTitle == Constants.Title.POPUP_MATERIAL_RETURN {
                
                searchHeightLC.isActive = false
                if popupTitle == Constants.Title.POPUP_WAREHOUSE {
                    listData = data as! [WarehouseData]
                } else if popupTitle == Constants.Title.POPUP_MANUFACTURER {
                    listData = data as! [ManufacturerData]
                } else if popupTitle == Constants.Title.POPUP_PRODUCT {
                    listData = data as! [ProductData]
                } else if popupTitle == Constants.Title.POPUP_MATERIAL {
                    listData = data as! [MaterialData]
                } else if popupTitle == Constants.Title.POPUP_MATERIAL_USE {
                    listData = data as! [MaterialStockData]
                } else if popupTitle == Constants.Title.POPUP_MATERIAL_RETURN {
                    listData = data as! [MaterialStockData]
                }
                let cellNib = UINib(nibName: "SearchListPopupCell", bundle: nil)
                listTbV.register(cellNib, forCellReuseIdentifier: "SearchListPopupCell")
            }
            searchData = listData
            listTbV.reloadData()
        }
    }
    
    /// 뷰컨트롤러 닫기
    func closeViewController() {
        dismiss(animated: true, completion: nil)
    }

    /// 상세 페이지 이동
    ///
    /// - Parameter sender: sender
    func openDetailViewController(sender: String) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "HistoryDetailSegue", sender: sender)
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedPopupClose(_ sender: Any) {
        closeViewController()
    }
    
    @IBAction func changedSearch(_ sender: CTTextField) {
        if searchData != nil {
            searchData?.removeAll()
            searchData = nil
        }
        if let keyword = sender.text, keyword.isExist == true {
            if popupTitle == Constants.Title.POPUP_WAREHOUSE {
                searchData = listData?.filter({ ($0 as! WarehouseData).warehouseName.contains(keyword) })
            } else if popupTitle == Constants.Title.POPUP_MANUFACTURER {
                searchData = listData?.filter({ ($0 as! ManufacturerData).manufacturerName.contains(keyword) })
            } else if popupTitle == Constants.Title.POPUP_PRODUCT {
                searchData = listData?.filter({ ($0 as! ProductData).productName.contains(keyword) })
            } else if popupTitle == Constants.Title.POPUP_MATERIAL {
                searchData = listData?.filter({ ($0 as! MaterialData).materialName.contains(keyword) })
            } else if popupTitle == Constants.Title.POPUP_MATERIAL_USE {
                searchData = listData?.filter({ ($0 as! MaterialStockData).materialName.contains(keyword) })
            } else if popupTitle == Constants.Title.POPUP_MATERIAL_RETURN {
                searchData = listData?.filter({ ($0 as! MaterialStockData).materialName.contains(keyword) })
            }
        } else {
            searchData = listData
        }
        listTbV.reloadData()
    }
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if popupTitle == Constants.Title.POPUP_DETAIL_SYMPTOM {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchListPopupCell", for: indexPath) as! SearchListPopupCell
            if let data = searchData?[indexPath.row] as? CodeListData {
                let title = "\(data.commCdNm!) > \(data.detCdNm!)"
                cell.setData(title)
            }
            return cell
        } else if popupTitle == Constants.Title.POPUP_DETAIL_HISTORY {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryListPopupCell", for: indexPath) as! HistoryListPopupCell
            if let data = searchData?[indexPath.row] as? HistoryListData {
                cell.setData(data, count:indexPath.row+1)
            }
            return cell
        } else if popupTitle == Constants.Title.POPUP_WAREHOUSE ||
            popupTitle == Constants.Title.POPUP_MANUFACTURER ||
            popupTitle == Constants.Title.POPUP_PRODUCT ||
            popupTitle == Constants.Title.POPUP_MATERIAL ||
            popupTitle == Constants.Title.POPUP_MATERIAL_USE ||
            popupTitle == Constants.Title.POPUP_MATERIAL_RETURN {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchListPopupCell", for: indexPath) as! SearchListPopupCell
            if let data = searchData?[indexPath.row] as? WarehouseData {
                cell.setData(data.warehouseName)
            } else if let data = searchData?[indexPath.row] as? ManufacturerData {
                cell.setData(data.manufacturerName)
            } else if let data = searchData?[indexPath.row] as? ProductData {
                cell.setData(data.productName)
            } else if let data = searchData?[indexPath.row] as? MaterialData {
                cell.setData("자재명 : \(data.materialName)\n가격 : \(data.materialPrice)" )
            } else if let data = searchData?[indexPath.row] as? MaterialStockData {
                let title = "자재명 : \(data.materialName)\n제품명 : \(data.productName!)"
                cell.setData(title)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let data = searchData?[indexPath.row] as? CodeListData {
            delegate?.selectedData(data)
            closeViewController()
        } else if let data = searchData?[indexPath.row] as? HistoryListData {
            openDetailViewController(sender: data.receiveSeq!)
        } else if let data = searchData?[indexPath.row] as? WarehouseData {
            delegate?.selectedData(data)
            closeViewController()
        } else if let data = searchData?[indexPath.row] as? ManufacturerData {
            delegate?.selectedData(data)
            closeViewController()
        } else if let data = searchData?[indexPath.row] as? ProductData {
            delegate?.selectedData(data)
            closeViewController()
        } else if let data = searchData?[indexPath.row] as? MaterialData {
            delegate?.selectedData(data)
            closeViewController()
        } else if let data = searchData?[indexPath.row] as? MaterialStockData {
            delegate?.selectedData(data)
            closeViewController()
        }
    }
}
