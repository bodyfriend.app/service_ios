//
//  SignPopupViewController.swift
//  service
//
//  Created by insung on 2018. 8. 8..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

protocol SignPopupDelegate {
    func setSignImage(_ image: UIImage?)
}

class SignPopupViewController: UIViewController {
    
    @IBOutlet weak var drawV: DrawView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var delegate: SignPopupDelegate?
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        landscapeViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    override var shouldAutorotate: Bool {
//        return true
//    }
//
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .landscapeLeft
//    }

//    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
//        return .landscapeLeft
//    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func initLayout() {
        drawV.applyRoundBorder(COLOR_BORDER_GRAY)
        resetBtn.applyRoundBorder()
        cancelBtn.applyRoundBorder()
        confirmBtn.applyRoundBorder()
    }
    
    /// 화면 가로모드
    func landscapeViewController() {
        let angle: CGFloat = 90
        let transform = CGAffineTransform(rotationAngle: angle.degrees())
        view.transform = transform
    }
    
    /// 뷰컨트롤러 닫기
    func closeViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Action Eevet
    @IBAction func pressedReset(_ sender: UIButton) {
        drawV.drawPath?.removeAllPoints()
        drawV.setNeedsDisplay()
    }
    
    @IBAction func pressedCancel(_ sender: UIButton) {
        closeViewController()
    }
    
    @IBAction func pressedConfirm(_ sender: UIButton) {
        var image: UIImage? = nil
        if drawV.drawPath?.isEmpty == false {
            image = drawV.imageCaptureView()
        }
        delegate?.setSignImage(image)
        closeViewController()
    }
}
