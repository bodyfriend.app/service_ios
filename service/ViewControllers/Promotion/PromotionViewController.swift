//
//  PromotionViewController.swift
//  service
//
//  Created by insung on 11/12/2018.
//  Copyright © 2018 bodyfriend. All rights reserved.
//

import UIKit
import WebKit

class PromotionViewController: BaseViewController {
    
    @IBOutlet weak var contentV: UIView!
    var webV: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func initLayout() {
        super.initLayout()
        setTopbarForTitle("프로모션")
        
        webV = WKWebView()
        contentV.addSubview(webV)
        webV.applyAutoLayoutFromSuperview(contentV)

        let url = URL(string: Constants.URL.PROMOTION)
        let request = URLRequest(url: url!)
        webV.load(request)
    }
}
