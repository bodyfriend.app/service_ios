//
//  InputTextProtocol.swift
//  service
//
//  Created by insung on 2018. 8. 7..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import Foundation

@objc protocol InputTextProtocol {
    @objc optional func pressedDone(_ sender: Any)
    @objc optional func pressedCancel(_ sender: Any)
    @objc optional func pressedDone(_ sender: Any, type: NSInteger)
}
