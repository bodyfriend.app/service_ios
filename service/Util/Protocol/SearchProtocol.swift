//
//  SearchProtocol.swift
//  service
//
//  Created by insung on 2018. 9. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

protocol SearchProtocol {
    func requestSearch()
}
