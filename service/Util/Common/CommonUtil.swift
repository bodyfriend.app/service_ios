//
//  CommonUtil.swift
//  service
//
//  Created by insung on 2018. 7. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import Toast_Swift
import SideMenu

enum LinkType: Int {
    case call
    case web
}

class CommonUtil {
    class func showAlert(msg: String, target: UIViewController, cancel: (() -> Void)?, confirm: (() -> Void)?) {
        self.showAlert(title: "", msg: msg, target: target, cancel: cancel, confirm: confirm)
    }
    
    class func showAlert(title: String, msg: String, target: UIViewController, cancel: (() -> Void)?, confirm: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        if (cancel != nil) {
            let cancelAction = UIAlertAction(title: "취소", style: .cancel) { (_) in
                cancel?()
            }
            alert.addAction(cancelAction)
        }
        
        let confirmAction = UIAlertAction(title: "확인", style: .default) { (_) in
            confirm?()
        }
        alert.addAction(confirmAction)
        
        DispatchQueue.main.async {
            target.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showActionSheet(datas: Array<String>, target: UIViewController, selected: ((_ index: Int) -> Void)?) {
        KeyboardManager.hide()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        for i in 0 ..< datas.count {
            let data = datas[i]
            let action = UIAlertAction(title: data, style: UIAlertAction.Style.default) { (action) in
                selected!(i)
            }
            alert.addAction(action)
        }
        let closeAction = UIAlertAction(title: "닫기", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(closeAction)
        DispatchQueue.main.async {
            target.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showMessage(_ msg: String) {
        KeyboardManager.hide()
        Constants.Define.APPDELEGATE.window?.makeToast(msg, duration: 1.0, position: .bottom, title: nil, image: nil, style: ToastManager.shared.style, completion: nil)
    }
    
    class func todayFromDate(format: String) -> String  {
        return self.stringFromDate(Date(), format: format)
    }
    
    class func firstDayFromDate(_ date: Date) -> Date {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month], from: date)
        components.day = 1
        return calendar.date(from: components)!
    }
    
    class func preYearDate(_ date: Date, preYear: Int) -> Date {
        let calendar = Calendar.current
        var components = DateComponents()
        components.year = -preYear
        return calendar.date(byAdding: components, to: date, wrappingComponents: false)!
    }
    
    class func nextDayDate(_ date: Date, nextDay: Int) -> Date {
        let calendar = Calendar.current
        var components = DateComponents()
        components.day = nextDay
        return calendar.date(byAdding: components, to: date, wrappingComponents: false)!
    }
    
    class func nextMonthDate(_ date: Date, nextMonth: Int) -> Date {
        let calendar = Calendar.current
        var components = DateComponents()
        let month = nextMonth
        components.month = month
        return calendar.date(byAdding: components, to: date, wrappingComponents: false)!
    }
    
    class func stringFromDate(_ date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    class func dateFromString(_ date: String?, format: String) -> Date? {
        if date.isExist == false {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: date!)!
    }
    
    class func changeDateFormat(preFormat: String, date: String?, changeFormat: String) -> String? {
        if date.isExist == false {
            return nil
        }
        if let changeDate = self.dateFromString(date!, format: preFormat) {
            let formatter = DateFormatter()
            formatter.dateFormat = changeFormat
            return formatter.string(from: changeDate)
        }
        return nil
    }
    
    class func getWeekend(_ date: Date) -> Int {
        let calendar = Calendar.current
        return calendar.component(.weekday, from: date)
    }
    
    class func imageUrl(_ url: String?) -> String? {
        if url.isExist == false {
            return nil
        }
        var fullUrl = String(format: "%@%@", Constants.URL.BASE, url!)
        fullUrl = fullUrl.replacingOccurrences(of: "//", with: "/")
        return fullUrl
    }

    class func openLink(_ type: LinkType,_ string: String) {
        var url: URL?
        if type == .call {
            url = URL(string: String(format: "tel://%@", string))!
        } else if type == .web {
            var fullUrl = string
            if string.hasPrefix("http") == false {
                fullUrl = String(format: "%@%@", Constants.URL.BASE, string)
            }
            url = URL(string: fullUrl)!
        }
        if UIApplication.shared.canOpenURL(url!) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    class func documentUrl() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class func moveMainViewController() {
        
        //  네비게이션 달기
        let mainSb = UIStoryboard(name: Constants.Identifier.STORYBOARD_MAIN_NAME, bundle: nil)
        let mainVC = mainSb.instantiateViewController(withIdentifier: Constants.Identifier.STORYBOARD_MAIN) as! MainViewController
        let menuSb = UIStoryboard(name: Constants.Identifier.STORYBOARD_SIDE_MENU_NAME, bundle: nil)
        let menuVC = menuSb.instantiateViewController(withIdentifier: Constants.Identifier.STORYBOARD_SIDE_MENU) as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuVC
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        Constants.Define.APPDELEGATE.window?.rootViewController = mainVC
    }
    
    class func moveLoginViewController() {
        let storyboard = UIStoryboard(name: Constants.Identifier.STORYBOARD_LOGIN_NAME, bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: Constants.Identifier.STORYBOARD_LOGIN) as! BaseViewController
        Constants.Define.APPDELEGATE.window?.rootViewController = loginVC
    }
    
    class func openAppSetting() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        DispatchQueue.main.async {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
