//
//  CommonObject.swift
//  service
//
//  Created by insung on 2018. 7. 31..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

enum CallType: Int {
    case none
    case visit
    case happy
    case emergency
    case initial
    
    func valueFromType() -> String? {
        switch self {
        case .visit:
            return "지정일 방문"
        case .happy:
            return "해피콜"
        case .emergency:
            return "긴급"
        case .initial:
            return "초도/긴급"
        default:
            return nil
        }
    }
}

enum CodeTitleType: Int {
    case main
    case sub
}

enum ProductType: String {
    case chair = "M"            // 안마의자
    case well = "W"             // 정수기
    case latex = "L"            // 라텍스
    case product = "H"          // 생활가전
    case yet = "U"              // 미처리
    case upgrade = "UH"         // 업그레이드 (안씀)
    case overheat = "A"         // 면상발열 (안씀)
}

enum ServiceListType: Int {
    case chair
    case product
    case yet
    case upgrade
    case overheat
    case vip
}

class CommonObject {
    class func selectedToArray(_ array: [String], selectedTitle: String?) -> Int {
        for title in array {
            if (title == selectedTitle) {
                return array.firstIndex(of: title)!
            }
        }
        return 0
    }
    
    class func valueFromData(_ data: DetailData, title: String) -> String? {
        switch(title) {
        case Constants.Title.DETAIL_NAME:
            return data.userName
        case Constants.Title.DETAIL_OVERHEAT:
            return nil
        case Constants.Title.DETAIL_PHONE:
            return data.hPhoneNo
        case Constants.Title.DETAIL_TEL:
            return data.telNo
        case Constants.Title.DETAIL_AREA:
            return nil
        case Constants.Title.DETAIL_PURCHASE_OFFICE:
            return data.purchasingOffice
        case Constants.Title.DETAIL_ADDRESS:
            if let addr = data.addr {
                return addr
            } else if let jibun = data.jibun {
                return jibun
            }
        case Constants.Title.DETAIL_CALL_TYPE:
            if let callType = (Int)(data.callType!) { // 정수 요청타입 값의 이름
                return CallType(rawValue: callType)?.valueFromType()
            }
            return nil
        case Constants.Title.DETAIL_DELIVERY:
            if data.divisionEngineer.isExist == true {
                let userData = UserManager.loadUserData(key: Constants.Key.KEY_USER_DATA)
                return String(format: "%@(%@)", (userData?.adminNm)!, (userData?.adminId)!)
            }
            return nil
        case Constants.Title.DETAIL_CONTRACT:
            return data.salesCheck == "Y" ? "동일" : "상이"
        case Constants.Title.DETAIL_ETC:
            return data.etcClausal
        case Constants.Title.DETAIL_CONSULTANT:
            return data.lastConsultant
        case Constants.Title.DETAIL_RECEIVE_DATE:
            return data.receiveDate
        case Constants.Title.DETAIL_VISIT_DATE:
            return data.visitDate
        case Constants.Title.DETAIL_VISIT_TIME:
            return CommonUtil.changeDateFormat(preFormat: "HHmm", date: data.visitTime, changeFormat: "a hh시 mm분")
        case Constants.Title.DETAIL_RECEIVE_DETAIL:
            return data.symptom
        case Constants.Title.DETAIL_CONTACT_HISTORY:
            return data.initSymptom
        case Constants.Title.DETAIL_LONG_ASK:
            return data.longTermReq
        case Constants.Title.DETAIL_MODEL_NAME:
            return data.oldProductName?.removeWhitespace()
        case Constants.Title.DETAIL_SERIAL_NUM:
            return data.productSn
        case Constants.Title.DETAIL_PURCHASE_DATE:
            return data.inProductDate
        case Constants.Title.DETAIL_AS:
            return data.serviceMonth
        case Constants.Title.DETAIL_MANUFACTURE:
            return data.oldManufacturer?.removeWhitespace()
        default:
            break
        }
        return nil
    }
    
    class func titlesFromCodeData(_ datas: [CodeListData]) -> [String] {
        return titlesFromCodeData(datas, type: .main)
    }
    
    class func titlesFromCodeData(_ datas: [CodeListData], type: CodeTitleType) -> [String] {
        var titles = [String]()
        for data in datas {
            if type == .main {
                titles.append(data.commCdNm!)
            } else if type == .sub {
                titles.append(data.detCdNm!)
            }
        }
        return titles
    }
    
    class func codeFromData(_ datas: [CodeListData], title: String) -> String? {
        for data in datas {
            if data.detCdNm == title {
                return data.detCd
            }
        }
        return nil
    }
    
    class func titlesFromData(_ datas: [ProductInfoData]) -> [String] {
        var titles = [String]()
        for data in datas {
            if let name = data.productName {
                titles.append(name)
            }
        }
        return titles
    }
    
    class func countOfArray(_ array: [Any]?) -> Int {
        return array?.count ?? 0
    }
    
    class func yetNoteType(_ title: String?) -> Int {
        if title == "맞교체" {
            return 0
        } else if title == "회수수리" {
            return 1
        } else if title == "수리거부" {
            return 2
        } else if title == "반품요청" {
            return 3
        } else {
            return 4
        }
    }
    
    class func titleProgressType(_ index: Int) -> String {
        if index == 0 {
            return "취소접수"
        } else if index == 1 {
            return "접수"
        } else if index == 2 {
            return "방문약속"
        } else if index == 3 {
            return "미처리"
        } else if index == 4 {
            return "수리완료"
        } else if index == 8 {
            return "자재확인"
        }

        return ""
    }
    
    class func indexProgressType(_ title: String?) -> Int {
        if title == "취소접수" {
            return 0
        } else if title == "접수" {
            return 1
        } else if title == "방문약속" {
            return 2
        } else if title == "미처리" {
            return 3
        } else if title == "수리완료" {
            return 4
        } else if title == "자재확인" {
            return 8
        }
        return 0
    }
    
    class func indexPaymentType(_ title: String?) -> Int {
        if title == "카드" {
            return 1
        } else if title == "현금" {
            return 2
        } else if title == "계좌" {
            return 3
        }
        return 0
    }
    
    class func paymentType(_ index: Int) -> String? {
        if index == 1 {
            return "카드"
        } else if index == 2 {
            return "현금"
        } else if index == 3 {
            return "계좌"
        }
        return nil
    }
    
    class func insdexCashReceiptType(_ title: String?) -> Int {
        if title == "핸드폰" {
            return 0
        } else if title == "카드" {
            return 1
        }
        return 0
    }
    
    class func cashReceiptType(_ index: Int) -> String? {
        if index == 0 {
            return "핸드폰"
        } else if index == 1 {
            return "카드"
        }
        return nil
    }
    
    class func indexSearchSortType(_ title: String?) -> Int {
        if title == "접수일순" {
            return 1
        } else if title == "방문예정일순" {
            return 2
        }
        return 1
    }
    
    class func productType(_ title: String?) -> ProductType {
        if title == "안마의자" {
            return .chair
        } else if title == "정수기" {
            return .well
        } else if title == "라텍스" {
            return .latex
        }
        return .chair
    }
}
