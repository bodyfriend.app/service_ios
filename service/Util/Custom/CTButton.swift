//
//  CTButton.swift
//  service
//
//  Created by insung on 2018. 8. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

@IBDesignable
class CTButton: UIButton {
    
    @IBInspectable var normalColor: UIColor?
    @IBInspectable var selectedColor: UIColor?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    open override var isHighlighted: Bool {
        willSet(value) {
            super.isHighlighted = value
            if super.isHighlighted {
                if selectedColor != nil {
                    backgroundColor = selectedColor
                }
            } else {
                if super.isSelected == false &&
                    normalColor != nil {
                    backgroundColor = normalColor
                }
            }
        }
    }
    
    open override var isSelected: Bool {
        willSet(value) {
            super.isSelected = value
            if super.isSelected {
                if selectedColor != nil {
                    backgroundColor = selectedColor
                }
            } else {
                if normalColor != nil {
                    backgroundColor = normalColor
                }
            }
        }
    }
}
