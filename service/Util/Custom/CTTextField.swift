//
//  CTTextField.swift
//  service
//
//  Created by insung on 2018. 7. 24..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

enum DatePickerType: Int {
    case date
    case time
}

enum DateType: NSInteger {
    case none
    case start
    case end
}

class CTTextField: UITextField {
    var contentPV: UIPickerView?
    var contentDP: UIDatePicker?
    var accessoryDelegate: InputTextProtocol?
    var dateType: NSInteger? = DateType.none.rawValue

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        KeyboardManager.setAccessoryView(view: self, target: self)
    }
    
    func setPickerView(_ target: Any) {
        if contentPV == nil {
            contentPV = UIPickerView()
            contentPV?.delegate = target as? UIPickerViewDelegate
        }
        
        contentPV?.backgroundColor = UIColor.white
        inputView = contentPV
        KeyboardManager.setAccessoryView(view: self, target: self, type: .done)
    }
    
    func setDatePickerView(_ type: DatePickerType) {
        if contentDP == nil {
            contentDP = UIDatePicker()
            contentDP?.backgroundColor = UIColor.white
            if #available(iOS 13.4, *) {
                contentDP?.preferredDatePickerStyle = UIDatePickerStyle.wheels
            }
        }
        
        if type == .date {
            contentDP?.datePickerMode = UIDatePicker.Mode.date
        } else if type == .time {
            contentDP?.datePickerMode = UIDatePicker.Mode.time
        }
        inputView = contentDP
        KeyboardManager.setAccessoryView(view: self, target: self, type: .done)
    }
    
    func showPickerView(_ target: Any) {
        setPickerView(target)
        becomeFirstResponder()
    }
    
    func showDatePickerView(_ type: DatePickerType) {
        setDatePickerView(type)
        becomeFirstResponder()
    }
    
    @objc func pressedClose(_ sender: Any) {
        KeyboardManager.hide()
    }
    
    @objc func pressedDone(_ sender: Any) {
        KeyboardManager.hide()
        if inputView is UIDatePicker {
            if dateType == DateType.none.rawValue {
                accessoryDelegate?.pressedDone?(inputView!)
            } else {
                accessoryDelegate?.pressedDone?(inputView!, type: dateType!)
            }
        } else {
            accessoryDelegate?.pressedDone?(inputView!)
        }
    }
    
    @objc func pressedCancel(_ sender: Any) {
        KeyboardManager.hide()
        accessoryDelegate?.pressedCancel?(sender)
    }
}
