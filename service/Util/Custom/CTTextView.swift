//
//  CTTextView.swift
//  service
//
//  Created by insung on 2018. 8. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class CTTextView: UITextView {
    var accessoryDelegate: InputTextProtocol?
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func awakeFromNib() {
        setKeyboardAccessory()
    }
    
    func setKeyboardAccessory() {
        KeyboardManager.setAccessoryView(view: self, target: self)
    }
    
    @objc func pressedClose(_ sender: Any) {
        KeyboardManager.hide()
    }
    
    @objc func pressedDone(_ sender: Any) {
        KeyboardManager.hide()
        accessoryDelegate?.pressedDone?(sender)
    }
    @objc func pressedCancel(_ sender: Any) {
        KeyboardManager.hide()
        accessoryDelegate?.pressedCancel?(sender)
    }
}
