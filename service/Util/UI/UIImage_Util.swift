//
//  UIImage_Util.swift
//  service
//
//  Created by insung on 2018. 8. 22..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeImage(_ width: CGFloat) -> UIImage {
        let oldWidth = self.size.width
        let scaleFactor = width / oldWidth
        let newHeight = self.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func drawTodayText() -> UIImage {
        let font = UIFont.boldSystemFont(ofSize: 30.0)
        let textColor = UIColor.red
        let today = NSString.init(string: String.todayForm("yyyy-MM-dd"))
        let attribute = [NSAttributedString.Key.foregroundColor : textColor, NSAttributedString.Key.font : font]
        let textSize = today.size(withAttributes: attribute)
        
        UIGraphicsBeginImageContext(self.size)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let rect = CGRect(x: self.size.width - textSize.width - 10, y: self.size.height - textSize.height - 10, width: self.size.width, height: self.size.height)
        today.draw(in: rect, withAttributes: attribute)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
