//
//  UIButton_Util.swift
//  service
//
//  Created by insung on 2018. 8. 6..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

private struct AssociatedKeys {
    static var indexPath = "indexPath"
}

extension UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    public func common() {
        titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: -6)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 12)
        applyRoundBorder(COLOR_BORDER_GRAY)
    }
    
    var indexPath : IndexPath {
        get {
            guard let path = objc_getAssociatedObject(self, &AssociatedKeys.indexPath) as? IndexPath else {
                return IndexPath(row: 0, section: 0)
            }
            return path
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.indexPath, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    class func selectToGroup(_ sBtn: UIButton, group: Array<UIButton>) {
        for btn in group {
            btn.isSelected = sBtn.isEqual(btn)
        }
    }
}
