//
//  UIView_Util.swift
//  service
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

private var stringTagHandle: UInt8 = 0

extension UIView {
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    public var stringTag: String? {
        get {
            if let object = objc_getAssociatedObject(self, &stringTagHandle) as? String {
                return object
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &stringTagHandle, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func viewWithStringTag(_ strTag: String) -> UIView? {
        if stringTag == strTag {
            return self
        }
        
        for view in subviews {
            if let matchingSubview = view.viewWithStringTag(strTag) {
                return matchingSubview
            }
        }
        return nil
    }

    func applyRoundBorder() {
        self.applyRoundBorder(radius: 5.0, color: nil, width: 0)
    }
    
    func applyRoundBorder(_ color: UIColor) {
        self.applyRoundBorder(radius: 5.0, color: color, width: 0.4)
    }
    
    func applyRoundBorder(_ radius: CGFloat) {
        self.applyRoundBorder(radius: radius, color: nil, width: 0)
    }
   
    func applyRoundBorder(radius: CGFloat, color: UIColor) {
        self.applyRoundBorder(radius: radius, color: color, width: 0.4)
    }
    
    func applyRoundBorder(radius: CGFloat, color: UIColor?, width: CGFloat) {
        self.layer.borderWidth = width;
        self.layer.cornerRadius = radius;
        if color != nil {
            self.layer.borderColor = color!.cgColor;
        }
    }
    
    func applyAutoLayoutFromSuperview(_ superview: UIView) {
        self.snp.makeConstraints({ (maker) in
            maker.top.bottom.leading.trailing.equalTo(superview);
        })
    }
    
    class func removeSubviews(_ superview: UIView?) {
        if let subviews = superview?.subviews {
            for subview in subviews {
                subview.removeFromSuperview()
            }
        }
    }
    
    func imageCaptureView() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return screenImage
    }
}
