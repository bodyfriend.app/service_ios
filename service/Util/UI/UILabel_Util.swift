//
//  UILabel_Util.swift
//  service
//
//  Created by insung on 2018. 7. 20..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

extension UILabel {
    
    func addUnderLine(string: String) {
        let attribute = [ NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue ]
        let attributedString = NSAttributedString(string: string, attributes: attribute)
        self.attributedText = attributedString
    }    
}

