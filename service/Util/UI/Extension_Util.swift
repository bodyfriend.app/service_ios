//
//  Extension_Util.swift
//  service
//
//  Created by insung on 2018. 7. 30..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

extension String {
    func removeWhitespace() -> String {
        return trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    var commaToDecimal: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let value = Int(self)!
        return formatter.string(from: NSNumber(value: value))!
    }
    
    var isExist: Bool {
        return !self.removeWhitespace().isEmpty
    }
    
    static func todayForm(_ format: String) -> String {
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        return formatter.string(from: Date())
    }
}

extension CGFloat {
    func degrees() -> CGFloat {
        return self * .pi / 180
    }
}

extension Optional where Wrapped == String {
    var isExist: Bool {
        guard let str = self else {
            return false
        }
        return str.isExist
    }
}
