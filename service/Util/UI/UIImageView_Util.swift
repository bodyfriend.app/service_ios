//
//  UIImageView_Util.swift
//  service
//
//  Created by insung on 2018. 8. 27..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

extension UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setImageFromUrl(_ urlString: String?, completion: @escaping (UIImage) -> Void) {
        guard var url = urlString else {
            return
        }
        url = url.replacingOccurrences(of: "/svc", with: "")
        let imageUrl = String(format: "%@%@", Constants.URL.BASE, url)
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: URL(string: imageUrl)!) else {
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: imageData) {
                    self.image = image
                }
            }
        }
    }

    func setImageForDynamic(_ urlString: String?) {
        guard var url = urlString else {
            return
        }
        url = url.replacingOccurrences(of: "/svc", with: "")
        let imageUrl = String(format: "%@%@", Constants.URL.BASE, url)
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: URL(string: imageUrl)!) else {
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: imageData) {
                    let ratio = image.size.height / image.size.width
                    self.snp.makeConstraints({ (make) in
                        make.height.equalTo(ratio * self.bounds.size.width)
                    })
                    self.image = image
                }
            }
        }
    }
    
    class func activeToGroup(_ aIV: UIImageView, group: Array<UIImageView>) {
        for iv in group {
            iv.isHighlighted = aIV.isEqual(iv)
        }
    }
    
    class func showToGroup(_ sIV: UIImageView, group: Array<UIImageView>) {
        for iv in group {
            iv.isHidden = !sIV.isEqual(iv)
        }
    }
}
